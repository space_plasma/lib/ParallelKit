/*
 * Copyright (c) 2018-2022, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#ifndef test_main_h
#define test_main_h

#define PARALLELKIT_INLINE_VERSION 1

#include <ParallelKit/ParallelKit.h>

#include "../ParallelKit/println.h"

#include <algorithm>
#include <array>
#include <complex>
#include <iostream>
#include <numeric>
#include <random>
#include <stdexcept>
#include <tuple>
#include <utility>
#include <vector>

#include <ParallelKit/mpi_wrapper.h>

#define XCTAssert(expression, ...)                                                                 \
    if (!(expression))                                                                             \
    println(std::cerr, "%% At \"", __FILE__,                                                       \
            "\""                                                                                   \
            "\n\t",                                                                                \
            "Line[", __LINE__, "] :: @(", #expression, ") :: ", __VA_ARGS__)

struct double16 {
    using native_type = double;
    std::aligned_storage_t<sizeof(native_type), sizeof(native_type) * 2> data;

    constexpr double16() noexcept : data{} {}
    double16(native_type x) noexcept { ::new (static_cast<void *>(&data)) native_type{ x }; }
    [[nodiscard]] auto operator*() const noexcept
    {
        return reinterpret_cast<native_type const &>(data);
    }
    friend bool     operator==(double16 const &A, double16 const &B) noexcept { return *A == *B; }
    friend bool     operator<(double16 const &A, double16 const &B) noexcept { return *A < *B; }
    friend double16 operator+(double16 const &A, double16 const &B) noexcept { return *A + *B; }
    friend double16 operator-(double16 const &A, double16 const &B) noexcept { return *A - *B; }
    friend decltype(auto) operator<<(std::ostream &os, double16 const &A) noexcept
    {
        return os << *A;
    }
};

PARALLELKIT_NAMESPACE_BEGIN(1)
template <> struct TypeMap<double16> {
    using type = double16;
    [[nodiscard]] auto operator()() const
    {
        return make_type<double16::native_type>().realigned(alignof(type));
    }
};
PARALLELKIT_NAMESPACE_END(1)

using namespace parallel::mpi;
using namespace parallel;
extern Rank master;

#endif /* test_main_h */
