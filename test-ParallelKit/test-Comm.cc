/*
 * Copyright (c) 2018-2021, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "test-main.h"

namespace {

void testComm_Comm(Comm const &world)
{
    if (world.rank() == master) {
        println(std::cout, __PRETTY_FUNCTION__);
    }

    try {
        Comm c;
        XCTAssert(!c, "");

        c = Comm::self();
        XCTAssert(c && MPI_COMM_SELF == *c && c.compare(Comm::self()) == Comparison::identical
                      && std::string("MPI_COMM_SELF") == c.label(),
                  "");

        c = Comm::world();
        XCTAssert(c && MPI_COMM_WORLD == *c && c.compare(world) == Comparison::identical
                      && std::string("MPI_COMM_WORLD") == c.label(),
                  "");

        c                 = world.duplicated();
        std::string label = "duplicated WORLD";
        c.set_label(label.c_str());
        XCTAssert(c && c.compare(world) == Comparison::congruent && label == c.label(), "");
        XCTAssert(c.size() == world.size() && c.rank() == world.rank(), "size = ", c.size(),
                  ", rank = ", c.rank().v);

        c     = world.split(world.rank());
        label = "split comm";
        c.set_label(label.c_str());
        XCTAssert(c && c.compare(Comm::self()) == Comparison::congruent && label == c.label(), "");
        XCTAssert(c.size() == 1 && c.rank() == 0, "size = ", c.size(), ", rank = ", c.rank().v);

        int const color = world.rank() % 2, key = world.size() - world.rank();
        int const size             = world.size() / 2 + (!color && world.size() % 2 ? 1 : 0);
        c                          = world.split(color, key);
        auto const translated_rank = c.group().translate(c.rank(), world);
        XCTAssert(c.size() == size && translated_rank && translated_rank == world.rank(),
                  "size = ", c.size(), ", rank = ", c.rank().v);
        auto prev = c.group().translate({ 0 }, world);
        for (int i = 1; i < c.size(); ++i) {
            auto const curr = c.group().translate({ i }, world);
            XCTAssert(prev > curr, "i = ", i, ", prev_rank = ", prev.v, ", cur_rank = ", curr.v);
            prev = curr;
        }

        if (master == world.rank()) {
            c               = world.split(master);
            auto const rank = c.group().translate(c.rank(), world);
            XCTAssert(c && rank && master == rank.v, "");
        } else {
            c = world.split(MPI_UNDEFINED);
            XCTAssert(!c, "");
        }

        c = world.created(Group{});
        XCTAssert(!c, "");

        c = world.created(world.group().included({ master }));
        if (master == world.rank()) {
            auto const rank = c.group().translate(c.rank(), world);
            XCTAssert(c && rank && master == rank.v, "");
        } else {
            XCTAssert(!c, "");
        }

        c = world.created(world.group().excluded({ master }));
        if (master != world.rank()) {
            auto const rank = c.group().translate(c.rank(), world);
            XCTAssert(c && rank && master != rank.v, "");
        } else {
            XCTAssert(!c, "");
        }
    } catch (std::exception const &e) {
        XCTAssert(false, e.what());
    }
}

void testComm_Group(Comm const &world)
{
    if (world.rank() == master) {
        println(std::cout, __PRETTY_FUNCTION__);
    }

    try {
        Group g;
        XCTAssert(g && *g == MPI_GROUP_EMPTY, "");

        g = world.group();
        XCTAssert(g && g.size() == world.size() && g.rank() == world.rank(), "");
        XCTAssert(g.translate(g.rank(), world) == world.rank(), "");
        XCTAssert(g.compare(world) == Comparison::identical, "");
        XCTAssert(g.compare(Group{}) == Comparison::unequal, "");

        g = world.group() & Group{};
        XCTAssert(g && g.size() == Group{}.size() && !g.rank(), "");
        // XCTAssert(g.translate(Rank::null(), world) == Rank::null(), ""); // NOTE: openmpi has
        // issue
        XCTAssert(g.compare(world) == Comparison::unequal, "");
        XCTAssert(g.compare(Group{}) == Comparison::identical, "");

        g = world.group() | Group{};
        XCTAssert(g && g.size() == world.size() && g.rank() == world.rank(), "");
        XCTAssert(g.translate(g.rank(), world) == world.rank(), "");
        XCTAssert(g.compare(world) == Comparison::identical, "");
        XCTAssert(g.compare(Group{}) == Comparison::unequal, "");

        g = world.group() - Group{};
        XCTAssert(g && g.size() == world.size() && g.rank() == world.rank(), "");
        XCTAssert(g.translate(g.rank(), world) == world.rank(), "");
        XCTAssert(g.compare(world) == Comparison::identical, "");
        XCTAssert(g.compare(Group{}) == Comparison::unequal, "");

        g = world.group().included({ world.rank() });
        XCTAssert(g && g.size() == 1 && g.rank() && g.rank() == 0, "");
        XCTAssert(g.translate(g.rank(), world) == world.rank(), "");

        g = world.group() - g;
        XCTAssert(g && g.size() == world.size() - 1 && !g.rank(), "");
        // XCTAssert(g.translate(Rank::null(), world) == Rank::null(), ""); // NOTE: openmpi has
        // issue

        g = world.group().excluded({ world.rank() });
        XCTAssert(g && g.size() == world.size() - 1 && !g.rank(), "");
        // XCTAssert(g.translate(Rank::null(), world) == Rank::null(), "");
    } catch (std::exception &e) {
        XCTAssert(false, e.what());
    }
}

void testCommunicator_Core(Comm const &world)
{
    if (world.rank() == master) {
        println(std::cout, __PRETTY_FUNCTION__);
    }

    try {
        Communicator<double16, char, long> const comm{ world.duplicated() };

        XCTAssert(comm.size() == world.size(), "");
        XCTAssert(comm.rank()->v == world.rank(), "");
        comm.barrier();
    } catch (std::exception const &e) {
        XCTAssert(false, e.what());
    }
}

} // namespace

void testComm(Comm const &world)
{
    world.barrier();

    testComm_Comm(world);
    testComm_Group(world);
    testCommunicator_Core(world);
}
