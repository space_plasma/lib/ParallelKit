/*
 * Copyright (c) 2018-2021, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "test-main.h"

#include <exception>

Rank master;

extern void testType(Comm const &world);
extern void testTypeMap(Comm const &world);
extern void testComm(Comm const &world);
extern void testP2P(Comm const &world);
extern void testCollective(Comm const &world);
extern void testReduction(Comm const &world);

int main(int argc, char *argv[])
{
    Comm::init(&argc, &argv);
    Comm const world = Comm::world();

    master = { world.size() - 1 };
    if (master == world.rank()) {
        println(std::cout, "%% ", __PRETTY_FUNCTION__, " - world.size() = ", world.size());
    }

    try {
        constexpr long n = 1;
        for (long i = 0; i < n; ++i) {
            if (master == world.rank()) {
                println(std::cout, "%% ", __PRETTY_FUNCTION__, " - iteration = ", i);
            }
            testType(world);
            testComm(world);
            testP2P(world);
            testCollective(world);
            testReduction(world);
        }
    } catch (std::exception const &e) {
        println(std::cerr, "uncaught exception : ", e.what());
        MPI_Abort(MPI_COMM_WORLD, -1);
    }

    return Comm::deinit();
}
