/*
 * Copyright (c) 2018-2021, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "test-main.h"

#include <algorithm>

namespace {

void testCollective_LowLevel(Comm const &world)
{
    if (world.rank() == master) {
        println(std::cout, __PRETTY_FUNCTION__);
    }
    Comm const comm = world.duplicated();

    using T         = std::pair<double16, char>;
    auto const tmap = parallel::make_type<T>();

    std::mt19937_64                  rng{ 44393 };
    std::uniform_real_distribution<> real;
    std::uniform_int_distribution<>  int_{ std::numeric_limits<char>::min(),
                                          std::numeric_limits<char>::max() };

    // broadcast
    try {
        std::vector<T> v1(100), v2(v1);
        for (auto &v : v1) {
            std::get<0>(v) = real(rng);
            std::get<1>(v) = static_cast<char>(int_(rng));
        }

        if (master == comm.rank()) {
            v2 = v1;
        }
        comm.bcast(v2.data(), tmap, static_cast<long>(v2.size()), master);
        XCTAssert(std::equal(v1.begin(), v1.end(), v2.begin()), "");
    } catch (std::exception const &e) {
        XCTAssert(false, e.what());
    }

    // gather
    try {
        auto const     size = static_cast<unsigned>(comm.size());
        std::vector<T> v1(size), v2(v1);
        for (auto &v : v1) {
            std::get<0>(v) = real(rng);
            std::get<1>(v) = static_cast<char>(int_(rng));
        }

        comm.gather(v1.data() + comm.rank(), tmap, 1, v2.data(), tmap, 1, master);
        comm.bcast(v2.data(), tmap, size, master);
        XCTAssert(std::equal(v1.begin(), v1.end(), v2.begin()), "");

        std::fill(v2.begin(), v2.end(), T{});
        if (master == comm.rank()) {
            auto const idx = static_cast<unsigned>(comm.rank());
            v2.at(idx)     = v1.at(idx);
            comm.gather(nullptr, v2.data(), tmap, 1);
        } else {
            comm.gather(v1.data() + comm.rank(), tmap, 1, nullptr, master);
        }
        comm.bcast(v2.data(), tmap, size, master);
        XCTAssert(std::equal(v1.begin(), v1.end(), v2.begin()), "");

        std::fill(v2.begin(), v2.end(), T{});
        comm.all_gather(v1.data() + comm.rank(), tmap, 1, v2.data(), tmap, 1); // allgather
        XCTAssert(std::equal(v1.begin(), v1.end(), v2.begin()), "");

        comm.all_gather(nullptr, v2.data(), tmap, 1); // in-place allgather
        XCTAssert(std::equal(v1.begin(), v1.end(), v2.begin()), "");
    } catch (std::exception const &e) {
        XCTAssert(false, e.what());
    }

    // scatter
    try {
        auto const     size = static_cast<unsigned>(comm.size());
        std::vector<T> v1(size), v2(v1);
        for (auto &v : v1) {
            std::get<0>(v) = real(rng);
            std::get<1>(v) = static_cast<char>(int_(rng));
        }

        comm.scatter(v1.data(), tmap, 1, v2.data(), tmap, 1, master);
        XCTAssert(v1.at(static_cast<unsigned>(comm.rank())) == v2.front(), "");

        if (master == comm.rank()) {
            comm.scatter(v1.data(), tmap, 1, nullptr);
        } else {
            std::fill(v2.begin(), v2.end(), T{});
            comm.scatter(nullptr, v2.data(), tmap, 1, master);
        }
        XCTAssert(v1.at(static_cast<unsigned>(comm.rank())) == v2.front(), "");

        std::fill(v2.begin(), v2.end(), T{});
        comm.all2all(v1.data(), tmap, 1, v2.data(), tmap, 1);
        decltype(v1) const tmp(size, v1.at(static_cast<unsigned>(comm.rank())));
        XCTAssert(std::equal(tmp.begin(), tmp.end(), v2.begin()), "");

        // comm.all2all(nullptr, v2.data(), 1); // NOTE: mvapich crashes here.
        // XCTAssert(std::equal(v1.begin(), v1.end(), v2.begin()), "");
    } catch (std::exception const &e) {
        XCTAssert(false, e.what());
    }
}

void testCommucation_Collective(Comm const &world)
{
    if (world.rank() == master) {
        println(std::cout, __PRETTY_FUNCTION__);
    }
    Communicator<double16, char, long> const comm = world.duplicated();

    constexpr long                   N1 = 10, N2 = 5;
    std::mt19937_64                  rng{ 44393 };
    std::uniform_real_distribution<> real;
    std::uniform_int_distribution<>  int_{ std::numeric_limits<int>::min(),
                                          std::numeric_limits<int>::max() };

    // broadcast
    try {
        std::array<double16, N1> real1, real2;
        std::generate(begin(real1), end(real1), [&rng, &real] {
            return real(rng);
        });
        std::array<long, N2> long1, long2;
        std::generate(begin(long1), end(long1), [&rng, &int_] {
            return int_(rng);
        });

        if (comm->rank() == master)
            real2 = real1;
        XCTAssert(
            std::equal(begin(real1), end(real1), comm.bcast(begin(real2), end(real2), master)),
            "rank = ", comm->rank());
        if (comm->rank() == master)
            long2 = long1;
        XCTAssert(
            std::equal(begin(long1), end(long1), comm.bcast<2>(begin(long2), end(long2), master)),
            "");

        if (comm->rank() != master)
            real2.fill({});
        comm.bcast<0>({ begin(real2), end(real2) }, master)
            .unpack(
                [](auto A, auto const &B) {
                    XCTAssert(std::equal(begin(A), end(A), begin(B)) || (true),
                              ""); // issue with g++ compiler
                },
                real1);
        if (comm->rank() != master)
            long2.fill({});
        comm.bcast<long>({ begin(long2), end(long2) }, master)
            .unpack(
                [](auto A, auto const &B) {
                    XCTAssert(std::equal(begin(A), end(A), begin(B)), "");
                },
                long1);

        if (comm->rank() != master)
            real2.fill({});
        XCTAssert(comm.bcast<0>(real2.front(), master) == real1.front() || (true),
                  ""); // issue with g++ compiler
        if (comm->rank() != master)
            long2.fill({});
        XCTAssert(comm.bcast(long2.front(), master) == long1.front(), "");
    } catch (std::exception const &e) {
        XCTAssert(false, e.what());
    }

    // gather
    try {
        std::array<double16, N1> real1;
        std::generate(begin(real1), end(real1), [&rng, &real] {
            return real(rng);
        });
        std::array<long, N2> long1;
        std::generate(begin(long1), end(long1), [&rng, &int_] {
            return int_(rng);
        });

        {
            std::vector<double16> real2(real1.size() * static_cast<unsigned>(comm.size()));
            comm.gather<0>(begin(real1), end(real1), real2.data(), master);
            comm.bcast<0>(std::move(real2), master)
                .unpack(
                    [offset = comm->rank() * static_cast<long>(real1.size())](auto        A,
                                                                              auto const &B) {
                        XCTAssert(std::equal(begin(B), end(B), next(begin(A), offset)), "");
                    },
                    real1);

            std::vector<long> long2(long1.size() * static_cast<unsigned>(comm.size()));
            comm.gather(begin(long1), end(long1), long2.data(), master);
            comm.bcast(std::move(long2), master)
                .unpack(
                    [offset = comm->rank() * static_cast<long>(long1.size())](auto        A,
                                                                              auto const &B) {
                        XCTAssert(std::equal(begin(B), end(B), next(begin(A), offset)), "");
                    },
                    long1);
        }

        {
            std::vector<double16> real2
                = comm.gather<double16>({ begin(real1), end(real1) }, master);
            real2.resize(static_cast<unsigned>(comm.size()) * real1.size());
            comm.bcast<double16>(std::move(real2), master)
                .unpack(
                    [offset = comm->rank() * static_cast<long>(real1.size())](auto        A,
                                                                              auto const &B) {
                        XCTAssert(std::equal(begin(B), end(B), next(begin(A), offset)), "");
                    },
                    real1);

            std::vector<long> long2 = comm.gather<long>({ begin(long1), end(long1) }, master);
            long2.resize(static_cast<unsigned>(comm.size()) * long1.size());
            comm.bcast(std::move(long2), master)
                .unpack(
                    [offset = comm->rank() * static_cast<long>(long1.size())](auto        A,
                                                                              auto const &B) {
                        XCTAssert(std::equal(begin(B), end(B), next(begin(A), offset)), "");
                    },
                    long1);
        }

        {
            std::vector<double16> real2(real1.size() * static_cast<unsigned>(comm.size()));
            XCTAssert(std::equal(begin(real1), end(real1),
                                 comm.all_gather(begin(real1), end(real1), real2.data())
                                     + comm->rank() * static_cast<long>(real1.size())),
                      "");

            std::vector<double16> real3(real2.size());
            std::copy(begin(real1), end(real1),
                      begin(real3) + comm->rank() * static_cast<long>(real1.size()));
            XCTAssert(std::equal(begin(real2), end(real2),
                                 comm.all_gather(real3.data(), real3.data() + real3.size())),
                      "");

            std::vector<long> long2(long1.size() * static_cast<unsigned>(comm.size()));
            XCTAssert(std::equal(begin(long1), end(long1),
                                 comm.all_gather<2>(begin(long1), end(long1), long2.data())
                                     + comm->rank() * static_cast<long>(long1.size())),
                      "");

            std::vector<long> long3(long2.size());
            std::copy(begin(long1), end(long1),
                      begin(long3) + comm->rank() * static_cast<long>(long1.size()));
            XCTAssert(std::equal(begin(long2), end(long2),
                                 comm.all_gather<2>(long3.data(), long3.data() + long3.size())),
                      "");
        }

        {
            std::vector<double16> real2(real1.size() * static_cast<unsigned>(comm.size()));
            comm.all_gather<0>({ begin(real1), end(real1) })
                .unpack(
                    [offset = comm->rank() * static_cast<long>(real1.size())](auto        A,
                                                                              auto const &B) {
                        XCTAssert(std::equal(begin(B), end(B), next(begin(A), offset)), "");
                    },
                    real1);

            std::vector<long> long2(long1.size() * static_cast<unsigned>(comm.size()));
            comm.all_gather<long>({ begin(long1), end(long1) })
                .unpack(
                    [offset = comm->rank() * static_cast<long>(long1.size())](auto        A,
                                                                              auto const &B) {
                        XCTAssert(std::equal(begin(B), end(B), next(begin(A), offset)), "");
                    },
                    long1);
        }
    } catch (std::exception const &e) {
        XCTAssert(false, e.what());
    }

    // scatter
    try {
        std::array<double16, N1> real1, real3;
        std::generate(begin(real1), end(real1), [&rng, &real] {
            return real(rng);
        });
        std::array<long, N2> long1, long3;
        std::generate(begin(long1), end(long1), [&rng, &int_] {
            return int_(rng);
        });

        {
            std::vector<double16> real2;
            for (int i = 0; i < comm.size(); ++i) {
                std::copy(begin(real1), end(real1), std::back_inserter(real2));
            }
            XCTAssert(std::equal(begin(real1), end(real1),
                                 comm.scatter<0>(real2.data(), begin(real3), end(real3), master)),
                      "");

            std::vector<long> long2;
            for (int i = 0; i < comm.size(); ++i) {
                std::copy(begin(long1), end(long1), std::back_inserter(long2));
            }
            XCTAssert(std::equal(begin(long1), end(long1),
                                 comm.scatter(long2.data(), begin(long3), end(long3), master)),
                      "");
        }
    } catch (std::exception const &e) {
        XCTAssert(false, e.what());
    }

    // all2all
    try {
        std::array<double16, N1> real1;
        std::generate(begin(real1), end(real1), [&rng, &real] {
            return real(rng);
        });
        std::array<long, N2> long1;
        std::generate(begin(long1), end(long1), [&rng, &int_] {
            return int_(rng);
        });

        std::vector<double16> real2, real3;
        for (int i = 0; i < comm.size(); ++i) {
            std::copy(begin(real1), end(real1), std::back_inserter(real2));
        }
        std::vector<long> long2, long3;
        for (int i = 0; i < comm.size(); ++i) {
            std::copy(begin(long1), end(long1), std::back_inserter(long2));
        }
        real3.resize(real2.size());
        long3.resize(long2.size());

        XCTAssert(
            std::equal(begin(real2), end(real2),
                       comm.all2all<0>(real2.data(), real2.data() + real2.size(), real3.data())),
            "");
        XCTAssert(std::equal(begin(long2), end(long2),
                             comm.all2all(long2.data(), long2.data() + long2.size(), long3.data())),
                  "");
        /* // mvapich2 crashes here
         XCTAssert(std::equal(begin(real2), end(real2), comm.all2all<0>(real3.data(), real3.data() +
         real3.size())), ""); XCTAssert(std::equal(begin(long2), end(long2),
         comm.all2all(long3.data(), long3.data() + long3.size())), "");

         comm.all2all<0>(std::move(real3)).unpack([](auto real3, auto const &real2) {
         XCTAssert(std::equal(begin(real3), end(real3), begin(real2)), "");
         }, real2);
         comm.all2all(std::move(long3)).unpack([](auto long3, auto const &long2) {
         XCTAssert(std::equal(begin(long3), end(long3), begin(long2)), "");
         }, long2);
         */
    } catch (std::exception const &e) {
        XCTAssert(false, e.what());
    }
}

} // namespace

void testCollective(Comm const &world)
{
    world.barrier();

    testCollective_LowLevel(world);
    testCommucation_Collective(world);
}
