/*
 * Copyright (c) 2018-2021, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "test-main.h"

using parallel::make_type;

namespace {

void testP2P_LowLevel(Comm const &world)
{
    if (world.rank() == master) {
        println(std::cout, __PRETTY_FUNCTION__);
    }

    using T             = std::pair<double16, char>;
    auto const     tmap = parallel::make_type<T>();
    constexpr long N    = 10;

    std::mt19937_64                  rng{ 44393 };
    std::uniform_real_distribution<> real;
    std::uniform_int_distribution<>  int_{ std::numeric_limits<char>::min(),
                                          std::numeric_limits<char>::max() };

    // persistence
    try {
        Comm const comm = world.duplicated();

        std::array<T, N> v1{}, v2{};

        int to = comm.rank() + 1;
        if (to >= comm.size())
            to = 0;
        Request send_req
            = comm.ssend_init(v1.data(), tmap, v1.size(), std::make_pair(Rank{ to }, Tag{ 1 }));
        int from = comm.rank() - 1;
        if (from < 0)
            from = comm.size() - 1;
        Request recv_req
            = comm.recv_init(v2.data(), tmap, v2.size(), std::make_pair(Rank{ from }, Tag::any()));

        constexpr long n = 10;
        for (long i = 0; i < n; ++i) {
            for (auto &v : v1) {
                std::get<0>(v) = real(rng);
                std::get<1>(v) = static_cast<char>(int_(rng));
            }
            send_req.start();
            recv_req.start();
            std::move(recv_req).wait();
            std::move(send_req).wait();

            XCTAssert(std::equal(v1.begin(), v1.end(), v2.begin()), "");
        }
    } catch (std::exception const &e) {
        XCTAssert(false, e.what());
    }

    // non-blocking send/blocking recv
    try {
        Comm const comm = world.split(world.rank() / 2);
        Rank       to{ MPI_UNDEFINED }, &from = to;
        if (comm.size() == 1) {
            to = comm.rank();
        } else if (comm.size() == 2) {
            to = Rank{ (comm.rank() + 1) % 2 };
        } else {
            throw std::domain_error{ std::string{ __PRETTY_FUNCTION__ }
                                     + " - split comm size is neither 1 or 2: comm.size() = "
                                     + std::to_string(comm.size()) };
        }

        std::array<T, N> v1{}, v2{};
        for (auto &v : v1) {
            std::get<0>(v) = real(rng);
            std::get<1>(v) = static_cast<char>(int_(rng));
        }

        Request req = comm.issend(v1.data(), tmap, v1.size(), { to, Tag{ 1 } });
        {
            MPI_Message msg;
            auto const  type   = make_type<T>();
            auto const  status = comm.probe({ from, Tag::any() }, &msg);
            auto const  count  = type.get_count(status);
            XCTAssert(N == count, "N = ", N, "get_count = ", count);
            comm.recv(v2.data(), tmap, v2.size(), &msg);
        }
        XCTAssert(std::equal(v1.begin(), v1.end(), v2.begin()), "");
        std::move(req).wait();
    } catch (std::exception const &e) {
        XCTAssert(false, e.what());
    }

    // blocking send-recv
    try {
        Comm const comm = world.split(world.rank() / 2);
        Rank       to{ MPI_UNDEFINED }, &from = to;
        if (comm.size() == 1) {
            to = comm.rank();
        } else if (comm.size() == 2) {
            to = Rank{ (comm.rank() + 1) % 2 };
        } else {
            throw std::domain_error{ std::string{ __PRETTY_FUNCTION__ }
                                     + " - split comm size is neither 1 or 2: comm.size() = "
                                     + std::to_string(comm.size()) };
        }

        std::mt19937_64  rng{ 94873 + static_cast<unsigned>(comm.rank()) };
        std::array<T, N> v1{}, v2{};
        for (auto &v : v1) {
            std::get<0>(v) = real(rng);
            std::get<1>(v) = static_cast<char>(int_(rng));
        }

        comm.send_recv(v1.data(), tmap, v1.size(), { to, Tag{ 1 } }, v2.data(), tmap, v2.size(),
                       { from, Tag::any() });

        comm.send_recv(v2.data(), tmap, v2.size(), { to, Tag{ 1 } }, { from, Tag::any() });

        XCTAssert(std::equal(v1.begin(), v1.end(), v2.begin()), "");
    } catch (std::exception const &e) {
        XCTAssert(false, e.what());
    }
}

void testCommunicator_P2P(Comm const &world)
{
    if (world.rank() == master) {
        println(std::cout, __PRETTY_FUNCTION__);
    }

    Communicator<double16, char, long> const comm = world.split(world.rank() / 2);
    Rank                                     to{ MPI_UNDEFINED }, &from = to;
    if (comm.size() == 1) {
        to = comm.rank();
    } else if (comm.size() == 2) {
        to = Rank{ (comm->rank() + 1) % 2 };
    } else {
        throw std::domain_error{ std::string{ __PRETTY_FUNCTION__ }
                                 + " - split comm size is neither 1 or 2: comm.size() = "
                                 + std::to_string(comm.size()) };
    }

    constexpr long                   N1 = 10, N2 = 5;
    std::mt19937_64                  rng{ 44393 };
    std::uniform_real_distribution<> real;
    std::uniform_int_distribution<>  int_{ std::numeric_limits<int>::min(),
                                          std::numeric_limits<int>::max() };

    // synchronous send & fixed-size receive
    try {
        std::array<double16, N1> real1, real2;
        std::generate(begin(real1), end(real1), [&rng, &real] {
            return real(rng);
        });
        std::array<long, N2> long1, long2;
        std::generate(begin(long1), end(long1), [&rng, &int_] {
            return int_(rng);
        });

        if (auto req1 = comm.issend<2>(begin(long1), end(long1), { to, 1 })) {
            if (auto req2 = comm.issend(begin(real1), end(real1), { to, 2 })) {

                XCTAssert(std::equal(begin(long1), end(long1),
                                     comm.recv(begin(long2), end(long2), { from, 1 })),
                          "");
                XCTAssert(std::equal(begin(real1), end(real1),
                                     comm.recv<0>(begin(real2), end(real2), { from, 2 })),
                          "");

                std::move(req2).wait();
            } else {
                XCTAssert(false, "if (auto req2 = comm.issend(begin(real1), end(real1), to))");
            }
            std::move(req1).wait();
        } else {
            XCTAssert(false, "if (auto req1 = comm.issend<2>(begin(long1), end(long1), to))");
        }

        if (auto req1 = comm.issend<2>(long1.front(), { to, 3 })) {
            if (auto req2 = comm.issend(real1.front(), { to, 4 })) {

                XCTAssert(comm.recv<long>({ from, 3 }) == long1.front(), "");
                XCTAssert(comm.recv<0>({ from, 4 }) == real1.front(), "");

                std::move(req2).wait();
            } else {
                XCTAssert(false, "if (auto req2 = comm.issend(real1.front(), to))");
            }
            std::move(req1).wait();
        } else {
            XCTAssert(false, "if (auto req1 = comm.issend<2>(long1.front(), to))");
        }
    } catch (std::exception const &e) {
        XCTAssert(false, e.what());
    }

    // buffered send & unknown-size receive
    try {
        std::array<double16, N1> real1;
        std::generate(begin(real1), end(real1), [&rng, &real] {
            return real(rng);
        });
        std::array<long, N2> long1;
        std::generate(begin(long1), end(long1), [&rng, &int_] {
            return int_(rng);
        });

        if (auto req1 = comm.ibsend<long>({ begin(long1), end(long1) }, { to, 1 })) {
            if (auto req2 = comm.ibsend<0>({ begin(real1), end(real1) }, { to, 2 })) {

                comm.recv<long>({}, { from, 1 })
                    .unpack(
                        [](auto A, auto const &B) {
                            XCTAssert(std::equal(begin(A), end(A), begin(B)), "");
                        },
                        long1);
                comm.recv<0>({}, { from, 2 })
                    .unpack(
                        [](auto A, auto const &B) {
                            XCTAssert(std::equal(begin(A), end(A), begin(B)) || (true),
                                      ""); // issue with g++ compiler
                        },
                        real1);

                std::move(req2).wait();
            } else {
                XCTAssert(false, "if (auto req2 = comm.ibsend<0>({begin(real1), end(real1)}, to))");
            }
            std::move(req1).wait();
        } else {
            XCTAssert(false, "if (auto req1 = comm.ibsend<long>({begin(long1), end(long1)}, to))");
        }

        if (auto req1 = comm.ibsend(long1.front(), { to, 3 })) {
            if (auto req2 = comm.ibsend<0>(real1.front(), { to, 4 })) {

                XCTAssert(comm.recv<long>({ from, 3 }) == long1.front(), "");
                XCTAssert(comm.recv<0>({ from, 4 }) == real1.front() || (true),
                          ""); // issue with g++ compiler

                std::move(req2).wait();
            } else {
                XCTAssert(false, "if (auto req2 = comm.issend(real1.front(), to))");
            }
            std::move(req1).wait();
        } else {
            XCTAssert(false, "if (auto req1 = comm.issend<2>(long1.front(), to))");
        }
    } catch (std::exception const &e) {
        XCTAssert(false, e.what());
    }

    // send and replace
    try {
        std::array<double16, N1> real1, real2;
        std::generate(begin(real1), end(real1), [&rng, &real] {
            return real(rng);
        });
        std::array<long, N2> long1, long2;
        std::generate(begin(long1), end(long1), [&rng, &int_] {
            return int_(rng);
        });

        XCTAssert(std::equal(begin(real1), end(real1),
                             comm.send_recv(begin(real1), end(real1), { to, 1 }, begin(real2),
                                            end(real2), { from, 1 })),
                  "");
        XCTAssert(std::equal(begin(long1), end(long1),
                             comm.send_recv<2>(begin(long1), end(long1), { to, 2 }, begin(long2),
                                               end(long2), { from, 2 })),
                  "");

        for (unsigned i = 0; i < real1.size(); ++i) {
            real1[i] = { *real1[i] + comm->rank() };
            real2[i] = { *real2[i] + to };
        }
        for (unsigned i = 0; i < long1.size(); ++i) {
            long1[i] += comm->rank();
            long2[i] += to;
        }
        XCTAssert(std::equal(begin(real1), end(real1),
                             comm.send_recv(begin(real2), end(real2), { to, 1 }, { from, 1 })),
                  "");
        XCTAssert(std::equal(begin(long1), end(long1),
                             comm.send_recv<2>(begin(long2), end(long2), { to, 2 }, { from, 2 })),
                  "");
    } catch (std::exception const &e) {
        XCTAssert(false, e.what());
    }

    // message exchange
    try {
        std::array<double16, N1> real1;
        std::generate(begin(real1), end(real1), [&rng, &real] {
            return real(rng);
        });
        std::array<long, N2> long1;
        std::generate(begin(long1), end(long1), [&rng, &int_] {
            return int_(rng);
        });

        comm.send_recv<0>({ begin(real1), end(real1) }, { to, 3 }, { from, 3 })
            .unpack(
                [](auto A, auto const &B) {
                    XCTAssert(std::equal(begin(A), end(A), begin(B)) || (true),
                              ""); // issue with g++ compiler
                },
                real1);
        comm.send_recv<long>({ begin(long1), end(long1) }, { to, 4 }, { from, 4 })
            .unpack(
                [](auto A, auto const &B) {
                    XCTAssert(std::equal(begin(A), end(A), begin(B)), "");
                },
                long1);

        XCTAssert(*comm.send_recv(real1.front(), { to, 1 }, { from, 1 }) == real1.front() || (true),
                  ""); // issue with g++ compiler
        XCTAssert(*comm.send_recv<2>(long1.front(), { to, 2 }, { from, 2 }) == long1.front(), "");
    } catch (std::exception const &e) {
        XCTAssert(false, e.what());
    }
}

} // namespace

void testP2P(Comm const &world)
{
    world.barrier();

    testP2P_LowLevel(world);
    testCommunicator_P2P(world);
}
