/*
 * Copyright (c) 2018-2022, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "test-main.h"

namespace {

void testReduction_ReduceOp(Comm const &world)
{
    if (world.rank() == master) {
        println(std::cout, __PRETTY_FUNCTION__);
    }

    try {
        ReduceOp op1, op2;
        XCTAssert(!op1 && !op2, "");

        op1 = ReduceOp::max();
        XCTAssert(op1 && MPI_MAX == *op1, "");
        op2 = std::move(op1);
        XCTAssert(!op1 && op2 && MPI_MAX == *op2, "");

        op1 = ReduceOp::max();
        XCTAssert(op1 && op1.commutative() && MPI_MAX == *op1, "");
        op1 = ReduceOp::min();
        XCTAssert(op1 && op1.commutative() && MPI_MIN == *op1, "");
        op1 = ReduceOp::plus();
        XCTAssert(op1 && op1.commutative() && MPI_SUM == *op1, "");
        op1 = ReduceOp::prod();
        XCTAssert(op1 && op1.commutative() && MPI_PROD == *op1, "");
        op1 = ReduceOp::logic_and();
        XCTAssert(op1 && op1.commutative() && MPI_LAND == *op1, "");
        op1 = ReduceOp::bit_and();
        XCTAssert(op1 && op1.commutative() && MPI_BAND == *op1, "");
        op1 = ReduceOp::logic_or();
        XCTAssert(op1 && op1.commutative() && MPI_LOR == *op1, "");
        op1 = ReduceOp::bit_or();
        XCTAssert(op1 && op1.commutative() && MPI_BOR == *op1, "");
        op1 = ReduceOp::logic_xor();
        XCTAssert(op1 && op1.commutative() && MPI_LXOR == *op1, "");
        op1 = ReduceOp::bit_xor();
        XCTAssert(op1 && op1.commutative() && MPI_BXOR == *op1, "");
        op1 = ReduceOp::loc_min();
        XCTAssert(op1 && op1.commutative() && MPI_MINLOC == *op1, "");
        op1 = ReduceOp::loc_max();
        XCTAssert(op1 && op1.commutative() && MPI_MAXLOC == *op1, "");

        op1 = ReduceOp(false, [](void *, void *, int *, MPI_Datatype *) -> void {});
        XCTAssert(op1 && !op1.commutative(), "");
    } catch (std::exception const &e) {
        XCTAssert(false, e.what());
    }
}

void testReduction_LowLevel(Comm const &world)
{
    if (world.rank() == master) {
        println(std::cout, __PRETTY_FUNCTION__);
    }
    Comm const comm = world.duplicated();

    std::mt19937_64                  rng{ 44393 };
    std::uniform_real_distribution<> real;
    std::uniform_int_distribution<>  int_{ std::numeric_limits<char>::min(),
                                          std::numeric_limits<char>::max() };

    constexpr long N    = 100;
    using T             = std::pair<double16, short>;
    auto const     tmap = parallel::make_type<T>();
    ReduceOp const op(true, [](void *_a, void *_b, int *n, MPI_Datatype *) {
        T const *a = static_cast<T const *>(_a);
        T       *b = static_cast<T *>(_b);
        for (int i = 0; i < *n; ++i) {
            std::get<0>(b[i]) = std::get<0>(a[i]) + std::get<0>(b[i]);
            std::get<1>(b[i]) = std::get<1>(a[i]) + std::get<1>(b[i]);
        }
    });
    constexpr auto is_equal = [](T const &A, T const &B) noexcept {
        constexpr double eps = 1e-15;
        auto const [A1, A2]  = A;
        auto const [B1, B2]  = B;
        return A2 == B2 && (*A1 - *B1) < eps * *A1;
    };

    // reduce
    try {
        std::array<T, N> v1{}, v2{};
        std::vector<T>   v3{};
        for (auto &v : v1) {
            std::get<0>(v) = real(rng);
            std::get<1>(v) = static_cast<char>(int_(rng));
            //
            std::vector<T> const tmp(static_cast<unsigned>(comm.size()), v);
            auto const           sum
                = std::accumulate(tmp.begin(), tmp.end(), T{}, [](T const &a, T const &b) {
                      return std::make_pair(std::get<0>(a) + std::get<0>(b),
                                            std::get<1>(a) + std::get<1>(b));
                  });
            v3.push_back(sum);
        }

        comm.reduce(op, v1.data(), v2.data(), tmap, v1.size(), master);
        comm.bcast(v2.data(), tmap, v2.size(), master);
        XCTAssert(std::equal(v3.begin(), v3.end(), v2.begin(), is_equal), "");

        if (master == comm.rank()) {
            v2 = v1;
            comm.reduce(op, v2.data(), tmap, v2.size());
        } else {
            comm.reduce(op, v1.data(), tmap, v1.size(), master);
        }
        comm.bcast(v2.data(), tmap, v2.size(), master);
        XCTAssert(std::equal(v3.begin(), v3.end(), v2.begin(), is_equal), "");

        std::fill(v2.begin(), v2.end(), T{});
        comm.all_reduce(op, v1.data(), v2.data(), tmap, v1.size());
        XCTAssert(std::equal(v3.begin(), v3.end(), v2.begin(), is_equal), "");

        v2 = v1;
        comm.all_reduce(op, v2.data(), tmap, v2.size());
        XCTAssert(std::equal(v3.begin(), v3.end(), v2.begin(), is_equal), "");
    } catch (std::exception const &e) {
        XCTAssert(false, e.what());
    }

    // all_reduce
    try {
        T v1, v2, v3;
        {
            std::get<0>(v1) = real(rng);
            std::get<1>(v1) = static_cast<char>(int_(rng));
            //
            std::vector<T> const tmp(static_cast<unsigned>(comm.size()), v1);
            v3 = std::accumulate(tmp.begin(), tmp.end(), T{}, [](T const &a, T const &b) {
                return std::make_pair(std::get<0>(a) + std::get<0>(b),
                                      std::get<1>(a) + std::get<1>(b));
            });
        }

        comm.all_reduce(op, &(v2 = v1), tmap, 1);
        XCTAssert(is_equal(v3, v2), "");
    } catch (std::exception const &e) {
        XCTAssert(false, e.what());
    }
}

void testCommunicator_Reduction(Comm const &world)
{
    if (world.rank() == master) {
        println(std::cout, __PRETTY_FUNCTION__);
    }
    Communicator<double16, char, long> const comm = world.duplicated();

    constexpr long                   N1 = 10, N2 = 5;
    std::mt19937_64                  rng{ 44393 };
    std::uniform_real_distribution<> real;
    std::uniform_int_distribution<>  int_{ std::numeric_limits<char>::min(),
                                          std::numeric_limits<char>::max() };

    auto const     plus_double16 = ReduceOp::plus<double16>(true);
    constexpr auto is_equal      = [](double16 const &A, double16 const &B) noexcept {
        constexpr double eps = 1e-15;
        return (*A - *B) < eps * *A;
    };

    // reduce
    try {
        std::array<double16, N1> real1, real2;
        std::generate(begin(real1), end(real1), [&rng, &real] {
            return real(rng);
        });
        std::array<long, N2> long1, long2;
        std::generate(begin(long1), end(long1), [&rng, &int_] {
            return int_(rng);
        });

        std::vector<double16> real3;
        std::transform(begin(real1), end(real1), std::back_inserter(real3),
                       [mul = comm.size()](auto const &v) {
                           return mul * *v;
                       });
        std::vector<long> long3;
        std::transform(begin(long1), end(long1), std::back_inserter(long3),
                       [mul = comm.size()](auto const &v) {
                           return mul * v;
                       });

        {
            comm.reduce<0>(plus_double16, begin(real1), end(real1), begin(real2), master);
            XCTAssert(std::equal(begin(real3), end(real3),
                                 comm.bcast(begin(real2), end(real2), master), is_equal),
                      "");

            comm.reduce(std::plus(), begin(long1), end(long1), begin(long2), master);
            XCTAssert(std::equal(begin(long3), end(long3),
                                 comm.bcast<2>(begin(long2), end(long2), master)),
                      "");
        }

        {
            comm.bcast<0>(comm.reduce<0>(plus_double16, { begin(real1), end(real1) }, master),
                          master)
                .unpack(
                    [is_equal](auto real2, auto const &real3) {
                        XCTAssert(std::equal(begin(real2), end(real2), begin(real3), is_equal), "");
                    },
                    real3);

            comm.bcast<long>(
                    comm.reduce<std::plus<>, long>({}, { begin(long1), end(long1) }, master),
                    master)
                .unpack(
                    [](auto long2, auto const &long3) {
                        XCTAssert(std::equal(begin(long2), end(long2), begin(long3)), "");
                    },
                    long3);
        }

        real2.fill({});
        long2.fill({});
        {
            XCTAssert(std::equal(
                          begin(real3), end(real3),
                          comm.all_reduce<0>(plus_double16, begin(real1), end(real1), begin(real2)),
                          is_equal),
                      "");

            XCTAssert(
                std::equal(begin(long3), end(long3),
                           comm.all_reduce(std::plus(), begin(long1), end(long1), begin(long2))),
                "");
        }
        real2 = real1;
        long2 = long1;
        {
            XCTAssert(std::equal(begin(real3), end(real3),
                                 comm.all_reduce<0>(plus_double16, begin(real2), end(real2)),
                                 is_equal),
                      "");

            XCTAssert(std::equal(begin(long3), end(long3),
                                 comm.all_reduce(std::plus(), begin(long2), end(long2))),
                      "");
        }

        {
            comm.all_reduce<0>(plus_double16, { begin(real1), end(real1) })
                .unpack(
                    [is_equal](auto real2, auto const &real3) {
                        XCTAssert(std::equal(begin(real2), end(real2), begin(real3), is_equal), "");
                    },
                    real3);
            comm.all_reduce<std::plus<>, long>({}, { begin(long1), end(long1) })
                .unpack(
                    [](auto long2, auto const &long3) {
                        XCTAssert(std::equal(begin(long2), end(long2), begin(long3)), "");
                    },
                    long3);
        }

        XCTAssert(is_equal(comm.all_reduce<0>(plus_double16, real1.front()), real3.front()), "");
        auto const result = *comm.all_reduce<std::plus<>, long>({}, long1.front());
        XCTAssert(result == long3.front(), "");
    } catch (std::exception const &e) {
        XCTAssert(false, e.what());
    }
}

} // namespace

void testReduction(Comm const &world)
{
    world.barrier();

    testReduction_ReduceOp(world);
    testReduction_LowLevel(world);
    testCommunicator_Reduction(world);
}
