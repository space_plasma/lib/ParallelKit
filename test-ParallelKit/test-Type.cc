/*
 * Copyright (c) 2018-2021, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "test-main.h"

using parallel::make_type;

namespace {

void testType_Basic(Comm const &world)
{
    if (world.rank() == master) {
        println(std::cout, __PRETTY_FUNCTION__);
    }

    try {
        auto t0 = make_type<char>();
        XCTAssert(t0 && MPI_CHAR == *t0 && t0.alignment() == 1, "");

        auto t1 = std::move(t0);
        XCTAssert(!t0 && t0.alignment() == 0 && t1 && MPI_CHAR == *t1 && t1.alignment() == 1, "");

        t0 = std::move(t1);
        XCTAssert(!t1 && t1.alignment() == 0 && t0 && MPI_CHAR == *t0 && t0.alignment() == 1, "");
    } catch (std::exception const &e) {
        XCTAssert(false, e.what());
    }
}

void testType_NativeChar(Comm const &world)
{
    if (world.rank() == master) {
        println(std::cout, __PRETTY_FUNCTION__);
    }

    try {
        using T      = char;
        auto const t = make_type<T>();
        XCTAssert(t && *t == MPI_CHAR && t.alignment() == alignof(T), "");

        long const sig_size = t.signature_size();
        XCTAssert(sig_size == sizeof(T), "sig_size = ", sig_size);

        auto [lb, extent] = t.extent();
        XCTAssert(lb == 0, "lb = ", lb);
        XCTAssert(extent == sizeof(T), "extent = ", extent);

        std::tie(lb, extent) = t.true_extent();
        XCTAssert(lb == 0, "true lb = ", lb);
        XCTAssert(extent == sizeof(T), "true extent = ", extent);
    } catch (std::exception const &e) {
        XCTAssert(false, e.what());
    }
}

void testType_NativeUnsignedChar(Comm const &world)
{
    if (world.rank() == master) {
        println(std::cout, __PRETTY_FUNCTION__);
    }

    try {
        using T      = unsigned char;
        auto const t = make_type<T>();
        XCTAssert(t && *t == MPI_UNSIGNED_CHAR && t.alignment() == alignof(T), "");

        long const sig_size = t.signature_size();
        XCTAssert(sig_size == sizeof(T), "sig_size = ", sig_size);

        auto [lb, extent] = t.extent();
        XCTAssert(lb == 0, "lb = ", lb);
        XCTAssert(extent == sizeof(T), "extent = ", extent);

        std::tie(lb, extent) = t.true_extent();
        XCTAssert(lb == 0, "true lb = ", lb);
        XCTAssert(extent == sizeof(T), "true extent = ", extent);
    } catch (std::exception const &e) {
        XCTAssert(false, e.what());
    }
}

void testType_NativeShort(Comm const &world)
{
    if (world.rank() == master) {
        println(std::cout, __PRETTY_FUNCTION__);
    }

    try {
        using T      = short;
        auto const t = make_type<T>();
        XCTAssert(t && *t == MPI_SHORT && t.alignment() == alignof(T), "");

        long const sig_size = t.signature_size();
        XCTAssert(sig_size == sizeof(T), "sig_size = ", sig_size);

        auto [lb, extent] = t.extent();
        XCTAssert(lb == 0, "lb = ", lb);
        XCTAssert(extent == sizeof(T), "extent = ", extent);

        std::tie(lb, extent) = t.true_extent();
        XCTAssert(lb == 0, "true lb = ", lb);
        XCTAssert(extent == sizeof(T), "true extent = ", extent);
    } catch (std::exception const &e) {
        XCTAssert(false, e.what());
    }
}
void testType_NativeUnsignedShort(Comm const &world)
{
    if (world.rank() == master) {
        println(std::cout, __PRETTY_FUNCTION__);
    }

    try {
        using T      = unsigned short;
        auto const t = make_type<T>();
        XCTAssert(t && *t == MPI_UNSIGNED_SHORT && t.alignment() == alignof(T), "");

        long const sig_size = t.signature_size();
        XCTAssert(sig_size == sizeof(T), "sig_size = ", sig_size);

        auto [lb, extent] = t.extent();
        XCTAssert(lb == 0, "lb = ", lb);
        XCTAssert(extent == sizeof(T), "extent = ", extent);

        std::tie(lb, extent) = t.true_extent();
        XCTAssert(lb == 0, "true lb = ", lb);
        XCTAssert(extent == sizeof(T), "true extent = ", extent);
    } catch (std::exception const &e) {
        XCTAssert(false, e.what());
    }
}

void testType_NativeInt(Comm const &world)
{
    if (world.rank() == master) {
        println(std::cout, __PRETTY_FUNCTION__);
    }

    try {
        using T      = int;
        auto const t = make_type<T>();
        XCTAssert(t && *t == MPI_INT && t.alignment() == alignof(T), "");

        long const sig_size = t.signature_size();
        XCTAssert(sig_size == sizeof(T), "sig_size = ", sig_size);

        auto [lb, extent] = t.extent();
        XCTAssert(lb == 0, "lb = ", lb);
        XCTAssert(extent == sizeof(T), "extent = ", extent);

        std::tie(lb, extent) = t.true_extent();
        XCTAssert(lb == 0, "true lb = ", lb);
        XCTAssert(extent == sizeof(T), "true extent = ", extent);
    } catch (std::exception const &e) {
        XCTAssert(false, e.what());
    }
}
void testType_NativeUnsignedInt(Comm const &world)
{
    if (world.rank() == master) {
        println(std::cout, __PRETTY_FUNCTION__);
    }

    try {
        using T      = unsigned int;
        auto const t = make_type<T>();
        XCTAssert(t && *t == MPI_UNSIGNED && t.alignment() == alignof(T), "");

        long const sig_size = t.signature_size();
        XCTAssert(sig_size == sizeof(T), "sig_size = ", sig_size);

        auto [lb, extent] = t.extent();
        XCTAssert(lb == 0, "lb = ", lb);
        XCTAssert(extent == sizeof(T), "extent = ", extent);

        std::tie(lb, extent) = t.true_extent();
        XCTAssert(lb == 0, "true lb = ", lb);
        XCTAssert(extent == sizeof(T), "true extent = ", extent);
    } catch (std::exception const &e) {
        XCTAssert(false, e.what());
    }
}

void testType_NativeLong(Comm const &world)
{
    if (world.rank() == master) {
        println(std::cout, __PRETTY_FUNCTION__);
    }

    try {
        using T      = long;
        auto const t = make_type<T>();
        XCTAssert(t && *t == MPI_LONG && t.alignment() == alignof(T), "");

        long const sig_size = t.signature_size();
        XCTAssert(sig_size == sizeof(T), "sig_size = ", sig_size);

        auto [lb, extent] = t.extent();
        XCTAssert(lb == 0, "lb = ", lb);
        XCTAssert(extent == sizeof(T), "extent = ", extent);

        std::tie(lb, extent) = t.true_extent();
        XCTAssert(lb == 0, "true lb = ", lb);
        XCTAssert(extent == sizeof(T), "true extent = ", extent);
    } catch (std::exception const &e) {
        XCTAssert(false, e.what());
    }
}
void testType_NativeUnsignedLong(Comm const &world)
{
    if (world.rank() == master) {
        println(std::cout, __PRETTY_FUNCTION__);
    }

    try {
        using T      = unsigned long;
        auto const t = make_type<T>();
        XCTAssert(t && *t == MPI_UNSIGNED_LONG && t.alignment() == alignof(T), "");

        long const sig_size = t.signature_size();
        XCTAssert(sig_size == sizeof(T), "sig_size = ", sig_size);

        auto [lb, extent] = t.extent();
        XCTAssert(lb == 0, "lb = ", lb);
        XCTAssert(extent == sizeof(T), "extent = ", extent);

        std::tie(lb, extent) = t.true_extent();
        XCTAssert(lb == 0, "true lb = ", lb);
        XCTAssert(extent == sizeof(T), "true extent = ", extent);
    } catch (std::exception const &e) {
        XCTAssert(false, e.what());
    }
}

void testType_NativeFloat(Comm const &world)
{
    if (world.rank() == master) {
        println(std::cout, __PRETTY_FUNCTION__);
    }

    try {
        using T      = float;
        auto const t = make_type<T>();
        XCTAssert(t && *t == MPI_FLOAT && t.alignment() == alignof(T), "");

        long const sig_size = t.signature_size();
        XCTAssert(sig_size == sizeof(T), "sig_size = ", sig_size);

        auto [lb, extent] = t.extent();
        XCTAssert(lb == 0, "lb = ", lb);
        XCTAssert(extent == sizeof(T), "extent = ", extent);

        std::tie(lb, extent) = t.true_extent();
        XCTAssert(lb == 0, "true lb = ", lb);
        XCTAssert(extent == sizeof(T), "true extent = ", extent);
    } catch (std::exception const &e) {
        XCTAssert(false, e.what());
    }
}
void testType_NativeDouble(Comm const &world)
{
    if (world.rank() == master) {
        println(std::cout, __PRETTY_FUNCTION__);
    }

    try {
        using T      = double;
        auto const t = make_type<T>();
        XCTAssert(t && *t == MPI_DOUBLE && t.alignment() == alignof(T), "");

        long const sig_size = t.signature_size();
        XCTAssert(sig_size == sizeof(T), "sig_size = ", sig_size);

        auto [lb, extent] = t.extent();
        XCTAssert(lb == 0, "lb = ", lb);
        XCTAssert(extent == sizeof(T), "extent = ", extent);

        std::tie(lb, extent) = t.true_extent();
        XCTAssert(lb == 0, "true lb = ", lb);
        XCTAssert(extent == sizeof(T), "true extent = ", extent);
    } catch (std::exception const &e) {
        XCTAssert(false, e.what());
    }
}

void testType_Realignment(Comm const &world)
{
    if (world.rank() == master) {
        println(std::cout, __PRETTY_FUNCTION__);
    }

    try {
        using T                  = double;
        constexpr long alignment = alignof(T) * 2, new_ext = sizeof(T) * 2;

        auto const t = make_type<T>().realigned(alignment);
        XCTAssert(t && t.alignment() == alignment, "");

        long const sig_size = t.signature_size();
        XCTAssert(sig_size == sizeof(T), "sig_size = ", sig_size);

        auto [lb, extent] = t.extent();
        XCTAssert(lb == 0, "lb = ", lb);
        XCTAssert(extent == new_ext, "extent = ", extent);

        std::tie(lb, extent) = t.true_extent();
        XCTAssert(lb == 0, "true lb = ", lb);
        XCTAssert(extent == sizeof(T), "true extent = ", extent);
    } catch (std::exception const &e) {
        XCTAssert(false, e.what());
    }
}

void testType_Composite(Comm const &world)
{
    if (world.rank() == master) {
        println(std::cout, __PRETTY_FUNCTION__);
    }

    try {
        using T = std::pair<double, char>;

        auto t = make_type<T>();
        XCTAssert(t && t.alignment() == alignof(T), "");

        long const sig_size = t.signature_size();
        XCTAssert(sizeof(std::tuple_element_t<0, T>) + sizeof(std::tuple_element_t<1, T>)
                      == sig_size,
                  "sig_size = ", sig_size);

        auto [lb, extent] = t.extent();
        XCTAssert(lb == 0, "lb = ", lb);
        XCTAssert(extent == sizeof(T), "extent = ", extent);

        std::tie(lb, extent) = t.true_extent();
        XCTAssert(lb == 0, "true lb = ", lb);
        XCTAssert(sizeof(std::tuple_element_t<0, T>) + sizeof(std::tuple_element_t<1, T>) == extent,
                  "true extent = ", extent);

        auto const t2 = std::move(t);
        {
            XCTAssert(t2 && t2.alignment() == alignof(T), "");

            long const sig_size = t2.signature_size();
            XCTAssert(sizeof(std::tuple_element_t<0, T>) + sizeof(std::tuple_element_t<1, T>)
                          == sig_size,
                      "sig_size = ", sig_size);

            auto [lb, extent] = t2.extent();
            XCTAssert(lb == 0, "lb = ", lb);
            XCTAssert(extent == sizeof(T), "extent = ", extent);

            std::tie(lb, extent) = t2.true_extent();
            XCTAssert(lb == 0, "true lb = ", lb);
            XCTAssert(sizeof(std::tuple_element_t<0, T>) + sizeof(std::tuple_element_t<1, T>)
                          == extent,
                      "true extent = ", extent);
        }
    } catch (std::exception const &e) {
        XCTAssert(false, e.what());
    }
    try {
        using T = std::pair<std::pair<std::pair<char, double>, char>, short>;

        auto const t = make_type<T>();
        XCTAssert(t && t.alignment() == alignof(T), "");

        long const sig_size = t.signature_size();
        XCTAssert(sizeof(char) + sizeof(double) + sizeof(char) + sizeof(short) == sig_size,
                  "sig_size = ", sig_size);

        auto [lb, extent] = t.extent();
        XCTAssert(lb == 0, "lb = ", lb);
        XCTAssert(extent == sizeof(T), "extent = ", extent);

        std::tie(lb, extent) = t.true_extent();
        XCTAssert(lb == 0, "true lb = ", lb);
        XCTAssert(sizeof(double) * 3 + sizeof(short) == extent, "true extent = ", extent);
    } catch (std::exception const &e) {
        XCTAssert(false, e.what());
    }
}

void testType_Vector(Comm const &world)
{
    if (world.rank() == master) {
        println(std::cout, __PRETTY_FUNCTION__);
    }

    try {
        using T                = double;
        constexpr int n_blocks = 2, blocksize = 2, stride = 3;

        auto const t = make_type<T>().vector<n_blocks, blocksize, stride>();
        XCTAssert(t && t.alignment() == alignof(T), "");

        long const sig_size = t.signature_size();
        XCTAssert(sig_size == sizeof(T) * n_blocks * blocksize, "sig_size = ", sig_size);

        auto [lb, extent] = t.extent();
        XCTAssert(lb == 0, "lb = ", lb);
        XCTAssert(extent == sizeof(T) * ((n_blocks - 1) * stride + blocksize), "extent = ", extent);

        std::tie(lb, extent) = t.true_extent();
        XCTAssert(lb == 0, "true lb = ", lb);
        XCTAssert(extent == sizeof(T) * ((n_blocks - 1) * stride + blocksize),
                  "true extent = ", extent);
    } catch (std::exception const &e) {
        XCTAssert(false, e.what());
    }
}

void testType_Indexed(Comm const &world)
{
    if (world.rank() == master) {
        println(std::cout, __PRETTY_FUNCTION__);
    }

    try {
        using T                                      = std::pair<double, char>;
        constexpr int                       n_blocks = 2;
        constexpr std::array<int, n_blocks> blocksizes{ 3, 1 }, displacements{ 4, 0 };

        auto const t = make_type<T>().indexed<n_blocks>(blocksizes, displacements);
        XCTAssert(t && t.alignment() == alignof(T), "");

        long const sig_size = t.signature_size();
        XCTAssert(sig_size == (sizeof(double) + sizeof(char)) * 4, "sig_size = ", sig_size);

        auto [lb, extent] = t.extent();
        XCTAssert(lb == 0, "lb = ", lb);
        XCTAssert(extent == 112, "extent = ", extent);

        std::tie(lb, extent) = t.true_extent();
        XCTAssert(lb == 0, "true lb = ", lb);
        XCTAssert(extent == 112 - 7, "true extent = ", extent);
    } catch (std::exception const &e) {
        XCTAssert(false, e.what());
    }
}

void testType_IndexedBlock(Comm const &world)
{
    if (world.rank() == master) {
        println(std::cout, __PRETTY_FUNCTION__);
    }

    try {
        using T                                      = std::pair<double, char>;
        constexpr int                       n_blocks = 2, blocksize = 2;
        constexpr std::array<int, n_blocks> displacements{ 4, 0 };

        auto const t = make_type<T>().indexed<n_blocks, blocksize>(displacements);
        XCTAssert(t && t.alignment() == alignof(T), "");

        long const sig_size = t.signature_size();
        XCTAssert(sig_size == (sizeof(double) + sizeof(char)) * 4, "sig_size = ", sig_size);

        auto [lb, extent] = t.extent();
        XCTAssert(lb == 0, "lb = ", lb);
        XCTAssert(extent == 64 + sizeof(T) * 1 * blocksize, "extent = ", extent);

        std::tie(lb, extent) = t.true_extent();
        XCTAssert(lb == 0, "true lb = ", lb);
        XCTAssert(extent == 64 + sizeof(T) * 1 * blocksize - 7, "true extent = ", extent);
    } catch (std::exception const &e) {
        XCTAssert(false, e.what());
    }
}

void testType_Subarray(Comm const &world)
{
    if (world.rank() == master) {
        println(std::cout, __PRETTY_FUNCTION__);
    }

    try {
        using T                          = std::pair<double, char>;
        constexpr int                 ND = 2;
        constexpr std::array<int, ND> dims{ 3, 2 }, locs{ 1, 1 }, lens{ 1, 1 };

        auto const t = make_type<T>().subarray<ND>(dims, locs, lens);
        XCTAssert(t && t.alignment() == alignof(T), "");

        long const sig_size = t.signature_size();
        XCTAssert(sig_size == (sizeof(double) + sizeof(char)), "sig_size = ", sig_size);

        auto [lb, extent] = t.extent();
        XCTAssert(lb == 0, "lb = ", lb);
        XCTAssert(extent == sizeof(T) * 3 * 2, "extent = ", extent);

        std::tie(lb, extent) = t.true_extent();
        XCTAssert(lb == sizeof(T) * 3, "true lb = ", lb);
        XCTAssert(extent == sig_size, "true extent = ", extent);
    } catch (std::exception const &e) {
        XCTAssert(false, e.what());
    }
}

void testType_StdComplex(Comm const &world)
{
    if (world.rank() == master) {
        println(std::cout, __PRETTY_FUNCTION__);
    }

    try {
        using T = std::complex<double>;

        auto const t = make_type<T>();
        XCTAssert(t && t.alignment() == alignof(T), "");

        long const sig_size = t.signature_size();
        XCTAssert(sig_size == sizeof(T), "sig_size = ", sig_size);

        auto [lb, extent] = t.extent();
        XCTAssert(lb == 0, "lb = ", lb);
        XCTAssert(extent == sizeof(T), "extent = ", extent);

        std::tie(lb, extent) = t.true_extent();
        XCTAssert(lb == 0, "true lb = ", lb);
        XCTAssert(extent == sizeof(T), "true extent = ", extent);
    } catch (std::exception const &e) {
        XCTAssert(false, e.what());
    }
}

void testType_StdArray(Comm const &world)
{
    if (world.rank() == master) {
        println(std::cout, __PRETTY_FUNCTION__);
    }

    try {
        using T = std::array<double, 3>;

        auto const t = make_type<T>();
        XCTAssert(t && t.alignment() == alignof(T), "");

        long const sig_size = t.signature_size();
        XCTAssert(sig_size == sizeof(T), "sig_size = ", sig_size);

        auto [lb, extent] = t.extent();
        XCTAssert(lb == 0, "lb = ", lb);
        XCTAssert(extent == sizeof(T), "extent = ", extent);

        std::tie(lb, extent) = t.true_extent();
        XCTAssert(lb == 0, "true lb = ", lb);
        XCTAssert(extent == sizeof(T), "true extent = ", extent);
    } catch (std::exception const &e) {
        XCTAssert(false, e.what());
    }
}

void testType_SomeType(Comm const &world)
{
    if (world.rank() == master) {
        println(std::cout, __PRETTY_FUNCTION__);
    }

    try {
        using T = std::pair<double16::native_type, double16>;

        auto const t = make_type<T>();
        XCTAssert(t && t.alignment() == alignof(T), "");

        long const sig_size = t.signature_size();
        XCTAssert(sig_size == sizeof(double16::native_type) * 2, "sig_size = ", sig_size);

        auto [lb, extent] = t.extent();
        XCTAssert(lb == 0, "lb = ", lb);
        XCTAssert(extent == sizeof(T), "extent = ", extent);

        std::tie(lb, extent) = t.true_extent();
        XCTAssert(lb == 0, "true lb = ", lb);
        XCTAssert(extent == alignof(T) + sizeof(double16::native_type), "true extent = ", extent);
    } catch (std::exception const &e) {
        XCTAssert(false, e.what());
    }
}

} // namespace

void testType(Comm const &world)
{
    world.barrier();

    testType_Basic(world);
    testType_NativeChar(world);
    testType_NativeUnsignedChar(world);
    testType_NativeShort(world);
    testType_NativeUnsignedShort(world);
    testType_NativeInt(world);
    testType_NativeUnsignedInt(world);
    testType_NativeLong(world);
    testType_NativeUnsignedLong(world);
    testType_NativeFloat(world);
    testType_NativeDouble(world);
    testType_Realignment(world);
    testType_Composite(world);
    testType_Vector(world);
    testType_Indexed(world);
    testType_IndexedBlock(world);
    testType_Subarray(world);
    testType_StdComplex(world);
    testType_StdArray(world);
    testType_SomeType(world);
}
