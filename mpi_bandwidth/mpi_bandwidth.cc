/****************************************************************************
 * FILE: mpi_bandwidth.cc
 * DESCRIPTION:
 *   Provides point-to-point communications timings for any even
 *   number of MPI tasks.
 * AUTHOR: Blaise Barney (https://computing.llnl.gov/tutorials/mpi/samples/C/mpi_bandwidth.c)
 * LAST REVISED: 04/13/05
 ****************************************************************************/
/****************************************************************************
 * MODIFIED BY Kyungguk Min to use ParallelKit
 * 01/04/2021 - initial import
 * 01/09/2021 - modified to use `parallel::Communicator`
 ****************************************************************************/

#define PARALLELKIT_INLINE_VERSION 1
#include "../ParallelKit/println.h"
#include <ParallelKit/ParallelKit.h>

#include <array>
#include <iostream>
#include <memory>
#include <vector>

#include <ParallelKit/mpi_wrapper.h>

/* Change the next four parameters to suit your case */
constexpr int startsize  = 100000;
constexpr int endsize    = 1000000;
constexpr int increment  = 100000;
constexpr int roundtrips = 100;

using parallel::Communicator;
using parallel::mpi::Comm;

int main(int argc, char *argv[])
{
    int              numtasks, rank, nbytes, src{}, dest{}, namelength;
    std::vector<int> taskpairs;
    double thistime, bw, bestbw, worstbw, totalbw, avgbw, bestall, avgall, worstall, resolution, t1,
        t2;

    std::array<double, 3>           tmptimes;
    std::vector<decltype(tmptimes)> timings;

    std::array<char, MPI_MAX_PROCESSOR_NAME> host;
    std::vector<decltype(host)>              hostmap;

    auto const msgbuf = std::make_unique<char[]>(endsize);

    /* Some initializations and error checking */
    Comm::init(&argc, &argv);
    Communicator<char, int, double> const comm = Comm::world();
    numtasks                                   = comm.size();
    if (numtasks % 2 != 0) {
        println(std::cout, "ERROR: Must be an even number of tasks!  Quitting...");
        MPI_Abort(MPI_COMM_WORLD, -1);
    }
    taskpairs.resize(static_cast<unsigned>(numtasks));
    timings.resize(static_cast<unsigned>(numtasks) / 2);
    hostmap.resize(static_cast<unsigned>(numtasks));
    rank = comm->rank();
    for (unsigned i = 0; i < endsize; i++) {
        msgbuf[i] = 'x';
    }

    /* All tasks send their host name to task 0 */
    MPI_Get_processor_name(host.data(), &namelength);
    comm.all_gather(begin(host), end(host), begin(hostmap.front()));

    /* Determine who my send/receive partner is and tell task 0 */
    if (rank < numtasks / 2)
        dest = src = numtasks / 2 + rank;
    if (rank >= numtasks / 2)
        dest = src = rank - numtasks / 2;
    taskpairs.at(static_cast<unsigned>(rank)) = dest;
    comm.all_gather(taskpairs.data(), taskpairs.data() + numtasks);

    if (rank == 0) {
        resolution = MPI_Wtick();
        print(std::cout, "\n******************** MPI Bandwidth Test ********************\n");
        print(std::cout, "Message start size= ", startsize, " bytes\n");
        print(std::cout, "Message finish size= ", endsize, " bytes\n");
        print(std::cout, "Incremented by ", increment, " bytes per iteration\n");
        print(std::cout, "Roundtrips per iteration= ", roundtrips, "\n");
        print(std::cout, "MPI_Wtick resolution = ", resolution, "\n");
        print(std::cout, "************************************************************\n");
        for (unsigned i = 0; i < static_cast<unsigned>(numtasks); i++) {
            print(std::cout, "task ", i, " is on ", hostmap.at(i).data(),
                  " partner=", taskpairs.at(i), "\n");
        }
        print(std::cout, "************************************************************\n");
    }

    /*************************** first half of tasks *****************************/
    /* These tasks send/receive messages with their partner task, and then do a  */
    /* few bandwidth calculations based upon message size and timings.           */

    if (rank < numtasks / 2) {
        for (int n = startsize; n <= endsize; n = n + increment) {
            bestbw  = 0.0;
            worstbw = .99E+99;
            totalbw = 0.0;
            nbytes  = static_cast<int>(sizeof(char)) * n;
            for (int i = 1; i <= roundtrips; i++) {
                t1 = MPI_Wtime();
                comm.send_recv<char>(msgbuf.get(), std::next(msgbuf.get(), n), { dest, 1 },
                                     { src, 2 });
                t2       = MPI_Wtime();
                thistime = t2 - t1;
                bw       = static_cast<double>(nbytes * 2) / thistime;
                totalbw  = totalbw + bw;
                if (bw > bestbw)
                    bestbw = bw;
                if (bw < worstbw)
                    worstbw = bw;
            }
            /* Convert to megabytes per second */
            bestbw  = bestbw / 1000000.0;
            avgbw   = (totalbw / 1000000.0) / roundtrips;
            worstbw = worstbw / 1000000.0;

            /* Task 0 collects timings from all relevant tasks */
            if (rank == 0) {
                /* Keep track of my own timings first */
                timings[0][0] = bestbw;
                timings[0][1] = avgbw;
                timings[0][2] = worstbw;
                /* Initialize overall averages */
                bestall  = 0.0;
                avgall   = 0.0;
                worstall = 0.0;
                /* Now receive timings from other tasks and print results. Note */
                /* that this loop will be appropriately skipped if there are    */
                /* only two tasks. */
                for (unsigned j = 1; j < timings.size(); j++)
                    comm.recv(begin(timings[j]), end(timings[j]), { j, 100 });
                print(std::cout, "***Message size: ", n, " *** best  /  avg  / worst (MB/sec)\n");
                for (unsigned j = 0; j < timings.size(); j++) {
                    print(std::cout, "   task pair: ", j, " - ", taskpairs.at(j), ":    ",
                          timings[j][0], " / ", timings[j][1], " / ", timings[j][2], " \n");
                    bestall += timings[j][0];
                    avgall += timings[j][1];
                    worstall += timings[j][2];
                }
                print(std::cout, "   OVERALL AVERAGES:          ", bestall / (numtasks / 2), " / ",
                      avgall / (numtasks / 2), " / ", worstall / (numtasks / 2), " \n\n");
            } else {
                /* Other tasks send their timings to task 0 */
                tmptimes[0] = bestbw;
                tmptimes[1] = avgbw;
                tmptimes[2] = worstbw;
                comm.issend(std::begin(tmptimes), std::end(tmptimes), { 0, 100 }).wait();
            }
        }
    }

    /**************************** second half of tasks ***************************/
    /* These tasks do nothing more than send and receive with their partner task */

    if (rank >= numtasks / 2) {
        for (int n = startsize; n <= endsize; n = n + increment) {
            for (int i = 1; i <= roundtrips; i++) {
                comm.ibsend<char>(comm.recv<char>({}, { src, 1 }), { dest, 2 }).wait();
            }
        }
    }

    Comm::deinit();

} /* end of main */
