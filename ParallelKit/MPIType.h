/*
 * Copyright (c) 2021-2022, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#ifndef ParallelKit_MPIType_h
#define ParallelKit_MPIType_h

#include <ParallelKit/ParallelKit-config.h>

#include <array>
#include <string>
#include <type_traits>
#include <utility>

#include <ParallelKit/mpi_wrapper.h>

PARALLELKIT_NAMESPACE_BEGIN(1)
namespace mpi {

/// Wrapper for `MPI_Datatype`
///
class [[nodiscard]] Type {
    struct {
        long         alignment{ 0 };
        MPI_Datatype datatype{ MPI_DATATYPE_NULL };
        bool         is_native{ false };
    } m;

    // it takes the ownership of the datatype handle
    //
    Type(MPI_Datatype datatype, long alignment, bool is_native) noexcept
    : m{ alignment, datatype, is_native }
    {
    }

public:
    ~Type();
    Type() noexcept = default;

    /// @{
    Type(Type &&o) noexcept { std::swap(m, o.m); }
    Type &operator=(Type &&o) noexcept
    {
        std::swap(m, o.m);
        return *this;
    }
    /// @}

public:
    // MARK:- Query
    /// @name Query
    /// @{

    /// Check if `*this` is in a valid state.
    ///
    [[nodiscard]] explicit operator bool() const noexcept
    {
        return m.datatype != MPI_DATATYPE_NULL;
    }

    /// Returns internal `MPI_Datatype` handle.
    /// @details Any MPI operations that modify the state of this handle is discouraged and
    /// considered to ill-formed. If such operations are needed, duplicate the `MPI_Datatype` object
    /// referenced by this handle using `MPI_Type_dup`.
    ///
    [[nodiscard]] auto operator*() const noexcept { return m.datatype; }

    /// Returns the alignment associated with the underlying type.
    ///
    [[nodiscard]] auto alignment() const noexcept { return m.alignment; }

    /// Set a label associated with the datatype.
    ///
    [[nodiscard]] std::string label() const;
    /// Get the label associated with the datatype.
    ///
    void set_label(char const *label);

    /// Get the total size, in bytes, of the entries in the type signature associated with `*this`.
    /// @details This is the total size of the data in a message that would be created with this
    /// datatype. Entries that occur multiple times in the datatype are counted with their
    /// multiplicity.
    ///
    /// Internally, `MPI_Type_size_x` is called.
    /// @return Length of the datatype associated with `*this`.
    /// @exception When the corresponding MPI call returns an error.
    ///
    [[nodiscard]] long signature_size() const;

    /// Returns a pair of lower bound and extent of the datatype associated with `*this`.
    /// @details Internally, `MPI_Type_get_extent_x` is called.
    /// @exception When the corresponding MPI call returns an error.
    ///
    [[nodiscard]] std::pair<long, long> extent() const;

    /// Returns a pair of true lower bound and true extent of the datatype associated with `*this`.
    /// @details Internally, `MPI_Type_get_true_extent_x` is called.
    /// @exception When the corresponding MPI call returns an error.
    ///
    [[nodiscard]] std::pair<long, long> true_extent() const;

    /// Returns the number of "top-level" entries received. (Again, we count entries, each of type
    /// datatype, not bytes.)
    /// @details The datatype associated with `*this` should match the argument provided by the
    /// receive call that set the status variable.
    ///
    /// Internally, `MPI_Get_count` is called.
    /// @note Use of this is intened for cases where direct interactions with MPI routines are
    /// needed.
    /// @exception When the corresponding MPI call returns an error.
    ///
    [[nodiscard]] long get_count(MPI_Status const &status) const;

    /// Returns the number of received basic elements.
    /// @details The datatype associated with `*this` should match the argument provided by the
    /// receive call that set the status variable.
    ///
    /// get_count has a different behavior.
    /// It returns the number of "top-level entries" received, i.e. the number of "copies" of type
    /// datatype.
    ///
    /// Internally, `MPI_Get_elements_x` is called.
    /// @note Use of this is intened for cases where direct interactions with MPI routines are
    /// needed.
    /// @exception When the corresponding MPI call returns an error.
    ///
    [[nodiscard]] long get_elements(MPI_Status const &status) const;
    /// @}

public:
    // MARK:- Native Types
    /// @name Native Types
    /// @{

    /// Native type factory
    /// @tparam T One of the native types.
    ///
    template <class T> class Native;
    /// @}

public:
    // MARK:- Derived Types
    /// @name Derived Types
    /// @{

    /// Returns a new datatype that is identical to oldtype, except that the alignment of this new
    /// datatype is set to be Alignment.
    /// @details If necessary, the extent is adjusted, using `MPI_Type_create_resized` call, so that
    /// it becomes a next multiple of the new alignment.
    /// @param alignment New alignment to be associated with the datatype, which is greater than or
    /// equal to the current alignment.
    ///
    [[nodiscard]] Type realigned(long alignment) const;

    /// Create a vector datatype by replicating `*this`.
    /// @details Internally, `MPI_Type_vector` is called.
    ///
    /// `MPI_Type_contiguous` is equivalent to calling this with `block = stride = 1`.
    /// @tparam n_blocks The number of blocks (non-negative integer).
    /// @tparam blocksize The number of elements in each block (non-negative integer).
    /// @tparam stride The number of elements between start of each block (non-negative integer).
    ///
    template <int n_blocks, int blocksize = 1, int stride = blocksize>
    [[nodiscard]] Type vector() const
    {
        static_assert(n_blocks >= 0, "invalid number of blocks");
        static_assert(blocksize >= 0, "invalid block size");
        static_assert(stride >= 0, "invalid stride");
        return vector(n_blocks, blocksize, stride);
    }

    /// Create indexed datatype by replicating `*this`.
    /// @details Internally, `MPI_Type_indexed` is called.
    /// @tparam n_blocks The number of blocks (non-negative integer) --- also number of entries in
    /// array of displacements and array of block sizes.
    /// @param blocksizes The number of elements in each block (array of non-negative integers).
    /// @param displacements Displacement for each block, in multiples of oldtype extent (array of
    /// integers).
    ///
    template <unsigned n_blocks>
    [[nodiscard]] Type indexed(std::array<int, n_blocks> blocksizes,
                               std::array<int, n_blocks> displacements) const
    {
        return indexed(n_blocks, blocksizes.data(), displacements.data());
    }

    /// Create indexed block datatype by replicating `*this`.
    /// @details Internally, `MPI_Type_create_indexed_block` is called.
    /// @tparam n_disps The length of array of displacements (non-negative integer).
    /// @tparam blocksize The size of block (non-negative integer).
    /// @param displacements Array of displacements (array of integer).
    ///
    template <unsigned n_disps, unsigned blocksize>
    [[nodiscard]] Type indexed(std::array<int, n_disps> displacements) const
    {
        return indexed(n_disps, blocksize, displacements.data());
    }

    /// Create subarray datatype by describing an ND subarray of an ND array.
    /// @detailed Internally, `MPI_Type_create_subarray` is called.
    ///
    /// C storage ordering is implicitly assumed.
    /// @tparam ND The number of array dimensions (positive integer).
    /// @param dims The number of elements of type `*this` in each dimension of the full array
    /// (array of positive integers).
    /// @param slice_locs The starting coordinates of the subarray in each dimension (array of
    /// non-negative integers).
    /// @param slice_lens The number of elements of type `*this` in each dimension of the subarray
    /// (array of positive integers).
    ///
    template <unsigned ND>
    [[nodiscard]] Type subarray(std::array<int, ND> dims, std::array<int, ND> slice_locs,
                                std::array<int, ND> slice_lens) const
    {
        return subarray(ND, dims.data(), slice_locs.data(), slice_lens.data());
    }

    /// Construct a Type object by compositing multiple, heterogeneous datatypes.
    /// @details Composition is done using `MPI_Type_create_struct`.
    ///
    /// It creates a typemap reflecting a C struct.
    /// For example, a composite datatype
    ///
    /// > Type type_A = Type::composite(Type::native_char(), Type::native_long(),
    /// Type::native_char(), Type::native_short());
    ///
    /// corresponds to the typemap
    ///
    /// > struct A { char; long; char; short; };
    ///
    /// The new type `type_A` will have a lower bound of `0` and and an extent of `sizeof(long)*3`.
    /// The true lower bound is also `0`, but the true extent will be `sizeof(long)*2 +
    /// sizeof(short)*2`.
    ///
    /// Similarly,
    ///
    /// > Type type_B = Type::composite(type_A, Type::native_char());
    ///
    /// corresponds to the typemap
    ///
    /// > struct B { A; char; };
    ///
    /// where `type_A` itself is a composite type.
    ///
    /// The native alignment is respected and the extent of the new composite type is adjusted such
    /// that `extent == sizeof(T)`.
    /// @param first First type to composite.
    /// @param rest Rest of the type object.
    ///
    template <class First, class... Rest>
    [[nodiscard]] static Type composite(First &&first, Rest &&...rest)
    {
        static_assert((std::is_base_of_v<Type, std::decay_t<First>> && ...
                       && std::is_base_of_v<Type, std::decay_t<Rest>>));
        std::array<Type, sizeof...(Rest) + 1> types{ std::forward<First>(first),
                                                     std::forward<Rest>(rest)... };
        return make_composite(types.size(), types.data());
    }
    /// @}

private: // helpers
    [[nodiscard]] Type committed() &&;
    [[nodiscard]] Type vector(int n_blocks, int blocksize, int stride) const;
    [[nodiscard]] Type indexed(int n_blocks, int const *blocksizes, int const *displacements) const;
    [[nodiscard]] Type indexed(int n_disps, int blocksize, int const *displacements) const;
    [[nodiscard]] Type subarray(int ND, int const *dims, int const *slice_locs,
                                int const *slice_lens) const;
    [[nodiscard]] static Type make_composite(unsigned long size /*at least one element*/,
                                             Type const   *types);
};

// native type subclasses
//
#include <ParallelKit/NativeType.hh>

} // namespace mpi
PARALLELKIT_NAMESPACE_END(1)

#endif /* ParallelKit_MPIType_h */
