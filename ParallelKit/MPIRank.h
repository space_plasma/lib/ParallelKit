/*
 * Copyright (c) 2021-2022, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#ifndef ParallelKit_MPIRank_h
#define ParallelKit_MPIRank_h

#include <ParallelKit/ParallelKit-config.h>

#include <ParallelKit/mpi_wrapper.h>

PARALLELKIT_NAMESPACE_BEGIN(1)
namespace mpi {

/// Wrapper for MPI rank.
/// @details On construction, no range check is performed for a given rank.
///
/// The range of valid values for rank is *0 <= rank <= n − 1* and `MPI_PROC_NULL`, where *n* is the
/// number of processes in the group.
///
struct [[nodiscard]] Rank {
    int v{ MPI_UNDEFINED };
    //
    constexpr          operator int() const noexcept { return v; }
    constexpr explicit operator bool() const noexcept { return v != MPI_UNDEFINED; }

    [[nodiscard]] static constexpr Rank any() noexcept
    {
        return { MPI_ANY_SOURCE };
    } //!< Any source.
    [[nodiscard]] static constexpr Rank null() noexcept
    {
        return { MPI_PROC_NULL };
    } //!< Used to define a "dummy" destination or source in any send or receive call.
    [[nodiscard]] friend constexpr bool operator==(Rank const &A, Rank const &B) noexcept
    {
        return A.v == B.v;
    }
};

} // namespace mpi
PARALLELKIT_NAMESPACE_END(1)

#endif /* ParallelKit_MPIRank_h */
