/*
 * Copyright (c) 2021, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "InterProcessComm.h"

namespace PAR = PARALLELKIT_NAMESPACE(1);

namespace {
#ifdef DEBUG
[[maybe_unused]] long test_1()
{
    PAR::Communicator<double, long> comm{ PAR::mpi::Comm::world() };
    return comm.size();
}
#endif
} // namespace
