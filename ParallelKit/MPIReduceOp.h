/*
 * Copyright (c) 2021-2022, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#ifndef ParallelKit_MPIReduceOp_h
#define ParallelKit_MPIReduceOp_h

#include <ParallelKit/ParallelKit-config.h>

#include <algorithm>
#include <functional>
#include <iterator>
#include <type_traits>
#include <utility>

#include <ParallelKit/mpi_wrapper.h>

PARALLELKIT_NAMESPACE_BEGIN(1)
namespace mpi {

/// Wrapper for user-defined reduction operator.
///
class ReduceOp {
    struct {
        MPI_Op op{ MPI_OP_NULL };
        bool   is_native{ false };
    } m;

    ReduceOp(MPI_Op op, bool is_native) noexcept : m{ op, is_native } {}

public:
    ~ReduceOp();
    ReduceOp() noexcept = default;

    /// @{
    ReduceOp(ReduceOp &&o) noexcept { std::swap(m, o.m); }
    ReduceOp &operator=(ReduceOp &&o) noexcept
    {
        std::swap(m, o.m);
        return *this;
    }
    /// @}

    /// Construct user-defined reduction operator.
    /// @param commutative Indicates where the operator is commutative.
    /// @param f Pointer to the reduction operator function.
    ///
    ReduceOp(bool commutative, void (*f)(void *invec, void *inoutvec, int *vlen, MPI_Datatype *));

    /// @name Predefined Reduction Operators
    /// @{
    [[nodiscard]] static ReduceOp max() noexcept { return ReduceOp{ MPI_MAX, true }; }
    [[nodiscard]] static ReduceOp min() noexcept { return ReduceOp{ MPI_MIN, true }; }
    [[nodiscard]] static ReduceOp plus() noexcept { return ReduceOp{ MPI_SUM, true }; }
    [[nodiscard]] static ReduceOp prod() noexcept { return ReduceOp{ MPI_PROD, true }; }
    [[nodiscard]] static ReduceOp logic_and() noexcept { return ReduceOp{ MPI_LAND, true }; }
    [[nodiscard]] static ReduceOp bit_and() noexcept { return ReduceOp{ MPI_BAND, true }; }
    [[nodiscard]] static ReduceOp logic_or() noexcept { return ReduceOp{ MPI_LOR, true }; }
    [[nodiscard]] static ReduceOp bit_or() noexcept { return ReduceOp{ MPI_BOR, true }; }
    [[nodiscard]] static ReduceOp logic_xor() noexcept { return ReduceOp{ MPI_LXOR, true }; }
    [[nodiscard]] static ReduceOp bit_xor() noexcept { return ReduceOp{ MPI_BXOR, true }; }
    [[nodiscard]] static ReduceOp loc_min() noexcept { return ReduceOp{ MPI_MINLOC, true }; }
    [[nodiscard]] static ReduceOp loc_max() noexcept { return ReduceOp{ MPI_MAXLOC, true }; }
    [[nodiscard]] static ReduceOp replace() noexcept { return ReduceOp{ MPI_REPLACE, true }; }
    [[nodiscard]] static ReduceOp no_op() noexcept { return ReduceOp{ MPI_NO_OP, true }; }
    /// @}

    /// @name Reduction Operators for Custom Types
    /// @{
    template <class T> [[nodiscard]] static ReduceOp max(bool commutative) noexcept;
    template <class T> [[nodiscard]] static ReduceOp min(bool commutative) noexcept;
    template <class T> [[nodiscard]] static ReduceOp plus(bool commutative) noexcept;
    template <class T> [[nodiscard]] static ReduceOp prod(bool commutative) noexcept;
    template <class T> [[nodiscard]] static ReduceOp logic_and(bool commutative) noexcept;
    template <class T> [[nodiscard]] static ReduceOp bit_and(bool commutative) noexcept;
    template <class T> [[nodiscard]] static ReduceOp logic_or(bool commutative) noexcept;
    template <class T> [[nodiscard]] static ReduceOp bit_or(bool commutative) noexcept;
    template <class T> [[nodiscard]] static ReduceOp bit_xor(bool commutative) noexcept;
    /// @}

    /// Construct using operator object
    template <class T, class Op>
    [[nodiscard]] static ReduceOp make(Op const &, bool commutative) noexcept
    {
        static_assert(!std::is_reference_v<T>, "T should not be a reference");
        static_assert(
            std::is_same_v<std::decay_t<T>, std::decay_t<std::invoke_result_t<Op, T &&, T &&>>>);

        constexpr auto op = [](void *invec, void *inoutvec, int *vlen, MPI_Datatype *) noexcept {
            auto const *rhs = reinterpret_cast<T const *>(invec);
            auto       *lhs = reinterpret_cast<T *>(inoutvec);
            std::transform(rhs, std::next(rhs, *vlen), lhs, lhs, Op{});
        };
        return { commutative, op };
    }

    /// Check if `*this` is valid, i.e., not null.
    ///
    explicit operator bool() const noexcept { return m.op != MPI_OP_NULL; }

    /// Returns the native handle for `*this`.
    ///
    [[nodiscard]] auto operator*() const noexcept { return m.op; }

    /// Query commutative of `*this`.
    ///
    [[nodiscard]] bool commutative() const;
};

// user-defined reduction operators
//
#include <ParallelKit/MPIReduceOp.hh>

} // namespace mpi
PARALLELKIT_NAMESPACE_END(1)

#endif /* ParallelKit_MPIReduceOp_h */
