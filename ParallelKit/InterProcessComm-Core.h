/*
 * Copyright (c) 2021, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#ifndef ParallelKit_InterProcessComm_tuple_h
#define ParallelKit_InterProcessComm_tuple_h

#include <ParallelKit/ParallelKit-config.h>

#include <ParallelKit/MPIComm.h>
#include <ParallelKit/MPIType.h>
#include <ParallelKit/TypeMap.h>

#include <algorithm>
#include <array>
#include <functional>
#include <iterator>
#include <memory>
#include <stdexcept>
#include <tuple>
#include <type_traits>
#include <utility>
#include <vector>

PARALLELKIT_NAMESPACE_BEGIN(1)

/// Message type-aware, inter-process communicator
/// @details The purpose of this class is to provide a more strongly-typed message passing
/// mechanism. Sending and, to a certain extent, receiving message, types can be deduced at the call
/// site of communication member functions. Given the message type, either deduced or explicitly
/// specified, the typemap for MPI communication is obtained using the TypeMap adapter class. Not
/// only for the predefined types, users can also supply specializations of TypeMap adapter class
/// for user-defined types of which messages users want to pass around. Then, the communication
/// operations provided here automatically pick up a correct typemap for a user-defined type.
///
/// The class definition is templated.
/// The class is instantiated with a list of message types this class is restricted to interact
/// with. The corresponding MPI typemaps are cached at initialization of an object. Any
/// communications of messages of types not include here will fail at compile time.
/// @tparam Types List of message types which this class is restricted to interact with.
///
template <class... Types> class Communicator {
    static_assert(sizeof...(Types) > 0);
    static_assert((... && std::is_move_constructible_v<Types>),
                  "Types should be move-constructible");
    static_assert((... && std::is_default_constructible_v<Types>),
                  "Types should be default-constructible");
    static_assert((... && std::is_default_constructible_v<TypeMap<Types>>),
                  "TypeMap<Types> must be defined and default-constructible");
    // it's ok to include duplicate types in Types pack

private:
    using type_pack = std::tuple<Types...>;

    // helper to find index to a type T in the parameter pack
    //
    template <class T> class tuple_index {
        using size_t = unsigned long;
        template <size_t... Is>
        [[nodiscard]] static constexpr auto find_idx(std::integer_sequence<size_t, Is...>)
        {
            static_assert((... + std::is_same_v<T, Types>) == 1,
                          "None or multiple T's found in Types");
            return (... + (std::is_same_v<T, Types> ? Is : 0));
        }

    public:
        static constexpr size_t value
            = find_idx(std::make_integer_sequence<size_t, std::tuple_size_v<type_pack>>{});
    };

private:
    mpi::Comm                               comm{};
    std::array<mpi::Type, sizeof...(Types)> typemaps{};

private: // typemap access
    template <long I> [[nodiscard]] decltype(auto) get_type() const noexcept
    {
        return std::get<I>(typemaps);
    }
    template <class T> [[nodiscard]] decltype(auto) get_type() const noexcept
    {
        constexpr auto I = tuple_index<T>::value;
        return std::get<I>(typemaps);
    }

public:
    Communicator() noexcept = default;
    /// Construct Communicator using mpi::Comm
    ///
    Communicator(mpi::Comm comm) : comm{ std::move(comm) }, typemaps{ make_type<Types>()... } {}

    /// mpi::Comm's member access operator overloading
    ///
    auto operator->() const &noexcept { return std::addressof(comm); }

public: // helper classes
    /// Wrapper for mpi::Rank
    ///
    class [[nodiscard]] Rank {
        template <class T>
        static constexpr bool is_int_v = std::is_integral_v<T> && (sizeof(T) <= sizeof(int));

        mpi::Rank rank;

    public:
        /// Implicit type cast-style value unwraping
        ///
        constexpr operator mpi::Rank() const noexcept { return rank; }

        /// Member access operator overloading
        //
        constexpr auto operator->() const &noexcept { return std::addressof(rank); }

        /// Construct a Rank object by wrapping mpi::Rank
        ///
        constexpr Rank(mpi::Rank rank) noexcept : rank{ rank } {}

        /// Construct a Rank object using an integer number
        /// @tparam T An integral type of size less than or equal to `sizeof(int)`.
        ///
        template <class T, std::enable_if_t<is_int_v<T>, int> = 0>
        constexpr Rank(T rank) noexcept : rank{ static_cast<int>(rank) }
        {
        }

        /// Rank comparison
        ///
        [[nodiscard]] friend constexpr bool operator==(Rank const &A, Rank const &B) noexcept
        {
            return A->v == B->v;
        }
    };

    /// Wrapper for Rank and Tag
    ///
    class [[nodiscard]] Envelope {
        using pair_t = std::pair<mpi::Rank, mpi::Tag>;
        pair_t addr;

    public:
        constexpr operator pair_t const &() const noexcept { return addr; }

        constexpr Envelope(Rank rank, mpi::Tag tag) noexcept : addr{ rank, tag } {}
        constexpr Envelope(mpi::Rank rank, mpi::Tag tag) noexcept : addr{ rank, tag } {}

        template <class T>
        static constexpr bool is_int_v = std::is_integral_v<T> && (sizeof(T) <= sizeof(int));

        template <class Tag, std::enable_if_t<is_int_v<Tag>, int> = 0>
        constexpr Envelope(Rank rank, Tag tag) noexcept
        : addr{ rank, mpi::Tag{ static_cast<int>(tag) } }
        {
        }
        template <class Tag, std::enable_if_t<is_int_v<Tag>, int> = 0>
        constexpr Envelope(mpi::Rank rank, Tag tag) noexcept
        : addr{ rank, mpi::Tag{ static_cast<int>(tag) } }
        {
        }
    };

    /// Wrapper for userspace buffer for sending message and for mpi::Request.
    /// @details Immediate, buffered send operation returns a Ticket object with which
    /// callers can query whether the message associated with has been delivered.
    /// @note Since this class keeps the outgoing message buffer for buffered send operations,
    /// it is important to hold onto this object until the message in the buffer is delievered.
    /// It is ill-formed if a Ticket object returned by buffered send operations goes out of scope
    /// before the message delivery.
    ///
    template <class T> class [[nodiscard]] Ticket {
        friend Communicator;
        using payload_t = T;
        payload_t    payload{}; // userspace buffer for sending message
        mpi::Request req{};     // MPI Request token
        //
        Ticket(payload_t    payload,
               mpi::Request req) noexcept(std::is_nothrow_move_constructible_v<payload_t>)
        : payload{ std::move(payload) }, req{ std::move(req) }
        {
        }

    public:
        /// @{
        Ticket(Ticket &&) noexcept(std::is_nothrow_move_constructible_v<payload_t>) = default;
        Ticket &operator=(Ticket &&) noexcept(std::is_nothrow_move_assignable_v<payload_t>)
            = delete;
        /// @}

        /// Wait for package delivery
        ///
        void wait() && { std::move(req).wait(); }

        /// Query whether `*this` is a valid object
        ///
        explicit operator bool() const noexcept { return static_cast<bool>(req); }

        /// Member access operator overloading to the mpi::Request object.
        ///
        /// @{
        decltype(auto) operator->() &&noexcept { return std::addressof(req); }
        decltype(auto) operator->() &noexcept { return std::addressof(req); }
        /// @}
    };

    /// Wrapper for message received.
    /// @details This class encapsulates messages delievered.
    /// Memory space that fits the messages is allocated, messages received are saved,
    /// and they are then retrieved in a more programmer-friendly manner.
    ///
    template <class T> class [[nodiscard]] Package {
        friend Communicator;
        using payload_t = T;
        payload_t payload{}; // received message buffer
        //
        Package(payload_t payload) noexcept(std::is_nothrow_move_constructible_v<payload_t>)
        : payload{ std::move(payload) }
        {
        }

    public:
        /// @{
        Package(Package &&) noexcept(std::is_nothrow_move_constructible_v<payload_t>) = default;
        Package &operator=(Package &&) noexcept(std::is_nothrow_move_assignable_v<payload_t>)
            = delete;
        /// @}

        /// Unpack the wrapped message
        /// @details This function does both the unpacking and processing the message.
        /// The caller passes a function object which processes the message,
        /// and `*this` invokes it with the message delievered as the first argument.
        ///
        /// @tparam F Type of the function object with signature `Ret(Payload&&, RestArgs&&)`.
        /// `Payload` can be `std::vector<message_t>`, or just `message_t`,
        /// depending on the vector or scalar version of the receive function that returned this
        /// Package object.
        /// @tparam RestArgs Types of additional arguments passed to the function object.
        /// @param f Function object which processes the message.
        /// @param rest_args Additional arguments which will be passed to `f`.
        /// @return The return value of `f`.
        ///
        template <class F, class... RestArgs>
        auto unpack(F &&f, RestArgs &&...rest_args) &&noexcept(
            std::is_nothrow_invocable_v<F &&, payload_t &&, RestArgs &&...>)
            -> std::invoke_result_t<F &&, payload_t &&, RestArgs &&...>
        {
            static_assert(std::is_invocable_v<F &&, payload_t &&, RestArgs &&...>);
            // invoke the callable with payload as its first argument
            return std::invoke(std::forward<F>(f), std::move(payload),
                               std::forward<RestArgs>(rest_args)...);
        }
        /// Type-cast style unpack
        ///
        /// The delivery notification is fired immediately upon return.
        ///
        [[nodiscard]]
        operator payload_t() &&noexcept(std::is_nothrow_move_constructible_v<payload_t>)
        {
            return std::move(payload);
        }
        /// Pointer-derefencing style unpack
        ///
        /// The delivery notification is fired immediately upon return.
        ///
        [[nodiscard]] payload_t
        operator*() &&noexcept(std::is_nothrow_move_constructible_v<payload_t>)
        {
            return std::move(payload);
        }
    };

public: // communications implementation
    /// Determines the size of the group associated with `*this`.
    ///
    [[nodiscard]] int size() const { return comm.size(); }

    /// Determines the rank of the calling process in `*this`.
    ///
    [[nodiscard]] Rank rank() const { return comm.rank(); }

    /// Blocks until all processes in the communicator have reached this routine.
    ///
    void barrier() const { comm.barrier(); }

#include <ParallelKit/InterProcessComm-Collective.hh>
#include <ParallelKit/InterProcessComm-P2P.hh>
#include <ParallelKit/InterProcessComm-Reduction.hh>
};

PARALLELKIT_NAMESPACE_END(1)

#endif /* ParallelKit_InterProcessComm_tuple_h */
