/*
 * Copyright (c) 2021, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "MPIReduceOp.h"

#include "MPIComm.h"
#include "println.h"

#include <iostream>
#include <stdexcept>
#include <string>

PARALLELKIT_NAMESPACE_BEGIN(1)

mpi::ReduceOp::~ReduceOp()
{
    if (m.is_native || MPI_OP_NULL == m.op) {
        return;
    }
    if (MPI_Op_free(&m.op) != MPI_SUCCESS) {
        println(std::cerr, "%% ", __PRETTY_FUNCTION__, " - MPI_Op_free(...) returned error");
#if defined(PARALLELKIT_SHOULD_TERMINATE_ON_FAILURE_IN_DESTRUCTOR)                                 \
    && PARALLELKIT_SHOULD_TERMINATE_ON_FAILURE_IN_DESTRUCTOR
        std::terminate();
#endif
    }
}

mpi::ReduceOp::ReduceOp(bool const commutative,
                        void (*f)(void *invec, void *inoutvec, int *vlen, MPI_Datatype *))
{
    MPI_Op op;
    if (MPI_Op_create(f, commutative, &op) != MPI_SUCCESS) {
        throw std::runtime_error{ std::string{ __PRETTY_FUNCTION__ }
                                  + " - MPI_Op_create(...) returned error" };
    }
    *this = ReduceOp{ op, false };
}

bool mpi::ReduceOp::commutative() const
{
    int commute;
    if (MPI_Op_commutative(m.op, &commute) != MPI_SUCCESS) {
        throw std::runtime_error{ std::string{ __PRETTY_FUNCTION__ }
                                  + " - MPI_Op_commutative(...) returned error" };
    }
    return commute;
}

PARALLELKIT_NAMESPACE_END(1)
