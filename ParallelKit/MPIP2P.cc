/*
 * Copyright (c) 2021, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "MPIP2P.h"

#include "MPIComm.h"

#include <limits>
#include <stdexcept>
#include <string>

PARALLELKIT_NAMESPACE_BEGIN(1)

using mpi::Comm;

namespace {
constexpr int int_max = std::numeric_limits<int>::max();
//
[[nodiscard]] MPI_Comm operator*(mpi::P2P<Comm> const &comm) noexcept
{
    return static_cast<Comm const &>(comm).operator*();
}
} // namespace

// MARK: Persistent Communication Requests
//
auto mpi::P2P<Comm>::ssend_init(void const *data, Type const &type, long const count,
                                std::pair<Rank, Tag> const &to) const -> Request
{
    if (count > int_max) {
        throw std::invalid_argument{ std::string{ __PRETTY_FUNCTION__ }
                                     + " - number of elements greater than int::max" };
    }
    MPI_Request r;
    if (MPI_Ssend_init(data, static_cast<int>(count), *type, to.first, to.second, **this, &r)
        != MPI_SUCCESS) {
        throw std::runtime_error{ std::string{ __PRETTY_FUNCTION__ }
                                  + " - MPI_Ssend_init(...) returned error" };
    }
    return Request{ r };
}
auto mpi::P2P<Comm>::recv_init(void *buffer, Type const &type, long const count,
                               std::pair<Rank, Tag> const &from) const -> Request
{
    if (count > int_max) {
        throw std::invalid_argument{ std::string{ __PRETTY_FUNCTION__ }
                                     + " - number of elements greater than int::max" };
    }
    MPI_Request r;
    if (MPI_Recv_init(buffer, static_cast<int>(count), *type, from.first, from.second, **this, &r)
        != MPI_SUCCESS) {
        throw std::runtime_error{ std::string{ __PRETTY_FUNCTION__ }
                                  + " - MPI_Ssend_init(...) returned error" };
    }
    return Request{ r };
}

// MARK: Non-blocking Send/Blocking Recv
//
auto mpi::P2P<Comm>::issend(void const *data, Type const &type, long const count,
                            std::pair<Rank, Tag> const &to) const -> Request
{
    if (count > int_max) {
        throw std::invalid_argument{ std::string{ __PRETTY_FUNCTION__ }
                                     + " - number of elements greater than int::max" };
    }
    MPI_Request r;
    if (MPI_Issend(data, static_cast<int>(count), *type, to.first, to.second, **this, &r)
        != MPI_SUCCESS) {
        throw std::runtime_error{ std::string{ __PRETTY_FUNCTION__ }
                                  + " - MPI_Issend(...) returned error" };
    }
    return Request{ r };
}

MPI_Status mpi::P2P<Comm>::probe(std::pair<Rank, Tag> const &from) const
{
    MPI_Status s;
    if (MPI_Probe(from.first, from.second, **this, &s) != MPI_SUCCESS) {
        throw std::runtime_error{ std::string{ __PRETTY_FUNCTION__ }
                                  + " - MPI_Probe(...) returned error" };
    }
    return s;
}
MPI_Status mpi::P2P<Comm>::recv(void *buffer, Type const &type, long const count,
                                std::pair<Rank, Tag> const &from) const
{
    if (count > int_max) {
        throw std::invalid_argument{ std::string{ __PRETTY_FUNCTION__ }
                                     + " - number of elements greater than int::max" };
    }
    MPI_Status s;
    if (MPI_Recv(buffer, static_cast<int>(count), *type, from.first, from.second, **this, &s)
        != MPI_SUCCESS) {
        throw std::runtime_error{ std::string{ __PRETTY_FUNCTION__ }
                                  + " - MPI_Recv(...) returned error" };
    }
    return s;
}

MPI_Status mpi::P2P<Comm>::probe(std::pair<Rank, Tag> const &from, MPI_Message *msg) const
{
    MPI_Status s;
    if (MPI_Mprobe(from.first, from.second, **this, msg, &s) != MPI_SUCCESS) {
        throw std::runtime_error{ std::string{ __PRETTY_FUNCTION__ }
                                  + " - MPI_Mprobe(...) returned error" };
    }
    return s;
}
MPI_Status mpi::P2P<Comm>::recv(void *buffer, Type const &type, long const count, MPI_Message *msg)
{
    if (count > int_max) {
        throw std::invalid_argument{ std::string{ __PRETTY_FUNCTION__ }
                                     + " - number of elements greater than int::max" };
    }
    MPI_Status s;
    if (MPI_Mrecv(buffer, static_cast<int>(count), *type, msg, &s) != MPI_SUCCESS) {
        throw std::runtime_error{ std::string{ __PRETTY_FUNCTION__ }
                                  + " - MPI_Mrecv(...) returned error" };
    }
    return s;
}

// MARK: Send/Recv
//
MPI_Status mpi::P2P<Comm>::send_recv(void const *send_data, Type const &send_type,
                                     long const send_count, std::pair<Rank, Tag> const &send_to,
                                     void *recv_buffer, Type const &recv_type,
                                     long const                  recv_count,
                                     std::pair<Rank, Tag> const &recv_from) const
{
    if (send_count > int_max) {
        throw std::invalid_argument{ std::string{ __PRETTY_FUNCTION__ }
                                     + " - number of send elements greater than int::max" };
    }
    if (recv_count > int_max) {
        throw std::invalid_argument{ std::string{ __PRETTY_FUNCTION__ }
                                     + " - number of receive elements greater than int::max" };
    }
    MPI_Status s;
    if (MPI_Sendrecv(send_data, static_cast<int>(send_count), *send_type, send_to.first,
                     send_to.second, recv_buffer, static_cast<int>(recv_count), *recv_type,
                     recv_from.first, recv_from.second, **this, &s)
        != MPI_SUCCESS) {
        throw std::runtime_error{ std::string{ __PRETTY_FUNCTION__ }
                                  + " - MPI_Sendrecv(...) returned error" };
    }
    return s;
}
MPI_Status mpi::P2P<Comm>::send_recv(void *data, Type const &type, long const count,
                                     std::pair<Rank, Tag> const &send_to,
                                     std::pair<Rank, Tag> const &recv_from) const
{
    if (count > int_max) {
        throw std::invalid_argument{ std::string{ __PRETTY_FUNCTION__ }
                                     + " - number of elements greater than int::max" };
    }
    MPI_Status s;
    if (MPI_Sendrecv_replace(data, static_cast<int>(count), *type, send_to.first, send_to.second,
                             recv_from.first, recv_from.second, **this, &s)
        != MPI_SUCCESS) {
        throw std::runtime_error{ std::string{ __PRETTY_FUNCTION__ }
                                  + " - MPI_Sendrecv_replace(...) returned error" };
    }
    return s;
}

PARALLELKIT_NAMESPACE_END(1)
