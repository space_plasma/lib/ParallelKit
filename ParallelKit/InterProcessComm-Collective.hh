/*
 * Copyright (c) 2021-2022, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#ifndef ParallelKit_InterProcessComm_Collective_hh
#define ParallelKit_InterProcessComm_Collective_hh

/// @name Collective
/// @{

// MARK:- Blocking Broadcast
//
/// @memberof Communicator
/// Broadcasts a message from the root process to all processes in the group, itself included.
/// @details For the root process, the buffer contains the message to broadcast.
/// For other processes, the message is saved to buffer.
/// @see mpi::Collective::bcast.
/// @tparam I Index to the Types parameter pack.
/// @param [in, out] first Pointer to the first element of the message buffer.
/// @param [in, out] last Pointer to the one-past-the-last element of the message buffer.
/// @param root Rank of the root process from which the message is being broadcasted.
/// @return Pointer to the first element of the message buffer.
///
template <unsigned long I>
auto bcast(std::tuple_element_t<I, type_pack> *first, std::tuple_element_t<I, type_pack> *last,
           Rank root) const
{
    comm.bcast(first, get_type<I>(), std::distance(first, last), root);
    return first;
}
/// @memberof Communicator
/// Broadcasts a message from the root process to all processes in the group, itself included.
/// @details For the root process, the buffer contains the message to broadcast.
/// For other processes, the message is saved to buffer.
/// @see mpi::Collective::bcast.
/// @tparam T Message type.
/// @param [in, out] first Pointer to the first element of the message buffer.
/// @param [in, out] last Pointer to the one-past-the-last element of the message buffer.
/// @param root Rank of the root process from which the message is being broadcasted.
/// @return Pointer to the first element of the message buffer.
///
template <class T> auto bcast(T *first, T *last, Rank root) const
{
    comm.bcast(first, this->template get_type<T>(), std::distance(first, last), root);
    return first;
}

/// @memberof Communicator
/// Broadcasts messages from the root process to all processes in the group, itself included.
/// @details For the root process, the first argument contains the message to broadcast.
/// For other processes, the content of the `std::vector` container is not used, but
/// the size of it must be identical to the size of the messages being broadcasted from the master.
/// @tparam I Index to the Types parameter pack.
/// @param payload Message to broadcast for the root process;
/// for other processes, the size of the container must be the same as the message size being
/// broadcasted.
/// @param root Rank of the root process from which the message is being broadcasted.
/// @return A Package obejct wrapping the received message.
///
template <unsigned long I>
[[nodiscard]] auto bcast(std::vector<std::tuple_element_t<I, type_pack>> payload, Rank root) const
{
    comm.bcast(payload.data(), get_type<I>(), static_cast<long>(payload.size()), root);
    return Package<decltype(payload)>{ std::move(payload) };
}
/// @memberof Communicator
/// Broadcasts messages from the root process to all processes in the group, itself included.
/// @details For the root process, the first argument contains the message to broadcast.
/// For other processes, the content of the `std::vector` container is not used, but
/// the size of it must be identical to the size of the messages being broadcasted from the master.
/// @tparam T Message type.
/// @param payload Message to broadcast for the root process;
/// for other processes, the size of the container must be the same as the message size being
/// broadcasted.
/// @param root Rank of the root process from which the message is being broadcasted.
/// @return A Package obejct wrapping the received message.
///
template <class T> [[nodiscard]] auto bcast(std::vector<T> payload, Rank root) const
{
    comm.bcast(payload.data(), this->template get_type<T>(), static_cast<long>(payload.size()),
               root);
    return Package<decltype(payload)>{ std::move(payload) };
}

/// @memberof Communicator
/// Broadcasts a single message from the root process to all processes in the group, itself
/// included.
/// @details For the root process, the first argument contains the message to broadcast.
/// For other processes, this argument is just a placeholder.
/// @tparam I Index to the Types parameter pack.
/// @param payload Message to broadcast for the root process; for other processes, this is just a
/// placeholder.
/// @param root Rank of the root process from which the message is being broadcasted.
/// @return A Package obejct wrapping the received message.
///
template <unsigned long I>
[[nodiscard]] auto bcast(std::tuple_element_t<I, type_pack> payload, Rank root) const
{
    comm.bcast(&payload, get_type<I>(), long{ 1 }, root);
    return Package<decltype(payload)>{ std::move(payload) };
}
/// @memberof Communicator
/// Broadcasts a single message from the root process to all processes in the group, itself
/// included.
/// @details For the root process, the first argument contains the message to broadcast.
/// For other processes, this argument is just a placeholder.
/// @tparam T Message type.
/// @param payload Message to broadcast for the root process; for other processes, this is just a
/// placeholder.
/// @param root Rank of the root process from which the message is being broadcasted.
/// @return A Package obejct wrapping the received message.
///
template <class T> [[nodiscard]] auto bcast(T payload, Rank root) const
{
    comm.bcast(&payload, this->template get_type<T>(), long{ 1 }, root);
    return Package<decltype(payload)>{ std::move(payload) };
}

// MARK:- Blocking Gather
//
/// @memberof Communicator
/// Gather
/// @details Each process (root process included) sends the contents of its send buffer to the root
/// process. For other processes, the receive buffer is ignored. For the root process, the receive
/// buffer must be large enough to gather messages, that is, `recv_count = send_count *
/// comm.size()`.
/// @see mpi::Collective::gather.
/// @tparam I Index to the Types parameter pack.
/// @param [in] send_first Pointer to the first element of the outgoing message buffer.
/// @param [in] send_last Pointer to the one-past-the-last element of the outgoing message buffer.
/// @param [out] recv_first Pointer to the first element of the incomming message buffer.
/// The buffer size must be large enough to accomodate the gathered messages.
/// @param root Rank of the root process.
/// @return Pointer to the first element of the incomming message buffer.
///
template <unsigned long I>
auto gather(std::tuple_element_t<I, type_pack> const *send_first,
            std::tuple_element_t<I, type_pack> const *send_last,
            std::tuple_element_t<I, type_pack> *recv_first, Rank root) const
{
    if (this->rank() == root) // there must be enough receiving buffer
        comm.gather(send_first, get_type<I>(), std::distance(send_first, send_last), recv_first,
                    get_type<I>(), std::distance(send_first, send_last), root);
    else
        comm.gather(send_first, get_type<I>(), std::distance(send_first, send_last), nullptr, root);
    return recv_first;
}
/// @memberof Communicator
/// Gather
/// @details Each process (root process included) sends the contents of its send buffer to the root
/// process. For other processes, the receive buffer is ignored. For the root process, the receive
/// buffer must be large enough to gather messages, that is, `recv_count = send_count *
/// comm.size()`.
/// @see mpi::Collective::gather.
/// @tparam T Message type.
/// @param [in] send_first Pointer to the first element of the outgoing message buffer.
/// @param [in] send_last Pointer to the one-past-the-last element of the outgoing message buffer.
/// @param [out] recv_first Pointer to the first element of the incomming message buffer.
/// The buffer size must be large enough to accomodate the gathered messages.
/// @param root Rank of the root process.
/// @return Pointer to the first element of the incomming message buffer.
///
template <class T>
auto gather(T const *send_first, T const *send_last, T *recv_first, Rank root) const
{
    if (this->rank() == root) // there must be enough receiving buffer
        comm.gather(send_first, this->template get_type<T>(), std::distance(send_first, send_last),
                    recv_first, this->template get_type<T>(), std::distance(send_first, send_last),
                    root);
    else
        comm.gather(send_first, this->template get_type<T>(), std::distance(send_first, send_last),
                    nullptr, root);
    return recv_first;
}

/// @memberof Communicator
/// Gather
/// @details In this version, the gathered messages are packaged in a Package object, hence
/// freeing the caller from preparing the incomming message buffer.
/// @tparam I Index to the Types parameter pack.
/// @param payload Outgoing messages.
/// @param root Rank of the root process.
/// @return A Package object with the gathered messages contained in `std::vector`.
///
template <unsigned long I>
[[nodiscard]] auto gather(std::vector<std::tuple_element_t<I, type_pack>> payload, Rank root) const
{
    if (this->rank() == root) {
        long const recv_count = static_cast<long>(payload.size());
        payload.resize(static_cast<unsigned long>(recv_count * comm.size()));
        if (comm.rank() != 0)
            std::move(begin(payload), next(begin(payload), recv_count),
                      next(begin(payload), recv_count * comm.rank()));
        comm.gather(nullptr, payload.data(), get_type<I>(), recv_count);
        return Package<decltype(payload)>{ std::move(payload) };
    } else {
        comm.gather(payload.data(), get_type<I>(), static_cast<long>(payload.size()), nullptr,
                    root);
        return Package<decltype(payload)>{ std::move(payload) };
    }
}
/// @memberof Communicator
/// Gather
/// @details In this version, the gathered messages are packaged in a Package object, hence
/// freeing the caller from preparing the incomming message buffer.
/// @tparam T Message type.
/// @param payload Outgoing messages.
/// @param root Rank of the root process.
/// @return A Package object with the gathered messages contained in `std::vector`.
///
template <class T> [[nodiscard]] auto gather(std::vector<T> payload, Rank root) const
{
    if (this->rank() == root) {
        long const recv_count = static_cast<long>(payload.size());
        payload.resize(static_cast<unsigned long>(recv_count * comm.size()));
        if (comm.rank() != 0)
            std::move(begin(payload), next(begin(payload), recv_count),
                      next(begin(payload), recv_count * comm.rank()));
        comm.gather(nullptr, payload.data(), this->template get_type<T>(), recv_count);
        return Package<decltype(payload)>{ std::move(payload) };
    } else {
        comm.gather(payload.data(), this->template get_type<T>(), static_cast<long>(payload.size()),
                    nullptr, root);
        return Package<decltype(payload)>{ std::move(payload) };
    }
}

/// @memberof Communicator
/// Gather-to-all.
/// @details Same as gather, except that all processes receive the result, not just the root
/// process.
/// @see mpi::Collective::all_gather.
/// @tparam I Index to the Types parameter pack.
/// @param [in] send_first Pointer to the first element of the outgoing message buffer.
/// @param [in] send_last Pointer to the one-past-the-last element of the outgoing message buffer.
/// @param [out] recv_first Pointer to the first element of the incomming message buffer.
/// @return Pointer to the first element of the incomming message buffer.
///
template <unsigned long I>
auto all_gather(std::tuple_element_t<I, type_pack> const *send_first,
                std::tuple_element_t<I, type_pack> const *send_last,
                std::tuple_element_t<I, type_pack>       *recv_first) const
{
    comm.all_gather(send_first, get_type<I>(), std::distance(send_first, send_last), recv_first,
                    get_type<I>(), std::distance(send_first, send_last));
    return recv_first;
}
/// @memberof Communicator
/// Gather-to-all.
/// @details Same as gather, except that all processes receive the result, not just the root
/// process.
/// @see mpi::Collective::all_gather.
/// @tparam T Message type.
/// @param [in] send_first Pointer to the first element of the outgoing message buffer.
/// @param [in] send_last Pointer to the one-past-the-last element of the outgoing message buffer.
/// @param [out] recv_first Pointer to the first element of the incomming message buffer.
/// @return Pointer to the first element of the incomming message buffer.
///
template <class T> auto all_gather(T const *send_first, T const *send_last, T *recv_first) const
{
    comm.all_gather(send_first, this->template get_type<T>(), std::distance(send_first, send_last),
                    recv_first, this->template get_type<T>(), std::distance(send_first, send_last));
    return recv_first;
}

/// @memberof Communicator
/// In-place gather-to-all.
/// @see mpi::Collective::all_gather.
/// @exception If the length of the messages are not divisible by the communication group size.
/// @tparam I Index to the Types parameter pack.
/// @param [in, out] first Pointer to the first element of the message buffer.
/// @param [in, out] last Pointer to the one-past-the-last element of the message buffer.
/// @return Pointer to the first element of the message buffer.
///
template <unsigned long I>
auto all_gather(std::tuple_element_t<I, type_pack> *first,
                std::tuple_element_t<I, type_pack> *last) const
{
    if (std::distance(first, last) % comm.size())
        throw std::invalid_argument{ __PRETTY_FUNCTION__ };
    long const send_count = std::distance(first, last) / comm.size();
    comm.all_gather(nullptr, first, get_type<I>(), send_count);
    return first;
}
/// @memberof Communicator
/// In-place gather-to-all.
/// @see mpi::Collective::all_gather.
/// @exception If the length of the messages are not divisible by the communication group size.
/// @tparam T Message type.
/// @param [in, out] first Pointer to the first element of the message buffer.
/// @param [in, out] last Pointer to the one-past-the-last element of the message buffer.
/// @return Pointer to the first element of the message buffer.
///
template <class T> auto all_gather(T *first, T *last) const
{
    if (std::distance(first, last) % comm.size())
        throw std::invalid_argument{ __PRETTY_FUNCTION__ };
    long const send_count = std::distance(first, last) / comm.size();
    comm.all_gather(nullptr, first, this->template get_type<T>(), send_count);
    return first;
}

/// @memberof Communicator
/// Gather-to-all.
/// @details In this version, the gathered messages are packaged in a Package object, hence
/// freeing the caller from preparing the incomming message buffer.
/// @tparam I Index to the Types parameter pack.
/// @param payload Outgoing messages.
/// @return A Package object with the gathered messages contained in `std::vector`.
///
template <unsigned long I>
[[nodiscard]] auto all_gather(std::vector<std::tuple_element_t<I, type_pack>> payload) const
{
    long const recv_count = static_cast<long>(payload.size());
    payload.resize(static_cast<unsigned long>(recv_count * comm.size()));
    if (comm.rank() != 0)
        std::move(begin(payload), next(begin(payload), recv_count),
                  next(begin(payload), recv_count * comm.rank()));
    comm.all_gather(nullptr, payload.data(), get_type<I>(), recv_count);
    return Package<decltype(payload)>{ std::move(payload) };
}
/// @memberof Communicator
/// Gather-to-all.
/// @details In this version, the gathered messages are packaged in a Package object, hence
/// freeing the caller from preparing the incomming message buffer.
/// @tparam T Message type.
/// @param payload Outgoing messages.
/// @return A Package object with the gathered messages contained in `std::vector`.
///
template <class T> [[nodiscard]] auto all_gather(std::vector<T> payload) const
{
    long const recv_count = static_cast<long>(payload.size());
    payload.resize(static_cast<unsigned long>(recv_count * comm.size()));
    if (comm.rank() != 0)
        std::move(begin(payload), next(begin(payload), recv_count),
                  next(begin(payload), recv_count * comm.rank()));
    comm.all_gather(nullptr, payload.data(), this->template get_type<T>(), recv_count);
    return Package<decltype(payload)>{ std::move(payload) };
}

// MARK:- Blocking Scatter
//
/// @memberof Communicator
/// Inverse of the gather operation.
/// @details First, the messages being scattered from the root processes are evenly divided by the
/// communication size in the group. Then, all the processes, including the root, get their message
/// chunks in the order of their ranks. The send buffer on the root process must be equal to, or
/// greater than, the receiving message count times the communication size in the group. The send
/// buffer is ignored for all non-root processes.
/// @see mpi::Collective::scatter.
/// @tparam I Index to the Types parameter pack.
/// @param [in] send_first Pointer to the first element of the outgoing message buffer.
/// @param [out] recv_first Pointer to the first element of the incomming message buffer.
/// @param [out] recv_last Pointer to the one-past-the-last element of the incomming message buffer.
/// @param root Rank of the root process.
/// @return Pointer to the first element of the incomming message buffer.
///
template <unsigned long I>
auto scatter(std::tuple_element_t<I, type_pack> const *send_first,
             std::tuple_element_t<I, type_pack>       *recv_first,
             std::tuple_element_t<I, type_pack> *recv_last, Rank root) const
{
    if (this->rank() == root) // there must be enough outgoing messages
        comm.scatter(send_first, get_type<I>(), std::distance(recv_first, recv_last), recv_first,
                     get_type<I>(), std::distance(recv_first, recv_last), root);
    else
        comm.scatter(nullptr, recv_first, get_type<I>(), std::distance(recv_first, recv_last),
                     root);
    return recv_first;
}
/// @memberof Communicator
/// Inverse of the gather operation.
/// @details First, the messages being scattered from the root processes are evenly divided by the
/// communication size in the group. Then, all the processes, including the root, get their message
/// chunks in the order of their ranks. The send buffer on the root process must be equal to, or
/// greater than, the receiving message count times the communication size in the group. The send
/// buffer is ignored for all non-root processes.
/// @see mpi::Collective::scatter.
/// @tparam T Message type.
/// @param [in] send_first Pointer to the first element of the outgoing message buffer.
/// @param [out] recv_first Pointer to the first element of the incomming message buffer.
/// @param [out] recv_last Pointer to the one-past-the-last element of the incomming message buffer.
/// @param root Rank of the root process.
/// @return Pointer to the first element of the incomming message buffer.
///
template <class T> auto scatter(T const *send_first, T *recv_first, T *recv_last, Rank root) const
{
    if (this->rank() == root) // there must be enough outgoing messages
        comm.scatter(send_first, this->template get_type<T>(), std::distance(recv_first, recv_last),
                     recv_first, this->template get_type<T>(), std::distance(recv_first, recv_last),
                     root);
    else
        comm.scatter(nullptr, recv_first, this->template get_type<T>(),
                     std::distance(recv_first, recv_last), root);
    return recv_first;
}

/// @memberof Communicator
/// All-to-all.
/// @details `MPI_ALLTOALL` is an extension of `MPI_ALLGATHER` to the case where each process sends
/// distinct data to each of the receivers. The *j*'th block sent from process *i* is received by
/// process *j* and is placed in the *i*'th block of recvbuf.
///
/// The type signature associated with sendcount, sendtype, at a process must be equal to the type
/// signature associated with recvcount, recvtype at any other process. This implies that the amount
/// of data sent must be equal to the amount of data received, pairwise between every pair of
/// processes. As usual, however, the type maps may be different.
///
/// All arguments on all processes are significant. The argument comm must have identical values on
/// all processes.
/// @exception If the length of the messages are not divisible by the communication group size.
/// @see mpi::Comm::all2all.
/// @tparam I Index to the Types parameter pack.
/// @param [in] send_first Pointer to the first element of the outgoing message buffer.
/// @param [in] send_last Pointer to the one-past-the-last element of the outgoing message type.
/// @param [out] recv_first Pointer to the first element of the incomming message buffer.
/// @return Pointer to the first element of the incomming message buffer.
///
template <unsigned long I>
auto all2all(std::tuple_element_t<I, type_pack> const *send_first,
             std::tuple_element_t<I, type_pack> const *send_last,
             std::tuple_element_t<I, type_pack>       *recv_first) const
{
    if (std::distance(send_first, send_last) % comm.size())
        throw std::invalid_argument{ __PRETTY_FUNCTION__ };
    long const send_count = std::distance(send_first, send_last) / comm.size();
    comm.all2all(send_first, get_type<I>(), send_count, recv_first, get_type<I>(), send_count);
    return recv_first;
}
/// @memberof Communicator
/// All-to-all.
/// @details `MPI_ALLTOALL` is an extension of `MPI_ALLGATHER` to the case where each process sends
/// distinct data to each of the receivers. The *j*'th block sent from process *i* is received by
/// process *j* and is placed in the *i*'th block of recvbuf.
///
/// The type signature associated with sendcount, sendtype, at a process must be equal to the type
/// signature associated with recvcount, recvtype at any other process. This implies that the amount
/// of data sent must be equal to the amount of data received, pairwise between every pair of
/// processes. As usual, however, the type maps may be different.
///
/// All arguments on all processes are significant. The argument comm must have identical values on
/// all processes.
/// @exception If the length of the messages are not divisible by the communication group size.
/// @see mpi::Comm::all2all.
/// @tparam T Message type.
/// @param [in] send_first Pointer to the first element of the outgoing message buffer.
/// @param [in] send_last Pointer to the one-past-the-last element of the outgoing message type.
/// @param [out] recv_first Pointer to the first element of the incomming message buffer.
/// @return Pointer to the first element of the incomming message buffer.
///
template <class T> auto all2all(T const *send_first, T const *send_last, T *recv_first) const
{
    if (std::distance(send_first, send_last) % comm.size())
        throw std::invalid_argument{ __PRETTY_FUNCTION__ };
    long const send_count = std::distance(send_first, send_last) / comm.size();
    comm.all2all(send_first, this->template get_type<T>(), send_count, recv_first,
                 this->template get_type<T>(), send_count);
    return recv_first;
}

/// @memberof Communicator
/// In-place all-to-all.
/// @see mpi::Collective::all2all.
/// @exception If the length of the messages are not divisible by the communication group size.
/// @tparam I Index to the Types parameter pack.
/// @param [in, out] first Pointer to the first element of the message buffer.
/// @param [in, out] last Pointer to the one-past-the-last element of the message buffer.
/// @return Pointer to the first element of the message buffer.
///
template <unsigned long I>
auto all2all(std::tuple_element_t<I, type_pack> *first,
             std::tuple_element_t<I, type_pack> *last) const
{
    if (std::distance(first, last) % comm.size())
        throw std::invalid_argument{ __PRETTY_FUNCTION__ };
    long const send_count = std::distance(first, last) / comm.size();
    comm.all2all(nullptr, first, get_type<I>(), send_count);
    return first;
}
/// @memberof Communicator
/// In-place all-to-all.
/// @see mpi::Collective::all2all.
/// @exception If the length of the messages are not divisible by the communication group size.
/// @tparam T Message type.
/// @param [in, out] first Pointer to the first element of the message buffer.
/// @param [in, out] last Pointer to the one-past-the-last element of the message buffer.
/// @return Pointer to the first element of the message buffer.
///
template <class T> auto all2all(T *first, T *last) const
{
    if (std::distance(first, last) % comm.size())
        throw std::invalid_argument{ __PRETTY_FUNCTION__ };
    long const send_count = std::distance(first, last) / comm.size();
    comm.all2all(nullptr, first, this->template get_type<T>(), send_count);
    return first;
}

/// @memberof Communicator
/// All-to-all.
/// @details In this version, the gathered messages are packaged in a Package object, hence
/// freeing the caller from preparing the incomming message buffer.
/// @tparam I Index to the Types parameter pack.
/// @param payload Outgoing messages.
/// @return A Package object with the gathered messages contained in `std::vector`.
///
template <unsigned long I>
[[nodiscard]] auto all2all(std::vector<std::tuple_element_t<I, type_pack>> payload) const
{
    if (std::distance(begin(payload), end(payload)) % comm.size())
        throw std::invalid_argument{ __PRETTY_FUNCTION__ };
    long const send_count = std::distance(begin(payload), end(payload)) / comm.size();
    comm.all2all(nullptr, payload.data(), get_type<I>(), send_count);
    return Package<decltype(payload)>{ std::move(payload) };
}
/// @memberof Communicator
/// All-to-all.
/// @details In this version, the gathered messages are packaged in a Package object, hence
/// freeing the caller from preparing the incomming message buffer.
/// @tparam T Message type.
/// @param payload Outgoing messages.
/// @return A Package object with the gathered messages contained in `std::vector`.
///
template <class T> [[nodiscard]] auto all2all(std::vector<T> payload) const
{
    if (std::distance(begin(payload), end(payload)) % comm.size())
        throw std::invalid_argument{ __PRETTY_FUNCTION__ };
    long const send_count = std::distance(begin(payload), end(payload)) / comm.size();
    comm.all2all(nullptr, payload.data(), this->template get_type<T>(), send_count);
    return Package<decltype(payload)>{ std::move(payload) };
}
/// @}

#endif /* ParallelKit_InterProcessComm_Collective_hh */
