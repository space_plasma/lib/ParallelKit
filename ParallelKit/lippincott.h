/*
 * Copyright (c) 2020-2021, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#ifndef ParallelKit_lippincott_h
#define ParallelKit_lippincott_h

#include "ParallelKit-config.h"

#include <cstdlib>
#include <exception>
#include <string>

PARALLELKIT_NAMESPACE_BEGIN(1)
void print_backtrace();

// error handling routines
//
namespace {

[[noreturn, maybe_unused]] void fatal_error(char const *reason) noexcept
{
    std::puts(reason);
    print_backtrace();
    std::abort();
}
[[noreturn, maybe_unused]] void fatal_error(std::string const &reason) noexcept
{
    fatal_error(reason.c_str());
}
[[noreturn, maybe_unused]] void lippincott() noexcept
try {
    throw;
} catch (std::exception const &e) {
    fatal_error(e.what());
} catch (...) {
    fatal_error("Unknown exception");
}

} // namespace
PARALLELKIT_NAMESPACE_END(1)

#endif /* ParallelKit_lippincott_h */
