/*
 * Copyright (c) 2021, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#ifndef ParallelKit_InterProcessComm_h
#define ParallelKit_InterProcessComm_h

#include <ParallelKit/MPICollective.h>
#include <ParallelKit/MPIComm.h>
#include <ParallelKit/MPIComparison.h>
#include <ParallelKit/MPIGroup.h>
#include <ParallelKit/MPIP2P.h>
#include <ParallelKit/MPIRank.h>
#include <ParallelKit/MPIReduceOp.h>
#include <ParallelKit/MPIReduction.h>
#include <ParallelKit/MPIRequest.h>
#include <ParallelKit/MPITag.h>
#include <ParallelKit/MPIType.h>
#include <ParallelKit/TypeMap.h>

#include <ParallelKit/InterProcessComm-Core.h>

/// @namespace mpi
/// Namespace for MPI wrappers.

#endif /* ParallelKit_InterProcessComm_h */
