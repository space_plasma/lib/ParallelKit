/*
 * Copyright (c) 2017-2021, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#if defined(__APPLE__) && defined(__OBJC__)
#import <Cocoa/Cocoa.h>

//! Project version number for ParallelKit.
FOUNDATION_EXPORT double ParallelKitVersionNumber;

//! Project version string for ParallelKit.
FOUNDATION_EXPORT const unsigned char ParallelKitVersionString[];
#endif

// In this header, you should import all the public headers of your framework using statements like
// #import <ParallelKit/PublicHeader.h>

/// @file
/// `ParallelKit` umbrella header.
///
/// The whole library API can be included using
///
/// > #include <ParallelKit/ParallelKit.h>
///
/// This is an umbrella header which includes the two parts of API:
/// - Inter-thread message passing, found at ParallelKit/InterThreadComm.h; and
/// - Inter-process message passing.
///

/// @def PARALLELKIT_INLINE_VERSION
/// Defines the version namespace to be inlined
///
/// Defining this macro with the version number to be inlined before including the library headers
/// enables inlining the specified version namespace.
/// @see ParallelKit/ParallelKit-config.h.
///
#if !defined(PARALLELKIT_INLINE_VERSION)
// default version selection
//
#define PARALLELKIT_INLINE_VERSION 1
#endif

#if defined(__cplusplus)

#include <ParallelKit/InterProcessComm.h>
#include <ParallelKit/InterThreadComm.h>

/// @namespace parallel
/// Namespace for ParallelKit.
///

#endif
