/*
 * Copyright (c) 2021, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

/// @file

#ifndef ParallelKit_TypeMap_h
#define ParallelKit_TypeMap_h

#include <ParallelKit/ParallelKit-config.h>

#include <ParallelKit/MPIType.h>

#include <array>
#include <complex>
#include <tuple>
#include <type_traits>
#include <utility>
#include <vector>

PARALLELKIT_NAMESPACE_BEGIN(1)

/// Adapter for Type construction
/// @details For a given type `T`, requirements for TypeMap<T> are:
///
/// - Make `TypeMap<T>' class be default-constructible;
/// - Define a call operator with no argument and that returns the Type instance corresponding to
/// `T`;
/// - All subsequent call to the call operator must return a Type instance whose typemap is
/// equivalent to that of the first call (that is, any operations using all Type instances returned
/// from this call are expected to behave the same as using any of the Type instances);
/// - Optionally, declare type alias `type = T`.
///
template <class T> struct TypeMap;

// MARK:- Type Maker
/// @name Type Maker
/// @{

/// Create a Type object compositing `Types`.
///
template <class... Types> [[nodiscard]] auto make_type()
{
    static_assert(sizeof...(Types) > 0);
    if constexpr (sizeof...(Types) == 1)
        return TypeMap<std::decay_t<Types>...>{}();
    else
        return mpi::Type::composite(make_type<Types>()...);
}
/// Create a Type object compositing `Types`.
/// @details The argument is for automatic type deduction and will be untouched.
///
template <class... Types> [[nodiscard]] auto make_type(Types &&...)
{
    return make_type<Types...>();
}
/// @}

// MARK:- Native Types
//
/// @name TypeMap for Native Types
/// @{

/// TypeMap for `char`
///
template <> struct TypeMap<char> {
    using type = char;
    [[nodiscard]] auto operator()() const noexcept { return mpi::Type::Native<char>(); }
};
/// TypeMap for `unsigned char`
///
template <> struct TypeMap<unsigned char> {
    using type = unsigned char;
    [[nodiscard]] auto operator()() const noexcept { return mpi::Type::Native<unsigned char>(); }
};

/// TypeMap for `short`
///
template <> struct TypeMap<short> {
    using type = short;
    [[nodiscard]] auto operator()() const noexcept { return mpi::Type::Native<short>(); }
};
/// TypeMap for `unsigned short`
///
template <> struct TypeMap<unsigned short> {
    using type = unsigned short;
    [[nodiscard]] auto operator()() const noexcept { return mpi::Type::Native<unsigned short>(); }
};

/// TypeMap for `int`
///
template <> struct TypeMap<int> {
    using type = int;
    [[nodiscard]] auto operator()() const noexcept { return mpi::Type::Native<int>(); }
};
/// TypeMap for `unsigned int`
///
template <> struct TypeMap<unsigned> {
    using type = unsigned;
    [[nodiscard]] auto operator()() const noexcept { return mpi::Type::Native<unsigned>(); }
};

/// TypeMap for `long`
///
template <> struct TypeMap<long> {
    using type = long;
    [[nodiscard]] auto operator()() const noexcept { return mpi::Type::Native<long>(); }
};
/// TypeMap for `unsigned long`
///
template <> struct TypeMap<unsigned long> {
    using type = unsigned long;
    [[nodiscard]] auto operator()() const noexcept { return mpi::Type::Native<unsigned long>(); }
};

/// TypeMap for `float`
///
template <> struct TypeMap<float> {
    using type = float;
    [[nodiscard]] auto operator()() const noexcept { return mpi::Type::Native<float>(); }
};
/// TypeMap for `double`
///
template <> struct TypeMap<double> {
    using type = double;
    [[nodiscard]] auto operator()() const noexcept { return mpi::Type::Native<double>(); }
};
/// @}

// MARK:- stdlib Types
//
/// @name TypeMap for stdlib Types
/// @{

/// TypeMap for `std::complex`
///
template <class T> struct TypeMap<std::complex<T>> {
    using type = std::complex<T>;
    [[nodiscard]] auto operator()() const
    {
        return make_type<T>().template vector<sizeof(type) / sizeof(T)>();
    }
};

/// TypeMap for `std::pair`
///
template <class T, class U> struct TypeMap<std::pair<T, U>> {
    using type = std::pair<T, U>;
    [[nodiscard]] auto operator()() const
    {
        if constexpr (std::is_same_v<T, U>)
            return make_type<T>().template vector<sizeof(type) / sizeof(T)>();
        else
            return make_type<T, U>();
    }
};

/// @cond
template <class... Ts> struct TypeMap<std::tuple<Ts...>> {
    static_assert(!sizeof...(Ts), "the memory layout of std::tuple is ABI-dependent");
    using type = std::tuple<Ts...>;
    TypeMap()  = delete;
};
/// @endcond

/// TypeMap for `std::array`
///
template <class T, unsigned long N> struct TypeMap<std::array<T, N>> {
    using type = std::array<T, N>;
    [[nodiscard]] auto operator()() const { return make_type<T>().template vector<N>(); }
};
/// @}

PARALLELKIT_NAMESPACE_END(1)

#endif /* ParallelKit_TypeMap_h */
