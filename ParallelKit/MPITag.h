/*
 * Copyright (c) 2021-2022, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#ifndef ParallelKit_MPITag_h
#define ParallelKit_MPITag_h

#include <ParallelKit/ParallelKit-config.h>

#include <ParallelKit/mpi_wrapper.h>

PARALLELKIT_NAMESPACE_BEGIN(1)
namespace mpi {

/// Wrapper for MPI tag.
/// @details No range check is performed for a given tag.
///
/// The range of valid tag values is *0 <= tag <=* `MPI_TAG_UB`.
/// MPI requires that `MPI_TAG_UB` be no less than 32767.
///
struct [[nodiscard]] Tag {
    int v{ MPI_UNDEFINED };
    //
    constexpr          operator int() const noexcept { return v; }
    constexpr explicit operator bool() const noexcept { return v != MPI_UNDEFINED; }

    [[nodiscard]] static constexpr Tag  any() noexcept { return { MPI_ANY_TAG }; } //!< Any tag.
    [[nodiscard]] friend constexpr bool operator==(Tag const &A, Tag const &B) noexcept
    {
        return A.v == B.v;
    }
};

} // namespace mpi
PARALLELKIT_NAMESPACE_END(1)

#endif /* ParallelKit_MPITag_h */
