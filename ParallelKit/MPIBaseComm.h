/*
 * Copyright (c) 2021-2022, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#ifndef ParallelKit_MPIBaseComm_h
#define ParallelKit_MPIBaseComm_h

#include <ParallelKit/ParallelKit-config.h>

#include <ParallelKit/MPIComparison.h>
#include <ParallelKit/MPIGroup.h>
#include <ParallelKit/MPIRank.h>

#include <string>
#include <utility>

#include <ParallelKit/mpi_wrapper.h>

PARALLELKIT_NAMESPACE_BEGIN(1)
namespace mpi {

// forward decl
//
/// @cond
class Comm;
/// @endcond

/// Wrapper for `MPI_Comm`.
///
class BaseComm {
    MPI_Comm m_comm{ MPI_COMM_NULL };

protected:
    ~BaseComm();
    BaseComm() noexcept = default;

    /// @{
    BaseComm(BaseComm &&o) noexcept { std::swap(m_comm, o.m_comm); }
    BaseComm &operator=(BaseComm &&o) noexcept
    {
        std::swap(m_comm, o.m_comm);
        return *this;
    }
    /// @}

    BaseComm(MPI_Comm comm, decltype(nullptr)) noexcept
    : m_comm{ comm } {}               // this is for predefined comm's
    explicit BaseComm(MPI_Comm comm); // custom error handler is set

public:
    // MARK:- MPI Init/Finalize
    /// @name MPI Init/Finalize
    /// @{

    /// Initialize the MPI execution environment.
    /// @param argc Pointer to the number of arguments.
    /// @param argv Pointer to the argument vector.
    ///
    static int init(int *argc, char ***argv) noexcept;
    /// Initialize the MPI execution environment.
    /// @param argc Pointer to the number of arguments.
    /// @param argv Pointer to the argument vector.
    /// @param required Level of desired thread support.
    /// @param [out] provided Level of provided thread support.
    ///
    static int init(int *argc, char ***argv, int required, int &provided) noexcept;

    /// Return true if `MPI_Init` is called.
    ///
    [[nodiscard]] static bool is_initialized() noexcept;

    /// Terminates MPI execution environment.
    ///
    static int deinit() noexcept;

    /// Return true if `MPI_Finalize` is called.
    ///
    [[nodiscard]] static bool is_finalized() noexcept;
    /// @}

public:
    // MARK:- Query
    /// @name Query
    /// @{

    /// Returns true if `*this` is valid, i.e., not `MPI_COMM_NULL`.
    ///
    explicit operator bool() const noexcept { return m_comm != MPI_COMM_NULL; }

    /// Returns the associated `MPI_Comm` handle.
    ///
    [[nodiscard]] auto operator*() const noexcept { return m_comm; }

    /// Associates a label with `*this`.
    ///
    void set_label(char const *label);
    /// Returns the label associated with `*this`.
    ///
    [[nodiscard]] std::string label() const;

    /// Determines the size of the group associated with `*this`.
    ///
    [[nodiscard]] int size() const;

    /// Determines the rank of the calling process in `*this`.
    ///
    [[nodiscard]] Rank rank() const;

    /// Compare two communicators.
    /// @return
    /// - Comparison::identical if and only if `*this` and other are handles for the same object
    /// (identical groups and same contexts).
    /// - Comparison::congruent if the underlying groups are identical in constituents and rank
    /// order; these communicators differ only by context.
    /// - Comparison::similar if the group members of `*this` and other are the same but the rank
    /// order differs.
    /// - Comparison::unequal results otherwise.
    ///
    [[nodiscard]] Comparison compare(BaseComm const &other) const;

    /// Returns Group associated with `*this`.
    ///
    [[nodiscard]] Group group() const;
    /// @}
};

} // namespace mpi
PARALLELKIT_NAMESPACE_END(1)

#endif /* ParallelKit_MPIBaseComm_h */
