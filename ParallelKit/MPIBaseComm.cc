/*
 * Copyright (c) 2021, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "MPIBaseComm.h"

#include "println.h"

#include <iostream>
#include <stdexcept>
#include <string>

PARALLELKIT_NAMESPACE_BEGIN(1)

mpi::BaseComm::~BaseComm()
{
    if (MPI_COMM_NULL != m_comm && MPI_COMM_WORLD != m_comm && MPI_COMM_SELF != m_comm
        && MPI_Comm_free(&m_comm) != MPI_SUCCESS) {
        println(std::cerr, "%% ", __PRETTY_FUNCTION__, " - MPI_Comm_free(...) returned error");
#if defined(PARALLELKIT_SHOULD_TERMINATE_ON_FAILURE_IN_DESTRUCTOR)                                 \
    && PARALLELKIT_SHOULD_TERMINATE_ON_FAILURE_IN_DESTRUCTOR
        std::terminate();
#endif
    }
}
mpi::BaseComm::BaseComm(MPI_Comm comm)
{
#if defined(PARALLELKIT_COMM_SET_ERROR_HANDLER) && PARALLELKIT_COMM_SET_ERROR_HANDLER
    if (MPI_COMM_NULL != comm && MPI_Comm_set_errhandler(comm, MPI_ERRORS_RETURN) != MPI_SUCCESS) {
        throw std::runtime_error{ std::string{ __PRETTY_FUNCTION__ }
                                  + " - MPI_Comm_set_errhandler(...) returned error" };
    }
#endif
    m_comm = comm;
}

int mpi::BaseComm::init(int *argc, char ***argv) noexcept
{
    return MPI_Init(argc, argv);
}
int mpi::BaseComm::init(int *argc, char ***argv, int const required, int &provided) noexcept
{
    return MPI_Init_thread(argc, argv, required, &provided);
}
int mpi::BaseComm::deinit() noexcept
{
    return MPI_Finalize();
}
bool mpi::BaseComm::is_initialized() noexcept
{
    int initialized;
    MPI_Initialized(&initialized);
    return initialized;
}
bool mpi::BaseComm::is_finalized() noexcept
{
    int finalized;
    MPI_Finalized(&finalized);
    return finalized;
}

void mpi::BaseComm::set_label(char const *label)
{
    if (MPI_Comm_set_name(m_comm, label) != MPI_SUCCESS) {
        throw std::runtime_error{ std::string{ __PRETTY_FUNCTION__ }
                                  + " - MPI_Comm_set_name(...) returned error" };
    }
}
std::string mpi::BaseComm::label() const
{
    char label[MPI_MAX_OBJECT_NAME];
    int  len;
    if (MPI_Comm_get_name(m_comm, label, &len) != MPI_SUCCESS) {
        throw std::runtime_error{ std::string{ __PRETTY_FUNCTION__ }
                                  + " - MPI_Comm_get_name(...) returned error" };
    }
    return label;
}

int mpi::BaseComm::size() const
{
    int size;
    if (MPI_Comm_size(m_comm, &size) != MPI_SUCCESS) {
        throw std::runtime_error{ std::string{ __PRETTY_FUNCTION__ }
                                  + " - MPI_Comm_size(...) returned error" };
    }
    return size;
}
auto mpi::BaseComm::rank() const -> Rank
{
    Rank rank;
    if (MPI_Comm_rank(m_comm, &rank.v) != MPI_SUCCESS) {
        throw std::runtime_error{ std::string{ __PRETTY_FUNCTION__ }
                                  + " - MPI_Comm_rank(...) returned error" };
    }
    return rank;
}

auto mpi::BaseComm::compare(BaseComm const &other) const -> Comparison
{
    int result;
    if (MPI_Comm_compare(m_comm, *other, &result) != MPI_SUCCESS) {
        throw std::runtime_error{ std::string{ __PRETTY_FUNCTION__ }
                                  + " - MPI_Comm_compare(...) returned error" };
    }
    switch (result) {
        case MPI_IDENT:
            return identical;
        case MPI_CONGRUENT:
            return congruent;
        case MPI_SIMILAR:
            return similar;
        case MPI_UNEQUAL:
            return unequal;
        default:
            throw std::domain_error{ std::string{ __PRETTY_FUNCTION__ }
                                     + " - unknown Comparison enum" };
    }
}

auto mpi::BaseComm::group() const -> Group
{
    MPI_Group g;
    if (MPI_Comm_group(m_comm, &g) != MPI_SUCCESS) {
        throw std::runtime_error{ std::string{ __PRETTY_FUNCTION__ }
                                  + " - MPI_Comm_group(...) returned error" };
    }
    return Group{ g };
}

PARALLELKIT_NAMESPACE_END(1)
