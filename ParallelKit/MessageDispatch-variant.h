/*
 * Copyright (c) 2020-2022, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#ifndef ParallelKit_MessageDispatch_variant_h
#define ParallelKit_MessageDispatch_variant_h

#include <ParallelKit/ParallelKit-config.h>

#include <algorithm>
#include <atomic>
#include <functional>
#include <iterator>
#include <map>
#include <memory>
#include <optional>
#include <queue>
#include <stdexcept>
#include <thread>
#include <type_traits>
#include <utility>
#include <variant>
#include <vector>

PARALLELKIT_NAMESPACE_BEGIN(1)
#ifndef DOXYGEN_SHOULD_SKIP_THIS
/// @cond
namespace _ver_variant {
/// @endcond

/// MPI-like Inter-thread message (payload) passing machinary
///
/// This class coordinates message passing among the threads with the workflow similar to the MPI
/// specification. Under the hood, this is implemented with modern C++ design. Synchoronization of
/// shared memory acces is implemented using `std::atomic_flag`.
///
/// Unlike MPI, payload types are NOT part of the message signature.
/// Therefore, mixed ordering of send-recv pairs for different payload types will throw an
/// exception.
///
/// Similar to the MPI specification, the sender-receiver matching mechanism (called Envelope) is
/// also implemented. Every message has a tag (like a sender/receiver pair) associated with it. In
/// addition to the message type, messages are also classified by tags. So, messages with different
/// tags do not interfere.
///
/// Similar to the MPI specification, callers interact with this object with peer-to-peer
/// communication, and collective and reduction operations.
///
/// @tparam Payloads Types of payloads (messages) to pass among the threads, which should be
/// move-constructible.
/// @sa MessageDispatch::Communicator
///
template <class... Payloads> class MessageDispatch {
    static_assert((... && std::is_move_constructible_v<Payloads>),
                  "Payloads should be move-constructible");

private:
    class Queue;
    class [[nodiscard]] Tracker;

public:
    class Communicator;
    using payload_t = std::variant<Payloads...>;

    /// Sender/receiver ID type
    ///
    class [[nodiscard]] Rank {
        template <class T>
        static constexpr bool is_int_v = std::is_integral_v<T> && (sizeof(T) <= sizeof(int));

        long rank{ -1 };

    public:
        constexpr Rank() noexcept = default;

        /// Construct a Rank object using an integral ID
        /// @tparam T An integral type of size less than or equal to `sizeof(int)`.
        ///
        template <class T, std::enable_if_t<is_int_v<T>, int> = 0>
        constexpr Rank(T rank) noexcept : rank{ rank }
        {
        }

        /// Implicit type cast-style value unwraping
        [[nodiscard]] operator long() const noexcept { return rank; }

        /// @cond
        [[nodiscard]] friend constexpr bool operator<(Rank const &lhs, Rank const &rhs) noexcept
        {
            return lhs.rank < rhs.rank;
        }
        /// @endcond
    };

    /// Message tag
    ///
    /// This class is used to classify messages based on tags.
    /// Messages with the same tag
    ///
    class [[nodiscard]] Envelope {
        long most, least;

    public:
        /// Construct an Envelope object using a sender/receiver identifier pair.
        ///
        constexpr Envelope(Rank const most, Rank const least) noexcept
        : most{ most }, least{ least }
        {
        }

        /// @cond
        [[nodiscard]] friend constexpr bool operator<(Envelope const &lhs,
                                                      Envelope const &rhs) noexcept
        {
            if (lhs.most == rhs.most)
                return lhs.least < rhs.least;
            else
                return lhs.most < rhs.most;
        }
        /// @endcond
    };

    /// Package tracker
    ///
    /// Ticket is in analogy a tracking number.
    /// The Ticket class tracks the Package delivery.
    ///
    /// @sa Package
    ///
    class [[nodiscard]] Ticket {
        friend Tracker;
        std::shared_ptr<std::atomic_flag> flag;
        Ticket(std::weak_ptr<std::atomic_flag> &weak) : flag{ std::make_shared<std::atomic_flag>() }
        {
            flag->test_and_set();
            weak = flag;
        }

    public:
        /// Default constructor
        ///
        /// A default-constructed object is in an invalid state.
        /// Calling wait on it is ill-formed.
        ///
        Ticket() noexcept          = default;
        Ticket(Ticket &&) noexcept = default;
        Ticket &operator=(Ticket &&) noexcept = default;

        /// Wait for package delivery
        ///
        /// It blocks the calling thread until the package is delievered.
        ///
        void wait() &&noexcept
        { // wait for delivery
            // deliberately not check flag presence; multiple calls are ill-formed
            while (flag->test_and_set(std::memory_order_acquire)) {
                std::this_thread::yield();
            }
            flag.reset();
        }
    };

private:
    // payload tracker
    //
    class [[nodiscard]] Tracker {
        friend Queue;
        [[nodiscard]] operator Ticket() & { return flag; }

    protected:
        payload_t                       payload;
        std::weak_ptr<std::atomic_flag> flag;

    public:
        Tracker &operator=(Tracker &&)                                                = delete;
        Tracker(Tracker &&) noexcept(std::is_nothrow_move_constructible_v<payload_t>) = default;
        Tracker(payload_t &&payload) noexcept(std::is_nothrow_move_constructible_v<payload_t>)
        : payload{ std::move(payload) }
        {
        }
    };

public:
    /// Wrapper for a payload to be unpacked by a receiver
    ///
    /// This class provides the mechanism for payload delivery notification to sender.
    /// Unpacking the package on the receiving side triggers the delivery notification for which the
    /// sender may wait. There is no other mechanism for triggering the notification.
    ///
    /// @tparam Payload Type of payload that `*this` wraps.
    /// @sa Ticket
    ///
    template <class Payload> class [[nodiscard]] Package : private Tracker {
        static_assert(std::is_constructible_v<payload_t, Payload &&>,
                      "no alternative for the given payload type");
        //
        class [[nodiscard]] Guard {
            std::shared_ptr<std::atomic_flag> flag;

        public:
            Guard(std::weak_ptr<std::atomic_flag> &&weak) noexcept(noexcept(weak.lock()))
            : flag{ weak.lock() }
            {
            }
            ~Guard() noexcept
            { // notify of delivery
                if (flag)
                    flag->clear(std::memory_order_release);
            }
            template <class F, class... Args>
            auto invoke(F &&f, Args &&...args) const
                noexcept(std::is_nothrow_invocable_v<F &&, Args &&...>)
                    -> std::invoke_result_t<F &&, Args &&...>
            { // invoke the callable
                static_assert(std::is_invocable_v<F &&, Args &&...>);
                return std::invoke(std::forward<F>(f), std::forward<Args>(args)...);
            }
        };

    public:
        /// @cond
        Package(Tracker &&t) noexcept(std::is_nothrow_move_constructible_v<Tracker>)
        : Tracker{ std::move(t) }
        {
        }
        /// @endcond
    public:
        /// Unpack and process the message
        ///
        /// This function does both the unpacking and processing the message.
        /// The caller passes a function object which processes the message,
        /// and `*this` invokes the call operator inside a guard.
        /// The delivery notification is fired upon return to the caller.
        ///
        /// @tparam F Type of the function object with signature `Ret(Payload&&, RestArgs&&)`.
        /// @tparam RestArgs Types of additional arguments passed to the function object.
        /// @param f Function object which processes the message.
        /// @param rest_args Additional arguments which will be passed to `f`.
        /// @return The return value of `f`.
        ///
        template <class F, class... RestArgs>
        auto unpack(F &&f, RestArgs &&...rest_args) && // std::get may throw exception
            -> std::invoke_result_t<F &&, Payload &&, RestArgs &&...>
        {
            static_assert(std::is_invocable_v<F &&, Payload &&, RestArgs &&...>);
            // invoke the callable with payload as its first argument
            return Guard{ std::move(this->flag) }.invoke(
                std::forward<F>(f), std::get<Payload>(std::move(this->payload)),
                std::forward<RestArgs>(rest_args)...);
        }
        /// Type-cast style unpack
        ///
        /// The delivery notification is fired immediately upon return.
        ///
        [[nodiscard]] operator Payload() && // std::get may throw exception
        {
            Guard const g = std::move(this->flag); // notify of delivery upon destruction
            return std::get<Payload>(std::move(this->payload));
        }
        /// Pointer-derefencing style unpack
        ///
        /// The delivery notification is fired immediately upon return.
        ///
        [[nodiscard]] Payload operator*() && { return static_cast<Package &&>(*this); }
    };

private:
    // per-Payload message queue
    //
    class Queue {
        using queue_t = std::queue<Tracker>;
        struct [[nodiscard]] mapped_t {
            queue_t          q{};
            std::atomic_flag flag = ATOMIC_FLAG_INIT;
        };
        using map_t = std::map<Envelope, mapped_t>;
        map_t map{};

    public: // constructor
        Queue() noexcept(std::is_nothrow_default_constructible_v<map_t>)      = default;
        Queue(Queue &&) noexcept(std::is_nothrow_move_constructible_v<map_t>) = default;
        Queue &operator=(Queue &&) noexcept(std::is_nothrow_move_assignable_v<map_t>) = default;
        explicit Queue(std::vector<Envelope> const &addresses)
        {
            for (auto const &address : addresses) {
                map.try_emplace(map.end(), address);
            }
        }

    private:
        class [[nodiscard]] Guard { // guarded invocation
            std::atomic_flag &flag;

        public:
            template <bool should_yield>
            Guard(std::atomic_flag &f, std::integral_constant<bool, should_yield>) noexcept
            : flag{ f }
            { // acquire access
                while (flag.test_and_set(std::memory_order_acquire)) {
                    if constexpr (should_yield) {
                        std::this_thread::yield();
                    }
                }
            }
            ~Guard() noexcept { flag.clear(std::memory_order_release); } // relinquish access
            template <class F, class... Args>
            auto invoke(F &&f, Args &&...args) const
                noexcept(std::is_nothrow_invocable_v<F &&, Args &&...>)
                    -> std::invoke_result_t<F &&, Args &&...>
            { // invoke the callable synchronously
                static_assert(std::is_invocable_v<F &&, Args &&...>);
                return std::invoke(std::forward<F>(f), std::forward<Args>(args)...);
            }
        };
        //
        [[nodiscard]] static auto push_back(queue_t &q, payload_t &&payload) -> Ticket
        {
            return q.emplace(std::move(payload));
        }
        [[nodiscard]] static auto pop_front(queue_t &q) -> std::optional<Tracker>
        {
            if (!q.empty()) {
                auto payload = std::move(q.front()); // must take the ownership of payload
                q.pop();
                return payload;
            }
            return std::nullopt;
        }
        [[nodiscard]] decltype(auto) at(Envelope const key) &
        {
            try {
                return map.at(key);
            } catch (std::out_of_range const &) {
                throw std::out_of_range{ __PRETTY_FUNCTION__ };
            }
        }

    public:
        [[nodiscard]] auto enqueue(Envelope const key, payload_t payload) & -> Ticket
        {
            auto &[q, flag] = at(key);
            return Guard{ flag, std::false_type{} }.invoke(&push_back, q, std::move(payload));
        }
        [[nodiscard]] auto try_dequeue(Envelope const key) & -> std::optional<Tracker>
        {
            auto &[q, flag] = at(key);
            return Guard{ flag, std::false_type{} }.invoke(&pop_front, q);
        }
        [[nodiscard]] auto dequeue(Envelope const key) & -> Tracker
        {
            auto &[q, flag] = at(key);
            do {
                if (auto opt = Guard{ flag, std::true_type{} }.invoke(&pop_front, q)) {
                    return *std::move(opt);
                }
                std::this_thread::yield();
            } while (true);
        }
    } queue{};

public: // constructor
    /// @{
    MessageDispatch() noexcept(std::is_nothrow_default_constructible_v<Queue>) = default;
    MessageDispatch(MessageDispatch &&) noexcept(std::is_nothrow_move_constructible_v<Queue>)
        = default;
    MessageDispatch &
    operator=(MessageDispatch &&) noexcept(std::is_nothrow_move_assignable_v<Queue>)
        = default;

    /// Construct an object with predefined sender/receiver pairs
    /// @details Using tags other than those that `*this` is initialized with will throw an
    /// exception.
    /// @param addresses Predefined sender/receiver pairs.
    ///
    explicit MessageDispatch(std::vector<Envelope> const &addresses) : queue{ addresses } {}
    /// Construct an object with predefined sender/receiver pairs
    /// @details Using tags other than those that `*this` is initialized with will throw an
    /// exception.
    /// @param world_size Size of predefined sender/receiver pairs.
    ///
    explicit MessageDispatch(unsigned const world_size)
    {
        std::vector<Envelope> addresses;
        addresses.reserve(world_size * world_size);
        for (unsigned i = 0; i < world_size; ++i) {
            for (unsigned j = 0; j < world_size; ++j) {
                addresses.emplace_back(i, j);
            }
        }
        *this = MessageDispatch{ addresses };
    }
    /// @}

public: // communication methods
    /// @{
    /// Send a message
    /// @tparam I Index to the **Payloads...** parameter pack.
    /// @param payload A message to send.
    /// @param envelope A sender/receiver pair.
    /// @return A Ticket object.
    ///
    template <unsigned long I>
    [[nodiscard]] auto send(std::variant_alternative_t<I, payload_t> const &payload,
                            Envelope const                                  envelope) -> Ticket
    {
        return queue.enqueue(envelope, payload);
    }
    template <unsigned long I>
    [[nodiscard]] auto send(std::variant_alternative_t<I, payload_t> &&payload,
                            Envelope const                             envelope) -> Ticket
    {
        return queue.enqueue(envelope, std::move(payload));
    }
    /// Send a message
    /// @tparam Payload Type of the message.
    /// It must be uniquely identified in `payload_t`.
    /// @param payload A message to send.
    /// @param envelope A sender/receiver pair.
    /// @return A Ticket object.
    ///
    template <class Payload>
    [[nodiscard]] auto send(Payload &&payload, Envelope const envelope) -> Ticket
    {
        static_assert(std::is_constructible_v<payload_t, decltype(payload)>,
                      "no alternative for the given payload type");
        return queue.enqueue(envelope, std::forward<Payload>(payload));
    }
    /// @}

    /// @{
    /// Receive a message
    /// @tparam I Index to the **Payloads...** parameter pack.
    /// @param envelope A sender/receiver pair.
    /// @return A Package object.
    ///
    template <unsigned long I>
    [[nodiscard]] auto recv(Envelope const envelope)
        -> Package<std::variant_alternative_t<I, payload_t>>
    {
        return queue.dequeue(envelope);
    }
    /// Receive a message
    /// @tparam Payload Type of the message.
    /// It must be uniquely identified in `payload_t`.
    /// @param envelope A sender/receiver pair.
    /// @return A Package object.
    ///
    template <class Payload> [[nodiscard]] auto recv(Envelope const envelope) -> Package<Payload>
    {
        return queue.dequeue(envelope);
    }
    /// @}

    /// @{
    /// Retrieve, if any, a message
    /// @tparam I Index to the **Payloads...** parameter pack.
    /// @param envelope A sender/receiver pair as a tag.
    /// @return An optional encapsulating a Package object if a message has been retrieved;
    /// otherwise, returns a nil optional.
    ///
    template <unsigned long I>
    [[nodiscard]] auto try_recv(Envelope const envelope)
        -> std::optional<Package<std::variant_alternative_t<I, payload_t>>>
    {
        return queue.try_dequeue(envelope);
    }
    /// Retrieve, if any, a message
    /// @tparam Payload Type of the message.
    /// It must be uniquely identified in `payload_t`.
    /// @param envelope A sender/receiver pair as a tag.
    /// @return An optional encapsulating a Package object if a message has been retrieved;
    /// otherwise, returns a nil optional.
    ///
    template <class Payload>
    [[nodiscard]] auto try_recv(Envelope const envelope) -> std::optional<Package<Payload>>
    {
        return queue.try_dequeue(envelope);
    }
    /// @}

    /// @{
    /// Scatter multiple messages to multiple recipients in one call
    /// @tparam I Index to the **Payloads...** parameter pack.
    /// @param payloads A list of messages to send.
    /// @param dests A list of sender/receiver pairs in the matching order with `payloads`.
    /// @return A list of Ticket objects.
    ///
    template <unsigned long I>
    [[nodiscard]] auto scatter(std::vector<std::variant_alternative_t<I, payload_t>> payloads,
                               std::vector<Envelope> const                          &dests)
    {
        if (payloads.size() != dests.size()) {
            throw std::invalid_argument{ __PRETTY_FUNCTION__ };
        }
        std::vector<Ticket> tks(payloads.size());
        std::transform(
            std::make_move_iterator(begin(payloads)), std::make_move_iterator(end(payloads)),
            begin(dests), begin(tks),
            [this](std::variant_alternative_t<I, payload_t> payload, Envelope const &dest) {
                return send<I>(std::move(payload), dest);
            });
        return tks; // NRVO
    }
    /// Scatter multiple messages to multiple recipients in one call
    /// @tparam Payload Type of the message.
    /// It must be uniquely identified in `payload_t`.
    /// @param payloads A list of messages to send.
    /// @param dests A list of sender/receiver pairs in the matching order with `payloads`.
    /// @return A list of Ticket objects.
    ///
    template <class Payload>
    [[nodiscard]] auto scatter(std::vector<Payload> payloads, std::vector<Envelope> const &dests)
    {
        static_assert(std::is_constructible_v<payload_t, Payload>,
                      "no alternative for the given payload type");
        if (payloads.size() != dests.size()) {
            throw std::invalid_argument{ __PRETTY_FUNCTION__ };
        }
        std::vector<Ticket> tks(payloads.size());
        std::transform(std::make_move_iterator(begin(payloads)),
                       std::make_move_iterator(end(payloads)), begin(dests), begin(tks),
                       [this](Payload payload, Envelope const &dest) {
                           return send(std::move(payload), dest);
                       });
        return tks; // NRVO
    }
    /// @}

    /// @{
    /// Broadcast a single message to multiple recipients
    /// @tparam I Index to the **Payloads...** parameter pack.
    /// @param payload A message to broadcast.
    /// @param dests A list of recipients.
    /// @return A list of Ticket objects.
    ///
    template <unsigned long I>
    [[nodiscard]] auto bcast(std::variant_alternative_t<I, payload_t> const &payload,
                             std::vector<Envelope> const                    &dests)
    {
        std::vector<Ticket> tks(dests.size());
        std::transform(begin(dests), end(dests), begin(tks),
                       [this, &payload](Envelope const &dest) {
                           return send<I>(payload, dest);
                       });
        return tks; // NRVO
    }
    /// Broadcast a single message to multiple recipients
    /// @tparam Payload Type of the message.
    /// It must be uniquely identified in `payload_t`.
    /// @param payload A message to broadcast.
    /// @param dests A list of recipients.
    /// @return A list of Ticket objects.
    ///
    template <class Payload>
    [[nodiscard]] auto bcast(Payload const &payload, std::vector<Envelope> const &dests)
    {
        static_assert(std::is_constructible_v<payload_t, decltype(payload)>,
                      "no alternative for the given payload type");
        std::vector<Ticket> tks(dests.size());
        std::transform(begin(dests), end(dests), begin(tks),
                       [this, &payload](Envelope const &dest) {
                           return send(payload, dest);
                       });
        return tks; // NRVO
    }
    /// @}

    /// @{
    /// Gather messages from multiple recipients
    /// @tparam I Index to the **Payloads...** parameter pack.
    /// @param srcs A list of senders.
    /// @return A list of Package objects.
    ///
    template <unsigned long I> [[nodiscard]] auto gather(std::vector<Envelope> const &srcs)
    {
        std::vector<Package<std::variant_alternative_t<I, payload_t>>> pkgs;
        pkgs.reserve(srcs.size());
        for (auto const &src : srcs) {
            pkgs.emplace_back(recv<I>(src));
        }
        return pkgs; // NRVO
    }
    /// Gather messages from multiple recipients
    /// @tparam Payload Type of the message.
    /// It must be uniquely identified in `payload_t`.
    /// @param srcs A list of senders.
    /// @return A list of Package objects.
    ///
    template <class Payload> [[nodiscard]] auto gather(std::vector<Envelope> const &srcs)
    {
        std::vector<Package<Payload>> pkgs;
        pkgs.reserve(srcs.size());
        for (auto const &src : srcs) {
            pkgs.emplace_back(recv<Payload>(src));
        }
        return pkgs; // NRVO
    }
    /// @}

    /// @{
    /// Sequentially apply a reduction operation on messages
    /// @tparam I Index to the **Payloads...** parameter pack.
    /// @tparam Ret Type of the initial value, which is also the return value type.
    /// The `I`'th message type must be convertible to this type.
    /// @tparam BinaryOp A binary operator for data reduction invocable with `Ret(A&&, B&&)`.
    /// The first argument is the message and the second argument is the result of the previous
    /// reduction (or the initial value when being called the first time). The return value must be
    /// convertible to `Ret`.
    /// @param participants A list of participants to the reduction.
    /// @param init The initial value.
    /// @param op A function object for reduction.
    /// @return Result of the reduction operation.
    ///
    template <unsigned long I, class Ret, class BinaryOp>
    [[nodiscard]] auto reduce(std::vector<Envelope> const &participants, Ret init, BinaryOp &&op)
    {
        static_assert(std::is_invocable_r_v<Ret, BinaryOp,
                                            std::variant_alternative_t<I, payload_t> &&, Ret &&>);
        for (auto const &participant : participants) {
            init = recv<I>(participant).unpack(op, std::move(init));
        }
        return init;
    }
    /// Sequentially apply a reduction operation on messages
    /// @tparam Payload Type of the message.
    /// It must be uniquely identified in `payload_t`.
    /// @tparam Ret Type of the initial value, which is also the return value type.
    /// The `I`'th message type must be convertible to this type.
    /// @tparam BinaryOp A binary operator for data reduction invocable with `Ret(A&&, B&&)`.
    /// The first argument is the message and the second argument is the result of the previous
    /// reduction (or the initial value when being called the first time). The return value must be
    /// convertible to `Ret`.
    /// @param participants A list of participants to the reduction.
    /// @param init The initial value.
    /// @param op A function object for reduction.
    /// @return Result of the reduction operation.
    ///
    template <class Payload, class Ret, class BinaryOp>
    [[nodiscard]] auto reduce(std::vector<Envelope> const &participants, Ret init, BinaryOp &&op)
    {
        static_assert(std::is_invocable_r_v<Ret, BinaryOp, Payload &&, Ret &&>);
        for (auto const &participant : participants) {
            init = recv<Payload>(participant).unpack(op, std::move(init));
        }
        return init;
    }
    /// @}

    /// @{
    /// Sequentially apply a function object to each message
    /// @tparam I Index to the **Payloads...** parameter pack.
    /// @tparam Fn A function object of the signature `Ret(A&&, RestArgs...)`, with each message as
    /// its first argument. The return value is neglected.
    /// @tparam RestArgs Types of additional arguments passed to the function object.
    /// @param participants A list of participants.
    /// @param f A function object to apply.
    /// @param rest Additional arguments to be passed to `f`.
    ///
    template <unsigned long I, class Fn, class... RestArgs>
    void for_each(std::vector<Envelope> const &participants, Fn &&f, RestArgs &&...rest)
    {
        static_assert(
            std::is_invocable_v<Fn, std::variant_alternative_t<I, payload_t> &&, RestArgs &...>);
        for (auto const &participant : participants) {
            recv<I>(participant).unpack(f, rest...);
        }
    }
    /// Sequentially apply a function object to each message
    /// @tparam Payload Type of the message.
    /// It must be uniquely identified in `payload_t`.
    /// @tparam Fn A function object of the signature `Ret(A&&, RestArgs...)`, with each message as
    /// its first argument. The return value is neglected.
    /// @tparam RestArgs Types of additional arguments passed to the function object.
    /// @param participants A list of participants.
    /// @param f A function object to apply.
    /// @param rest Additional arguments to be passed to `f`.
    ///
    template <class Payload, class Fn, class... RestArgs>
    void for_each(std::vector<Envelope> const &participants, Fn &&f, RestArgs &&...rest)
    {
        static_assert(std::is_invocable_v<Fn, Payload &&, RestArgs &...>);
        for (auto const &participant : participants) {
            recv<Payload>(participant).unpack(f, rest...);
        }
    }
    /// @}

    /// Construct a Communicator object
    /// @param rank A rank of the calling thread to be associated with the Communicator object.
    /// @return A Communicator object.
    ///
    template <class Int> [[nodiscard]] Communicator comm(Int const rank) &noexcept
    {
        return { this, rank };
    }
};

/// MPI-like inter-thread communicator
///
/// This class provides the MPI-like message passing interface among multiple threads identified by
/// their ranks. The coordination of message exchange is done by a central MessageDispatch object,
/// from which Communicator objects are constructed.
///
/// @sa MessageDispatch
///
template <class... Payloads> class MessageDispatch<Payloads...>::Communicator {
    friend MessageDispatch<Payloads...>;
    Communicator(MessageDispatch<Payloads...> *dispatch, Rank const rank) noexcept
    : dispatch{ dispatch }, rank{ rank }
    {
    }
    //
    MessageDispatch<Payloads...> *dispatch;

public:
    Rank rank; //!< Rank associated with `*this`.
    //
    Communicator() noexcept = default;

public: // communication methods
    /// @{
    /// Send a message
    /// @tparam I Index to the message types with which MessageDispatch is associated.
    /// @param payload A message to send.
    /// @param to Rank of the recipient.
    /// @return A Ticket object.
    ///
    template <unsigned long I>
    [[nodiscard]] auto send(std::variant_alternative_t<I, payload_t> const &payload,
                            Rank const                                      to) const
    {
        return dispatch->template send<I>(payload, { rank, to });
    }
    template <unsigned long I>
    [[nodiscard]] auto send(std::variant_alternative_t<I, payload_t> &&payload, Rank const to) const
    {
        return dispatch->template send<I>(std::move(payload), { rank, to });
    }
    /// Send a message
    /// @tparam Payload Type of the message.
    /// It must be uniquely identified in `MessageDispatch::payload_t`.
    /// @param payload A message to send.
    /// @param to Rank of the recipient.
    /// @return A Ticket object.
    ///
    template <class Payload> [[nodiscard]] auto send(Payload &&payload, Rank const to) const
    {
        return dispatch->send(std::forward<Payload>(payload), { rank, to });
    }
    /// @}

    /// @{
    /// Receive a message
    /// @tparam I Index to the message types with which MessageDispatch is associated.
    /// @param from Rank of the sender.
    /// @return A Package object.
    ///
    template <unsigned long I> [[nodiscard]] auto recv(Rank const from) const
    {
        return dispatch->template recv<I>({ from, rank });
    }
    /// Receive a message
    /// @tparam Payload Type of the message.
    /// It must be uniquely identified in `MessageDispatch::payload_t`.
    /// @param from Rank of the sender.
    /// @return A Package object.
    ///
    template <class Payload> [[nodiscard]] auto recv(Rank const from) const
    {
        return dispatch->template recv<Payload>({ from, rank });
    }
    /// @}

    /// @{
    /// Retrieve, if any, a message
    /// @tparam I Index to the message types with which MessageDispatch is associated.
    /// @param from Rank of the sender.
    /// @return An optional encapsulating a Package object if a message has been retrieved;
    /// otherwise, returns a nil optional.
    ///
    template <unsigned long I> [[nodiscard]] auto try_recv(Rank const from) const
    {
        return dispatch->template try_recv<I>({ from, rank });
    }
    /// Retrieve, if any, a message
    /// @tparam Payload Type of the message.
    /// It must be uniquely identified in `MessageDispatch::payload_t`.
    /// @param from Rank of the sender.
    /// @return An optional encapsulating a Package object if a message has been retrieved;
    /// otherwise, returns a nil optional.
    ///
    template <class Payload> [[nodiscard]] auto try_recv(Rank const from) const
    {
        return dispatch->template try_recv<Payload>({ from, rank });
    }
    /// @}

    /// @{
    /// Scatter multiple messages to multiple recipients in one call
    /// @tparam I Index to the message types with which MessageDispatch is associated.
    /// @param payloads A list of messages to send.
    /// @param dests A list of recipients in the matching order with `payloads`.
    /// @return A list of Ticket objects.
    ///
    template <unsigned long I>
    [[nodiscard]] auto scatter(std::vector<std::variant_alternative_t<I, payload_t>> payloads,
                               std::vector<Rank> const                              &dests) const
    {
        if (payloads.size() != dests.size()) {
            throw std::invalid_argument{ __PRETTY_FUNCTION__ };
        }
        std::vector<Ticket> tks(payloads.size());
        std::transform(std::make_move_iterator(begin(payloads)),
                       std::make_move_iterator(end(payloads)), begin(dests), begin(tks),
                       [this](std::variant_alternative_t<I, payload_t> payload, Rank const &dest) {
                           return send<I>(std::move(payload), dest);
                       });
        return tks; // NRVO
    }
    /// Scatter multiple messages to multiple recipients in one call
    /// @tparam Payload Type of the message.
    /// It must be uniquely identified in `MessageDispatch::payload_t`.
    /// @param payloads A list of messages to send.
    /// @param dests A list of recipients in the matching order with `payloads`.
    /// @return A list of Ticket objects.
    ///
    template <class Payload>
    [[nodiscard]] auto scatter(std::vector<Payload> payloads, std::vector<Rank> const &dests) const
    {
        if (payloads.size() != dests.size()) {
            throw std::invalid_argument{ __PRETTY_FUNCTION__ };
        }
        std::vector<Ticket> tks(payloads.size());
        std::transform(std::make_move_iterator(begin(payloads)),
                       std::make_move_iterator(end(payloads)), begin(dests), begin(tks),
                       [this](Payload payload, Rank const &dest) {
                           return send(std::move(payload), dest);
                       });
        return tks; // NRVO
    }
    /// @}

    /// @{
    /// Broadcast a single message to multiple recipients
    /// @tparam I Index to the message types with which MessageDispatch is associated.
    /// @param payload A message to broadcast.
    /// @param dests A list of recipients.
    /// @return A list of Ticket objects.
    ///
    template <unsigned long I>
    [[nodiscard]] auto bcast(std::variant_alternative_t<I, payload_t> const &payload,
                             std::vector<Rank> const                        &dests) const
    {
        std::vector<Ticket> tks(dests.size());
        std::transform(begin(dests), end(dests), begin(tks), [this, &payload](Rank const &dest) {
            return send<I>(payload, dest);
        });
        return tks; // NRVO
    }
    /// Broadcast a single message to multiple recipients
    /// @tparam Payload Type of the message.
    /// It must be uniquely identified in `MessageDispatch::payload_t`.
    /// @param payload A message to broadcast.
    /// @param dests A list of recipients.
    /// @return A list of Ticket objects.
    ///
    template <class Payload>
    [[nodiscard]] auto bcast(Payload const &payload, std::vector<Rank> const &dests) const
    {
        std::vector<Ticket> tks(dests.size());
        std::transform(begin(dests), end(dests), begin(tks), [this, &payload](Rank const &dest) {
            return send(payload, dest);
        });
        return tks; // NRVO
    }
    /// @}

    /// @{
    /// Gather messages from multiple recipients
    /// @tparam I Index to the message types with which MessageDispatch is associated.
    /// @param srcs A list of senders.
    /// @return A list of Package objects.
    ///
    template <unsigned long I> [[nodiscard]] auto gather(std::vector<Rank> const &srcs) const
    {
        std::vector<Package<std::variant_alternative_t<I, payload_t>>> pkgs;
        pkgs.reserve(srcs.size());
        for (auto const &src : srcs) {
            pkgs.emplace_back(recv<I>(src));
        }
        return pkgs; // NRVO
    }
    /// Gather messages from multiple recipients
    /// @tparam Payload Type of the message.
    /// It must be uniquely identified in `MessageDispatch::payload_t`.
    /// @param srcs A list of senders.
    /// @return A list of Package objects.
    ///
    template <class Payload> [[nodiscard]] auto gather(std::vector<Rank> const &srcs) const
    {
        std::vector<Package<Payload>> pkgs;
        pkgs.reserve(srcs.size());
        for (auto const &src : srcs) {
            pkgs.emplace_back(recv<Payload>(src));
        }
        return pkgs; // NRVO
    }
    /// @}

    /// @{
    /// Sequentially apply a reduction operation on messages
    /// @tparam I Index to the message types with which MessageDispatch is associated.
    /// @tparam Ret Type of the initial value, which is also the return value type.
    /// The `I`'th message type must be convertible to this type.
    /// @tparam BinaryOp A binary operator for data reduction invocable with `Ret(A&&, B&&)`.
    /// The first argument is the message and the second argument is the result of the previous
    /// reduction (or the initial value when being called the first time). The return value must be
    /// convertible to `Ret`.
    /// @param participants A list of participants to the reduction.
    /// @param init The initial value.
    /// @param op A function object for reduction.
    /// @return Result of the reduction operation.
    ///
    template <unsigned long I, class Ret, class BinaryOp>
    [[nodiscard]] auto reduce(std::vector<Rank> const &participants, Ret init, BinaryOp &&op) const
    {
        static_assert(std::is_invocable_r_v<Ret, BinaryOp,
                                            std::variant_alternative_t<I, payload_t> &&, Ret &&>);
        for (auto const &participant : participants) {
            init = recv<I>(participant).unpack(op, std::move(init));
        }
        return init;
    }
    /// Sequentially apply a reduction operation on messages
    /// @tparam Payload Type of the message.
    /// It must be uniquely identified in `MessageDispatch::payload_t`.
    /// @tparam Ret Type of the initial value, which is also the return value type.
    /// The `I`'th message type must be convertible to this type.
    /// @tparam BinaryOp A binary operator for data reduction invocable with `Ret(A&&, B&&)`.
    /// The first argument is the message and the second argument is the result of the previous
    /// reduction (or the initial value when being called the first time). The return value must be
    /// convertible to `Ret`.
    /// @param participants A list of participants to the reduction.
    /// @param init The initial value.
    /// @param op A function object for reduction.
    /// @return Result of the reduction operation.
    ///
    template <class Payload, class Ret, class BinaryOp>
    [[nodiscard]] auto reduce(std::vector<Rank> const &participants, Ret init, BinaryOp &&op) const
    {
        static_assert(std::is_invocable_r_v<Ret, BinaryOp, Payload &&, Ret &&>);
        for (auto const &participant : participants) {
            init = recv<Payload>(participant).unpack(op, std::move(init));
        }
        return init;
    }
    /// @}

    /// @{
    /// Sequentially apply a function object to each message
    /// @tparam I Index to the message types with which MessageDispatch is associated.
    /// @tparam Fn A function object of the signature `Ret(A&&, RestArgs...)`, with each message as
    /// its first argument. The return value is neglected.
    /// @tparam RestArgs Types of additional arguments passed to the function object.
    /// @param participants A list of participants.
    /// @param f A function object to apply.
    /// @param rest Additional arguments to be passed to `f`.
    ///
    template <unsigned long I, class Fn, class... RestArgs>
    void for_each(std::vector<Rank> const &participants, Fn &&f, RestArgs &&...rest)
    {
        static_assert(
            std::is_invocable_v<Fn, std::variant_alternative_t<I, payload_t> &&, RestArgs &...>);
        for (auto const &participant : participants) {
            recv<I>(participant).unpack(f, rest...);
        }
    }
    /// Sequentially apply a function object to each message
    /// @tparam Payload Type of the message.
    /// It must be uniquely identified in `MessageDispatch::payload_t`.
    /// @tparam Fn A function object of the signature `Ret(A&&, RestArgs...)`, with each message as
    /// its first argument. The return value is neglected.
    /// @tparam RestArgs Types of additional arguments passed to the function object.
    /// @param participants A list of participants.
    /// @param f A function object to apply.
    /// @param rest Additional arguments to be passed to `f`.
    ///
    template <class Payload, class Fn, class... RestArgs>
    void for_each(std::vector<Rank> const &participants, Fn &&f, RestArgs &&...rest) const
    {
        static_assert(std::is_invocable_v<Fn, Payload &&, RestArgs &...>);
        for (auto const &participant : participants) {
            recv<Payload>(participant).unpack(f, rest...);
        }
    }
    /// @}
};

} // namespace _ver_variant
#endif /* DOXYGEN_SHOULD_SKIP_THIS */
PARALLELKIT_NAMESPACE_END(1)

#endif /* ParallelKit_MessageDispatch_variant_h */
