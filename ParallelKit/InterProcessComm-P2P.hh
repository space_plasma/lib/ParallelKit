/*
 * Copyright (c) 2021-2022, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#ifndef ParallelKit_InterProcessComm_P2P_hh
#define ParallelKit_InterProcessComm_P2P_hh

/// @name Peer-to-Peer Communication
/// @{

// MARK:- ISSend
//
/// @memberof Communicator
/// Immediate, synchronous send
/// @details This is an iterator-style mpi::Comm's issend
/// @tparam I Index to the Types parameter pack.
/// @param first Pointer to the first element of the message.
/// @param last Pointer to the one-past-the-last element of the message.
/// @param to Envelope of the destination process.
/// @return A mpi::Request object.
///
template <unsigned long I>
[[nodiscard]] mpi::Request issend(std::tuple_element_t<I, type_pack> const *first,
                                  std::tuple_element_t<I, type_pack> const *last, Envelope to) const
{
    return comm.issend(first, get_type<I>(), std::distance(first, last), to);
}
/// @memberof Communicator
/// Immediate, synchronous send
/// @details This is an iterator-style mpi::Comm's issend
/// @tparam T Type of the message.
/// @param first Pointer to the first element of the message.
/// @param last Pointer to the one-past-the-last element of the message.
/// @param to Envelope of the destination process.
/// @return A mpi::Request object.
///
template <class T>
[[nodiscard]] mpi::Request issend(T const *first, T const *last, Envelope to) const
{
    return comm.issend(first, this->template get_type<T>(), std::distance(first, last), to);
}

/// @memberof Communicator
/// Immediate, synchronous send of a scalar
/// @details Here, a scalar means a single element of some message type.
/// @tparam I Index to the Types parameter pack.
/// @param payload Payload to send.
/// @param to Envelope of the destination process.
/// @return A mpi::Request object.
///
template <unsigned long I>
[[nodiscard]] mpi::Request issend(std::tuple_element_t<I, type_pack> const &payload,
                                  Envelope                                  to) const
{
    return comm.issend(&payload, get_type<I>(), long{ 1 }, to);
}
/// @memberof Communicator
/// Immediate, synchronous send of a scalar
/// @details Here, a scalar means a single element of some message type.
/// @tparam T Type of the message.
/// @param payload Payload to send.
/// @param to Envelope of the destination process.
/// @return A mpi::Request object.
///
template <class T> [[nodiscard]] mpi::Request issend(T const &payload, Envelope to) const
{
    return comm.issend(&payload, this->template get_type<T>(), long{ 1 }, to);
}

// MARK:- IBSend
//
/// @memberof Communicator
/// Immediate, buffered send
/// @details The message buffer is kept in a Ticket object returned by this function, and
/// internally MPI_ISSEND is called using the message buffer.
/// It is important to keep the Ticket object until the delivery of the message in the buffer is
/// complete.
/// @tparam I Index to the message type in the Types parameter pack.
/// @param payload `std::vector` object containing messages.
/// @param to Envelope of the destination process.
/// @return A Ticket object.
///
template <unsigned long I>
[[nodiscard]] auto ibsend(std::vector<std::tuple_element_t<I, type_pack>> payload,
                          Envelope                                        to) const
{
    auto req = comm.issend(payload.data(), get_type<I>(), static_cast<long>(payload.size()), to);
    return Ticket<decltype(payload)>{ std::move(payload), std::move(req) };
}
/// @memberof Communicator
/// Immediate, buffered send
/// @details The message buffer is kept in a Ticket object returned by this function, and
/// internally MPI_ISSEND is called using the message buffer.
/// It is important to keep the Ticket object until the delivery of the message in the buffer is
/// complete.
/// @tparam T Message type.
/// @param payload `std::vector` object containing messages.
/// @param to Envelope of the destination process.
/// @return A Ticket object.
///
template <class T> [[nodiscard]] auto ibsend(std::vector<T> payload, Envelope to) const
{
    auto req = comm.issend(payload.data(), this->template get_type<T>(),
                           static_cast<long>(payload.size()), to);
    return Ticket<decltype(payload)>{ std::move(payload), std::move(req) };
}

/// @memberof Communicator
/// Immediate, buffered send of a scalar
/// @details Same as the synchronous scalar send, except that the sending message is buffered.
/// It is important to keep the Ticket object until the delivery of the message in the buffer is
/// complete.
/// @tparam I Index to the message type in the Types parameter pack.
/// @tparam Payload Payload type. It needs not be the **I**'th message type, but must be convertible
/// to it.
/// @param _payload Message to send.
/// @param to Envelope of the destination process.
/// @return A Ticket object.
///
template <unsigned long I, class Payload>
[[nodiscard]] auto ibsend(Payload &&_payload, Envelope to) const
{
    static_assert(std::is_constructible_v<std::tuple_element_t<I, type_pack>, Payload &&>,
                  "I'th type is not constructible with Payload&&");
    auto payload
        = std::make_unique<std::tuple_element_t<I, type_pack>>(std::forward<Payload>(_payload));
    auto req = issend<I>(*payload, to);
    return Ticket<decltype(payload)>{ std::move(payload), std::move(req) };
}
/// @memberof Communicator
/// Immediate, buffered send of a scalar
/// @details Same as the synchronous scalar send, except that the sending message is buffered.
/// It is important to keep the Ticket object until the delivery of the message in the buffer is
/// complete.
/// @tparam Payload Message type.
/// @param _payload Message to send.
/// @param to Envelope of the destination process.
/// @return A Ticket object.
///
template <class Payload> [[nodiscard]] auto ibsend(Payload &&_payload, Envelope to) const
{
    auto payload = std::make_unique<std::decay_t<Payload>>(std::forward<Payload>(_payload));
    auto req     = issend(*payload, to);
    return Ticket<decltype(payload)>{ std::move(payload), std::move(req) };
}

// MARK:- Blocking, Non-buffered Recv
//
/// @memberof Communicator
/// Blocking receive with the matching message type and count
/// @tparam I Index to the Types parameter pack.
/// @param [out] first Pointer to the first element of the message buffer.
/// @param [out] last Pointer to the one-past-the-last element of the message buffer.
/// @param from Envelope of the source process.
/// @return Pointer to the first element received.
///
template <unsigned long I>
auto recv(std::tuple_element_t<I, type_pack> *first, std::tuple_element_t<I, type_pack> *last,
          Envelope from) const
{
    comm.recv(first, get_type<I>(), std::distance(first, last), from);
    return first;
}
/// @memberof Communicator
/// Blocking receive with the matching message type and count
/// @tparam T Message type.
/// @param [out] first Pointer to the first element of the message buffer.
/// @param [out] last Pointer to the one-past-the-last element of the message buffer.
/// @param from Envelope of the source process.
/// @return Pointer to the first element received.
///
template <class T> auto recv(T *first, T *last, Envelope from) const
{
    comm.recv(first, this->template get_type<T>(), std::distance(first, last), from);
    return first;
}

// MARK:- Blocking, Buffered Recv
//
/// @memberof Communicator
/// Blocking receive with the matching message type but known count
/// @details It is assumed that multiple messages (including zero count) of the same type are being
/// received.
/// @tparam I Index to the Types parameter pack.
/// @param payload This argument is to distinguish from a single message receive operation; its
/// content will not used.
/// @param from Envelope of the source process.
/// @return A Package with the received message contained in `std::vector`.
///
template <unsigned long I>
[[nodiscard]] auto recv([[maybe_unused]] std::vector<std::tuple_element_t<I, type_pack>> payload,
                        Envelope                                                         from) const
{
    MPI_Message      msg;
    mpi::Type const &type  = get_type<I>();
    auto const       count = type.get_count(comm.probe(from, &msg));
    payload.resize(static_cast<unsigned long>(count));
    comm.recv(payload.data(), type, count, &msg);
    return Package<decltype(payload)>{ std::move(payload) };
}
/// @memberof Communicator
/// Blocking receive with the matching message type but known count
/// @details It is assumed that multiple messages (including zero count) of the same type are being
/// received.
/// @tparam T Message type.
/// @param payload This argument is to distinguish from a single message receive operation; its
/// content will not used.
/// @param from Envelope of the source process.
/// @return A Package with the received message contained in `std::vector`.
///
template <class T>
[[nodiscard]] auto recv([[maybe_unused]] std::vector<T> payload, Envelope from) const
{
    MPI_Message      msg;
    mpi::Type const &type  = this->template get_type<T>();
    auto const       count = type.get_count(comm.probe(from, &msg));
    payload.resize(static_cast<unsigned long>(count));
    comm.recv(payload.data(), type, count, &msg);
    return Package<decltype(payload)>{ std::move(payload) };
}

/// @memberof Communicator
/// Blocking receive of a single element with the matching message type
/// @details It is assumed that only one message of the type is being received.
/// @exception When the received message count is not equal to one.
/// @tparam I Index to the Types parameter pack.
/// @param from Envelope of the source process.
/// @return A Package object which wraps the received message of the indicated type.
///
template <unsigned long I> [[nodiscard]] auto recv(Envelope from) const
{
    MPI_Message      msg;
    mpi::Type const &type = get_type<I>();
    if (auto const count = type.get_count(comm.probe(from, &msg)); count == 1) {
        std::tuple_element_t<I, type_pack> payload;
        comm.recv(&payload, type, count, &msg);
        return Package<decltype(payload)>{ std::move(payload) };
    }
    throw std::domain_error{ __PRETTY_FUNCTION__ };
}
/// @memberof Communicator
/// Blocking receive of a single element with the matching message type
/// @details It is assumed that only one message of the type is being received.
/// @exception When the received message count is not equal to one.
/// @tparam T Message type.
/// @param from Envelope of the source process.
/// @return A Package object which wraps the received message of the indicated type.
///
template <class T> [[nodiscard]] auto recv(Envelope from) const
{
    MPI_Message      msg;
    mpi::Type const &type = this->template get_type<T>();
    if (auto const count = type.get_count(comm.probe(from, &msg)); count == 1) {
        T payload;
        comm.recv(&payload, type, count, &msg);
        return Package<decltype(payload)>{ std::move(payload) };
    }
    throw std::domain_error{ __PRETTY_FUNCTION__ };
}

// MARK:- Send and Receive
//
/// @memberof Communicator
/// Blocking send and receive.
/// @tparam I Index to the outgoing message type in the Types parameter pack.
/// @tparam J Index to the receiving message type in the Types parameter pack.
/// @param [in] send_first Pointer to the first element of the outgoing message buffer.
/// @param [in] send_last Pointer to the one-past-the-last element of the outgoing message buffer.
/// @param send_to Envelope of the destination process.
/// @param [out] recv_first Pointer to the first element of the receiving message buffer.
/// @param [out] recv_last Pointer to the one-past-the-last element of the receiving message buffer.
/// @param recv_from Envelope of the source process.
/// @return Pointer to the first element of the receiving message buffer.
///
template <unsigned long I, unsigned long J = I>
auto send_recv(std::tuple_element_t<I, type_pack> const *send_first,
               std::tuple_element_t<I, type_pack> const *send_last, Envelope send_to,
               std::tuple_element_t<J, type_pack> *recv_first,
               std::tuple_element_t<J, type_pack> *recv_last, Envelope recv_from) const
{
    comm.send_recv(send_first, get_type<I>(), std::distance(send_first, send_last), send_to,
                   recv_first, get_type<J>(), std::distance(recv_first, recv_last), recv_from);
    return recv_first;
}
/// @memberof Communicator
/// Blocking send and receive.
/// @tparam T Outgoing message type.
/// @tparam U Receiving message type.
/// @param [in] send_first Pointer to the first element of the outgoing message buffer.
/// @param [in] send_last Pointer to the one-past-the-last element of the outgoing message buffer.
/// @param send_to Envelope of the destination process.
/// @param [out] recv_first Pointer to the first element of the receiving message buffer.
/// @param [out] recv_last Pointer to the one-past-the-last element of the receiving message buffer.
/// @param recv_from Envelope of the source process.
/// @return Pointer to the first element of the receiving message buffer.
///
template <class T, class U = T>
auto send_recv(T const *send_first, T const *send_last, Envelope send_to, U *recv_first,
               U *recv_last, Envelope recv_from) const
{
    comm.send_recv(send_first, this->template get_type<T>(), std::distance(send_first, send_last),
                   send_to, recv_first, this->template get_type<U>(),
                   std::distance(recv_first, recv_last), recv_from);
    return recv_first;
}

/// @memberof Communicator
/// Send-receive-replace
/// @tparam I Index to the message type in the Types parameter pack.
/// @param [in, out] first Pointer to the first element of the message buffer.
/// @param [in, out] last Pointer to the one-past-the-last element of the message buffer.
/// @param send_to Envelope of the destination process.
/// @param recv_from Envelope of the source process.
/// @return Pointer to the first element of the message buffer.
///
template <unsigned long I>
auto send_recv(std::tuple_element_t<I, type_pack> *first, std::tuple_element_t<I, type_pack> *last,
               Envelope send_to, Envelope recv_from) const
{
    comm.send_recv(first, get_type<I>(), std::distance(first, last), send_to, recv_from);
    return first;
}
/// @memberof Communicator
/// Send-receive-replace
/// @tparam T Message type.
/// @param [in, out] first Pointer to the first element of the message buffer.
/// @param [in, out] last Pointer to the one-past-the-last element of the message buffer.
/// @param send_to Envelope of the destination process.
/// @param recv_from Envelope of the source process.
/// @return Pointer to the first element of the message buffer.
///
template <class T> auto send_recv(T *first, T *last, Envelope send_to, Envelope recv_from) const
{
    comm.send_recv(first, this->template get_type<T>(), std::distance(first, last), send_to,
                   recv_from);
    return first;
}

/// @memberof Communicator
/// Blocking message exchange.
/// @tparam I Index to the message type in the Types parameter pack.
/// @param payload Outgoing message.
/// @param send_to Envelope of the destination process.
/// @param recv_from Envelope of the source process.
/// @return A Package with the received message contained in `std::vector`.
///
template <unsigned long I>
[[nodiscard]] auto send_recv(std::vector<std::tuple_element_t<I, type_pack>> payload,
                             Envelope send_to, Envelope recv_from) const
{
    comm.send_recv(payload.data(), get_type<I>(), static_cast<long>(payload.size()), send_to,
                   recv_from);
    return Package<decltype(payload)>{ std::move(payload) };
}
/// @memberof Communicator
/// Blocking message exchange.
/// @tparam T Message type.
/// @param payload Outgoing message.
/// @param send_to Envelope of the destination process.
/// @param recv_from Envelope of the source process.
/// @return A Package with the received message contained in `std::vector`.
///
template <class T>
[[nodiscard]] auto send_recv(std::vector<T> payload, Envelope send_to, Envelope recv_from) const
{
    comm.send_recv(payload.data(), this->template get_type<T>(), static_cast<long>(payload.size()),
                   send_to, recv_from);
    return Package<decltype(payload)>{ std::move(payload) };
}

/// @memberof Communicator
/// Blocking send and receive of a single message of the type identified by I.
/// @tparam I Index to the message type in the Types parameter pack.
/// @param payload Message to send.
/// @param send_to Envelope of the destination process.
/// @param recv_from Envelope of the source process.
/// @return A Package object which wraps the received message of the indicated type.
///
template <unsigned long I>
[[nodiscard]] auto send_recv(std::tuple_element_t<I, type_pack> payload, Envelope send_to,
                             Envelope recv_from) const
{
    comm.send_recv(&payload, get_type<I>(), long{ 1 }, send_to, recv_from);
    return Package<decltype(payload)>{ std::move(payload) };
}
/// @memberof Communicator
/// Blocking send and receive of a single message of type T.
/// @tparam T Message type.
/// @param payload Message to send.
/// @param send_to Envelope of the destination process.
/// @param recv_from Envelope of the source process.
/// @return A Package object which wraps the received message of the indicated type.
///
template <class T>
[[nodiscard]] auto send_recv(T payload, Envelope send_to, Envelope recv_from) const
{
    comm.send_recv(&payload, this->template get_type<T>(), long{ 1 }, send_to, recv_from);
    return Package<decltype(payload)>{ std::move(payload) };
}
/// @}

#endif /* ParallelKit_InterProcessComm_P2P_hh */
