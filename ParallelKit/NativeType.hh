/*
 * Copyright (c) 2021, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#ifndef ParallelKit_NativeType_hh
#define ParallelKit_NativeType_hh

/// @cond
#define PARALLELKIT_STAMP_NATIVE_TYPE_CLASS_DEFINITION(type, map)                                  \
    template <> class Type::Native<type> final : public Type {                                     \
    public:                                                                                        \
        Native() noexcept : Type{ map, alignof(type), true } {}                                    \
        Native(Native &&) noexcept = default;                                                      \
        Native &operator=(Native &&) noexcept = default;                                           \
    }

PARALLELKIT_STAMP_NATIVE_TYPE_CLASS_DEFINITION(char, MPI_CHAR);
PARALLELKIT_STAMP_NATIVE_TYPE_CLASS_DEFINITION(unsigned char, MPI_UNSIGNED_CHAR);
PARALLELKIT_STAMP_NATIVE_TYPE_CLASS_DEFINITION(short, MPI_SHORT);
PARALLELKIT_STAMP_NATIVE_TYPE_CLASS_DEFINITION(unsigned short, MPI_UNSIGNED_SHORT);
PARALLELKIT_STAMP_NATIVE_TYPE_CLASS_DEFINITION(int, MPI_INT);
PARALLELKIT_STAMP_NATIVE_TYPE_CLASS_DEFINITION(unsigned int, MPI_UNSIGNED);
PARALLELKIT_STAMP_NATIVE_TYPE_CLASS_DEFINITION(long, MPI_LONG);
PARALLELKIT_STAMP_NATIVE_TYPE_CLASS_DEFINITION(unsigned long, MPI_UNSIGNED_LONG);
PARALLELKIT_STAMP_NATIVE_TYPE_CLASS_DEFINITION(float, MPI_FLOAT);
PARALLELKIT_STAMP_NATIVE_TYPE_CLASS_DEFINITION(double, MPI_DOUBLE);

#endif /* ParallelKit_NativeType_hh */
