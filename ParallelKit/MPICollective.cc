/*
 * Copyright (c) 2021, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "MPICollective.h"

#include "MPIComm.h"

#include <limits>
#include <stdexcept>
#include <string>

PARALLELKIT_NAMESPACE_BEGIN(1)

using mpi::Comm;

namespace {
constexpr int int_max = std::numeric_limits<int>::max();
//
[[nodiscard]] MPI_Comm operator*(mpi::Collective<Comm> const &comm) noexcept
{
    return static_cast<Comm const &>(comm).operator*();
}
} // namespace

// MARK:-  Synchronization
//
void mpi::Collective<Comm>::barrier() const
{
    if (MPI_Barrier(**this) != MPI_SUCCESS) {
        throw std::runtime_error{ std::string{ __PRETTY_FUNCTION__ }
                                  + " - MPI_Barrier(...) returned error" };
    }
}

void mpi::Collective<Comm>::bcast(void *buffer, Type const &type, long const count,
                                  Rank const root) const
{
    if (count > int_max) {
        throw std::invalid_argument{ std::string{ __PRETTY_FUNCTION__ }
                                     + " - number of elements greater than int::max" };
    }
    if (MPI_Bcast(buffer, static_cast<int>(count), *type, root, **this) != MPI_SUCCESS) {
        throw std::runtime_error{ std::string{ __PRETTY_FUNCTION__ }
                                  + " - MPI_Bcast(...) returned error" };
    }
}

void mpi::Collective<Comm>::gather(void const *send_data, Type const &send_type,
                                   long const send_count, void *recv_buffer, Type const &recv_type,
                                   long const recv_count, Rank const root) const
{
    if (send_count > int_max) {
        throw std::invalid_argument{ std::string{ __PRETTY_FUNCTION__ }
                                     + " - number of send elements greater than int::max" };
    }
    if (recv_count > int_max) {
        throw std::invalid_argument{ std::string{ __PRETTY_FUNCTION__ }
                                     + " - number of receive elements greater than int::max" };
    }
    if (MPI_Gather(send_data, static_cast<int>(send_count), *send_type, recv_buffer,
                   static_cast<int>(recv_count), *recv_type, root, **this)
        != MPI_SUCCESS) {
        throw std::runtime_error{ std::string{ __PRETTY_FUNCTION__ }
                                  + " - MPI_Gather(...) returned error" };
    }
}
void mpi::Collective<Comm>::gather([[maybe_unused]] decltype(nullptr) in_place, void *buffer,
                                   Type const &type, long const count) const
{
    gather(MPI_IN_PLACE, Type{}, long{}, buffer, type, count,
           static_cast<Comm const *>(this)->rank());
}

void mpi::Collective<Comm>::all_gather(void const *send_data, Type const &send_type,
                                       long const send_count, void *recv_buffer,
                                       Type const &recv_type, long const recv_count) const
{
    if (send_count > int_max) {
        throw std::invalid_argument{ std::string{ __PRETTY_FUNCTION__ }
                                     + " - number of send elements greater than int::max" };
    }
    if (recv_count > int_max) {
        throw std::invalid_argument{ std::string{ __PRETTY_FUNCTION__ }
                                     + " - number of receive elements greater than int::max" };
    }
    if (MPI_Allgather(send_data, static_cast<int>(send_count), *send_type, recv_buffer,
                      static_cast<int>(recv_count), *recv_type, **this)
        != MPI_SUCCESS) {
        throw std::runtime_error{ std::string{ __PRETTY_FUNCTION__ }
                                  + " - MPI_Allgather(...) returned error" };
    }
}

void mpi::Collective<Comm>::scatter(void const *send_data, Type const &send_type,
                                    long const send_count, void *recv_buffer, Type const &recv_type,
                                    long const recv_count, Rank const root) const
{
    if (send_count > int_max) {
        throw std::invalid_argument{ std::string{ __PRETTY_FUNCTION__ }
                                     + " - number of send elements greater than int::max" };
    }
    if (recv_count > int_max) {
        throw std::invalid_argument{ std::string{ __PRETTY_FUNCTION__ }
                                     + " - number of receive elements greater than int::max" };
    }
    if (MPI_Scatter(send_data, static_cast<int>(send_count), *send_type, recv_buffer,
                    static_cast<int>(recv_count), *recv_type, root, **this)
        != MPI_SUCCESS) {
        throw std::runtime_error{ std::string{ __PRETTY_FUNCTION__ }
                                  + " - MPI_Scatter(...) returned error" };
    }
}
void mpi::Collective<Comm>::scatter(void const *data, Type const &type, long const count,
                                    [[maybe_unused]] decltype(nullptr) in_place) const
{
    scatter(data, type, count, MPI_IN_PLACE, Type{}, long{},
            static_cast<Comm const *>(this)->rank());
}

void mpi::Collective<Comm>::all2all(void const *send_data, Type const &send_type,
                                    long const send_count, void *recv_buffer, Type const &recv_type,
                                    long const recv_count) const
{
    if (send_count > int_max) {
        throw std::invalid_argument{ std::string{ __PRETTY_FUNCTION__ }
                                     + " - number of send elements greater than int::max" };
    }
    if (recv_count > int_max) {
        throw std::invalid_argument{ std::string{ __PRETTY_FUNCTION__ }
                                     + " - number of receive elements greater than int::max" };
    }
    if (MPI_Alltoall(send_data, static_cast<int>(send_count), *send_type, recv_buffer,
                     static_cast<int>(recv_count), *recv_type, **this)
        != MPI_SUCCESS) {
        throw std::runtime_error{ std::string{ __PRETTY_FUNCTION__ }
                                  + " - MPI_Alltoall(...) returned error" };
    }
}

PARALLELKIT_NAMESPACE_END(1)
