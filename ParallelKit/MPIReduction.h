/*
 * Copyright (c) 2021-2022, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#ifndef ParallelKit_MPIReduction_h
#define ParallelKit_MPIReduction_h

#include <ParallelKit/ParallelKit-config.h>

#include <ParallelKit/MPIRank.h>
#include <ParallelKit/MPIReduceOp.h>
#include <ParallelKit/MPIType.h>
#include <ParallelKit/TypeMap.h>

#include <memory>
#include <type_traits>
#include <utility>

#include <ParallelKit/mpi_wrapper.h>

PARALLELKIT_NAMESPACE_BEGIN(1)
namespace mpi {

// forward decls
//
/// @cond
template <class> class Reduction;
class Comm;
/// @endcond

/// Wrapper for reduction operations.
/// @details Reduction operations on intracomminucators are assumed.
///
template <> class Reduction<Comm> {
public:
    // MARK:- Low-level MPI Interface Layer
    /// @name Low-level MPI Interface Layer
    /// @{

    /// Blocking reduce.
    /// @details It combines the elements provided in the input buffer of each process in the group,
    /// using the operation op, and returns the combined value in the output buffer of the process
    /// with rank root. The input buffer is defined by the arguments sendbuf, count and datatype;
    /// the output buffer is defined by the arguments recvbuf, count and datatype; both have the
    /// same number of elements, with the same type. It is called by all group members using the
    /// same arguments for count, datatype, op, root and comm. Thus, all processes provide input
    /// buffers of the same length, with elements of the same type as the output buffer at the root.
    /// Each process can provide one element, or a sequence of elements, in which case the combine
    /// operation is executed element-wise on each entry of the sequence.
    ///
    /// The operation op is always assumed to be associative.
    /// All predefined operations are also assumed to be commutative.
    /// Users may define operations that are assumed to be associative, but not commutative.
    /// The "canonical" evaluation order of a reduction is determined by the ranks of the processes
    /// in the group. However, the implementation can take advantage of associativity, or
    /// associativity and commutativity in order to change the order of evaluation. This may change
    /// the result of the reduction for operations that are not strictly associative and
    /// commutative, such as floating point addition.
    ///
    /// The datatype argument of `MPI_REDUCE` must be compatible with op.
    /// Furthermore, the datatype and op given for predefined operators must be the same on all
    /// processes. Note that it is possible for users to supply different user-defined operations to
    /// `MPI_REDUCE` in each process. MPI does not define which operations are used on which
    /// operands in this case. User-defined operators may operate on general, derived datatypes. In
    /// this case, each argument that the reduce operation is applied to is one element described by
    /// such a datatype, which may contain several basic values.
    /// @param op Reduction operator.
    /// @param [in] send_data Message to reduce.
    /// @param [out] recv_buffer Buffer for the reduced result.
    /// @param type Message type.
    /// @param count Message count.
    /// @param root Rank of the root process.
    ///
    void reduce(ReduceOp const &op, void const *send_data, void *recv_buffer, Type const &type,
                long count, Rank root) const;

    /// In-place reduce on the root process, that is, `*this`.
    /// @details The "in place" option for intracommunicators is specified by passing the value
    /// `MPI_IN_PLACE` to the argument sendbuf at the root. In such a case, the input data is taken
    /// at the root from the receive buffer, where it will be replaced by the output data.
    /// @param op Reduction operator.
    /// @param [in, out] buffer Buffer for the reduced result, as well as the data for the root
    /// process.
    /// @param type Message type.
    /// @param count Message count.
    ///
    void reduce(ReduceOp const &op, void *buffer, Type const &type, long count) const;

    /// Reduce on other processes.
    /// @param op Reduction operator.
    /// @param [in] data Message to reduce.
    /// @param type Message type.
    /// @param count Message count.
    /// @param root Rank of the root process.
    ///
    void reduce(ReduceOp const &op, void const *data, Type const &type, long const count,
                Rank const root) const
    {
        reduce(op, data, nullptr, type, count, root);
    }

    /// Blocking all-reduce.
    /// @details It behaves the same as reduce except that the result appears in the receive buffer
    /// of all the group members.
    /// @param op Reduction operator.
    /// @param [in] send_data Message to reduce.
    /// @param [out] recv_buffer Buffer for the reduced result.
    /// @param type Message type.
    /// @param count Message count.
    ///
    void all_reduce(ReduceOp const &op, void const *send_data, void *recv_buffer, Type const &type,
                    long count) const;

    /// In-place all-reduce.
    /// @details The "in place" option for intracommunicators is specified by passing the value
    /// `MPI_IN_PLACE` to the argument sendbuf at all processes. In this case, the input data is
    /// taken at each process from the receive buffer, where it will be replaced by the output data.
    /// @param op Reduction operator.
    /// @param [in, out] data Message to reduce, as well as the buffer where the result will be
    /// saved.
    /// @param type Message type.
    /// @param count Message count.
    ///
    void all_reduce(ReduceOp const &op, void *data, Type const &type, long const count) const
    {
        all_reduce(op, MPI_IN_PLACE, data, type, count);
    }
    /// @}
};

} // namespace mpi
PARALLELKIT_NAMESPACE_END(1)

#endif /* ParallelKit_MPIReduction_h */
