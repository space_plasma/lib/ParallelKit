/*
 * Copyright (c) 2021-2022, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#ifndef ParallelKit_MPICollective_h
#define ParallelKit_MPICollective_h

#include <ParallelKit/ParallelKit-config.h>

#include <ParallelKit/MPIRank.h>
#include <ParallelKit/MPIType.h>
#include <ParallelKit/TypeMap.h>

#include <memory>
#include <type_traits>
#include <utility>

#include <ParallelKit/mpi_wrapper.h>

PARALLELKIT_NAMESPACE_BEGIN(1)
namespace mpi {

// forward decls
//
/// @cond
template <class> class Collective;
class Comm;
/// @endcond

/// Wrapper for collective communication.
/// @details Collective operations on intracomminucators are assumed.
///
template <> class Collective<Comm> {
public:
    // MARK:-  Synchronization
    /// @name Synchronization
    /// @{

    /// Blocks until all processes in the communicator have reached this routine.
    ///
    void barrier() const;
    /// @}

public:
    // MARK:- Blocking Broadcast
    /// @name Blocking Broadcast
    /// @{

    /// Broadcasts a message from the process with rank root to all processes of the group, itself
    /// included.
    /// @details It is called by all members of the group using the same arguments for comm and
    /// root. On return, the content of root's buffer is copied to all other processes.
    ///
    /// General, derived datatypes are allowed for datatype.
    /// The type signature of count, datatype on any process must be equal to the type signature of
    /// count, datatype at the root. This implies that the amount of data sent must be equal to the
    /// amount received, pairwise between each process and the root. Distinct type maps between
    /// sender and receiver are still allowed.
    /// @param [in, out] buffer Message to broadcast.
    /// For the root process, the buffer contains the message to broadcast.
    /// For other processes, the message is saved to buffer.
    /// @param type Message type.
    /// @param count Message count.
    /// @param root Rank of the root process.
    ///
    void bcast(void *buffer, Type const &type, long count, Rank root) const;
    /// @}

public:
    // MARK:- Blocking Gather
    /// @name Blocking Gather
    /// @{

    /// Each process (root process included) sends the contents of its send buffer to the root
    /// process.
    /// @details The root process receives the messages and stores them in rank order.
    /// The receive buffer is ignored for all non-root processes.
    ///
    /// General, derived datatypes are allowed for both sendtype and recvtype.
    /// The type signature of sendcount, sendtype on each process must be equal to the type
    /// signature of recvcount, recvtype at the root. This implies that the amount of data sent must
    /// be equal to the amount of data received, pairwise between each process and the root.
    /// Distinct type maps between sender and receiver are still allowed.
    ///
    /// All arguments to the function are significant on process root, while on other processes,
    /// only arguments sendbuf, sendcount, sendtype, root, and comm are significant. The arguments
    /// root and comm must have identical values on all processes.
    ///
    /// The specification of counts and types should not cause any location on the root to be
    /// written more than once. Such a call is erroneous.
    ///
    /// Note that the recvcount argument at the root indicates the number of items it receives from
    /// each process, not the total number of items it receives.
    /// @param [in] send_data Outgoing message buffer.
    /// @param send_type Outgoing message type.
    /// @param send_count Outgoing message count.
    /// @param [out] recv_buffer Receiving message buffer.
    /// @param recv_type Receiving message type.
    /// @param recv_count Receiving message count.
    /// @param root Rank of the root process.
    ///
    void gather(void const *send_data, Type const &send_type, long send_count, void *recv_buffer,
                Type const &recv_type, long recv_count, Rank root) const;

    /// In-place gather on root process, that is, `*this`.
    /// @details The "in place" option for intracommunicators is specified by passing `MPI_IN_PLACE`
    /// as the value of sendbuf at the root. In such a case, sendcount and sendtype are ignored, and
    /// the contribution of the root to the gathered vector is assumed to be already in the correct
    /// place in the receive buffer.
    /// @param in_place `nullptr`; to remind that this is in-place gather on the root process.
    /// @param [out] buffer Receiving message buffer on the root process,
    /// whose size must be large enough to receive all messages from the processes involved in
    /// gathering.
    /// @param type Receiving message type.
    /// @param count Receiving message count.
    ///
    void gather([[maybe_unused]] decltype(nullptr) in_place, void *buffer, Type const &type,
                long count) const;

    /// Gather on other processes.
    /// @param [in] data Outgoing message buffer.
    /// @param type Outgoing message type.
    /// @param count Outgoing message count.
    /// @param in_place `nullptr`; to remind that this is in-place gather on other processes.
    /// @param root Rank of the root process.
    ///
    void gather(void const *data, Type const &type, long const count,
                [[maybe_unused]] decltype(nullptr) in_place, Rank const root) const
    {
        gather(data, type, count, nullptr, Type{}, long{}, root);
    }

    /// Gather-to-all.
    /// @details All processes receive the result, instead of just the root.
    /// The block of data sent from the j-th process is received by every process and placed in the
    /// j-th block of the buffer recvbuf.
    ///
    /// The type signature associated with sendcount, sendtype, at a process must be equal to the
    /// type signature associated with recvcount, recvtype at any other process.
    /// @param [in] send_data Outgoing message buffer.
    /// @param send_type Outgoing message type.
    /// @param send_count Outgoing message count.
    /// @param [out] recv_buffer Receiving message buffer.
    /// @param recv_type Receiving message type.
    /// @param recv_count Receiving message count.
    ///
    void all_gather(void const *send_data, Type const &send_type, long send_count,
                    void *recv_buffer, Type const &recv_type, long recv_count) const;

    /// In-place gather-to-all.
    /// @details The "in place" option for intracommunicators is specified by passing the value
    /// `MPI_IN_PLACE` to the argument sendbuf at all processes. sendcount and sendtype are ignored.
    /// Then the input data of each process is assumed to be in the area where that process would
    /// receive its own contribution to the receive buffer.
    /// @param in_place `nullptr`; to remind that this is in-place gather-to-all.
    /// @param [in, out] buffer Message buffer.
    /// @param type Message type.
    /// @param count Message count.
    ///
    void all_gather([[maybe_unused]] decltype(nullptr) in_place, void *buffer, Type const &type,
                    long const count) const
    {
        all_gather(MPI_IN_PLACE, Type{}, long{}, buffer, type, count);
    }
    /// @}

public:
    // MARK:- Blocking Scatter
    /// @name Blocking Scatter
    /// @{

    /// Inverse of gather operation.
    /// @details The send buffer is ignored for all non-root processes.
    ///
    /// The type signature associated with sendcount, sendtype at the root must be equal to the type
    /// signature associated with recvcount, recvtype at all processes (however, the type maps may
    /// be different). This implies that the amount of data sent must be equal to the amount of data
    /// received, pairwise between each process and the root. Distinct type maps between sender and
    /// receiver are still allowed.
    ///
    /// All arguments to the function are significant on process root, while on other processes,
    /// only arguments recvbuf, recvcount, recvtype, root, and comm are significant. The arguments
    /// root and comm must have identical values on all processes.
    ///
    /// The specification of counts and types should not cause any location on the root to be read
    /// more than once.
    /// @param [in] send_data Outgoing message buffer.
    /// @param send_type Outgoing message type.
    /// @param send_count Outgoing message count.
    /// @param [out] recv_buffer Receiving message buffer.
    /// @param recv_type Receiving message type.
    /// @param recv_count Receiving message count.
    /// @param root Rank of the root process.
    ///
    void scatter(void const *send_data, Type const &send_type, long send_count, void *recv_buffer,
                 Type const &recv_type, long recv_count, Rank root) const;

    /// In-place scatter on root process, that is, `*this`.
    /// @details The "in place" option for intracommunicators is specified by passing `MPI_IN_PLACE`
    /// as the value of recvbuf at the root. In such a case, recvcount and recvtype are ignored, and
    /// root "sends" no data to itself. The scattered vector is still assumed to contain n segments,
    /// where n is the group size; the root-th segment, which root should "send to itself," is not
    /// moved.
    /// @param [in] data Outgoing message buffer,
    /// whose size must be large enough to contain all messages to be sent to all processes involved
    /// in scattering.
    /// @param type Outgoing message type.
    /// @param count Outgoing message count.
    /// @param in_place `nullptr`; to remind that this is in-place scatter on the root process.
    ///
    void scatter(void const *data, Type const &type, long count,
                 [[maybe_unused]] decltype(nullptr) in_place) const;

    /// Scatter on other processes.
    /// @param in_place `nullptr`; to remind that this is in-place scatter on other processes.
    /// @param [out] buffer Receiving message buffer.
    /// @param type Receiving message type.
    /// @param count Receiving message count.
    /// @param root Rank of the root process.
    ///
    void scatter([[maybe_unused]] decltype(nullptr) in_place, void *buffer, Type const &type,
                 long const count, Rank const root) const
    {
        scatter(nullptr, Type{}, long{}, buffer, type, count, root);
    }

    /// All-to-all.
    /// @details `MPI_ALLTOALL` is an extension of `MPI_ALLGATHER` to the case where each process
    /// sends distinct data to each of the receivers. The *j*'th block sent from process *i* is
    /// received by process *j* and is placed in the *i*'th block of recvbuf.
    ///
    /// The type signature associated with sendcount, sendtype, at a process must be equal to the
    /// type signature associated with recvcount, recvtype at any other process. This implies that
    /// the amount of data sent must be equal to the amount of data received, pairwise between every
    /// pair of processes. As usual, however, the type maps may be different.
    ///
    /// All arguments on all processes are significant. The argument comm must have identical values
    /// on all processes.
    /// @param [in] send_data Outgoing message buffer.
    /// @param send_type Outgoing message type.
    /// @param send_count Outgoing message count.
    /// @param [out] recv_buffer Receiving message buffer.
    /// @param recv_type Receiving message type.
    /// @param recv_count Receiving message count.
    ///
    void all2all(void const *send_data, Type const &send_type, long send_count, void *recv_buffer,
                 Type const &recv_type, long recv_count) const;

    /// In-place all-to-all.
    /// @details The "in place" option for intracommunicators is specified by passing `MPI_IN_PLACE`
    /// to the argument sendbuf at all processes. In such a case, sendcount and sendtype are
    /// ignored. The data to be sent is taken from the recvbuf and replaced by the received data.
    /// Data sent and received must have the same type map as specified by recvcount and recvtype.
    /// @param in_place `nullptr`; to remind that this is in-place all-to-all.
    /// @param [in, out] buffer Message buffer.
    /// @param type Message type.
    /// @param count Message count.
    ///
    void all2all([[maybe_unused]] decltype(nullptr) in_place, void *buffer, Type const &type,
                 long const count) const
    {
        all2all(MPI_IN_PLACE, Type{}, long{}, buffer, type, count);
    }
    /// @}
};

} // namespace mpi
PARALLELKIT_NAMESPACE_END(1)

#endif /* ParallelKit_MPICollective_h */
