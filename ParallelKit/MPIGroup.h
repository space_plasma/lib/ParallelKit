/*
 * Copyright (c) 2021-2022, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#ifndef ParallelKit_MPIGroup_h
#define ParallelKit_MPIGroup_h

#include <ParallelKit/ParallelKit-config.h>

#include <ParallelKit/MPIComparison.h>
#include <ParallelKit/MPIRank.h>

#include <utility>
#include <vector>

#include <ParallelKit/mpi_wrapper.h>

PARALLELKIT_NAMESPACE_BEGIN(1)
namespace mpi {

// forward decl
//
/// @cond
class BaseComm;
/// @endcond

/// Wrapper for `MPI_Group`.
/// @details A group is an ordered set of process identifiers (henceforth processes).
/// Each process in a group is associated with an integer rank.
/// Ranks are contiguous and start from zero.
///
/// A group is used within a communicator to describe the participants in a communication "universe"
/// and to rank such participants.
///
/// There is a special pre-defined group: `MPI_GROUP_EMPTY`, which is a group with no members.
/// The predefined constant `MPI_GROUP_NULL` is the value used for invalid group handles.
///
class Group {
    friend BaseComm;

    MPI_Group m_group{ MPI_GROUP_NULL };
    explicit Group(MPI_Group g) noexcept : m_group{ g } {}

public:
    ~Group();

    /// @{
    Group(Group &&o) noexcept { std::swap(m_group, o.m_group); }
    Group &operator=(Group &&o) noexcept
    {
        std::swap(m_group, o.m_group);
        return *this;
    }
    /// @}

    /// Create an empty group.
    ///
    Group() noexcept : m_group{ MPI_GROUP_EMPTY } {}

public:
    // MARK:- Query
    /// @name Query
    /// @{

    /// Returns `true` if `*this` is valid, i.e., not `MPI_GROUP_NULL`.
    ///
    explicit operator bool() const noexcept { return m_group != MPI_GROUP_NULL; }

    /// Returns the associated `MPI_Group` handle.
    ///
    [[nodiscard]] auto operator*() const noexcept { return m_group; }

    /// Determines the number of processes in the group associated with `*this`.
    ///
    [[nodiscard]] int size() const;

    /// Determines the rank of the calling process in the group associated with `*this`.
    /// @return A Rank object associated with `*this`, or
    /// an undefined Rank object (which returns `false` on boolean cast) if the calling process is
    /// not a member of `*this`.
    ///
    [[nodiscard]] Rank rank() const;

    /// Determines the relative numbering of the same process in two different groups.
    /// @details `Rank::null()` is a valid rank argument for which the translated rank is also
    /// `Rank::null()`.
    /// @param rank The rank has to be non-netative and less than `this->size()`, or `Rank::null()`.
    /// @return Rank translated to the given group.
    ///
    [[nodiscard]] Rank translate(Rank rank, Group const &to) const;
    [[nodiscard]] Rank translate(Rank rank, BaseComm const &to) const;

    /// Compare two groups.
    /// @return
    /// - Comparison::identical results if the group members and group order is exactly the same in
    /// both groups. This happens for instance if `group1` and `group2` are the same handle.
    /// - Comparison::similar results if the group members are the same but the order is different.
    /// - Comparison::unequal results otherwise.
    ///
    [[nodiscard]] Comparison compare(Group const &other) const;
    [[nodiscard]] Comparison compare(BaseComm const &other) const;
    /// @}

public:
    // MARK:- Derived Group
    /// @name Derived Group
    /// @{

    // Note on set-like operations:
    // Note that for these operations the order of processes in the output group is determined
    // primarily by order in the first group (if possible) and then, if necessary, by order in the
    // second group. Neither union nor intersection are commutative, but both are associative. The
    // new group can be empty, that is, equal to MPI_GROUP_EMPTY.
    //
    /// Construct a new group which is union of `*this` and `other`.
    /// @details All elements of the first group (`*this`), followed by all elements of second group
    /// (`other`) not in the first group.
    ///
    /// Neither union nor intersection are commutative, but both are associative.
    ///
    [[nodiscard]] Group operator|(Group const &other) const;

    /// Construct a new group which is intersection of `*this` and `other`.
    /// @details All elements of the first group (`*this`) that are also in the second group
    /// (`other`), ordered as in the first group.
    ///
    /// Neither union nor intersection are commutative, but both are associative.
    ///
    [[nodiscard]] Group operator&(Group const &other) const;

    /// Construct a new group which is difference of `*this` from `other`.
    /// @details All elements of the first group (`*this`) that are not in the second group
    /// (`other`), ordered as in the first group.
    ///
    [[nodiscard]] Group operator-(Group const &other) const;

    /// Construct a new group that consists of *N* processes in `*this` with ranks.
    /// @details The process with rank *i* in new group is the process with rank `ranks[i]` in
    /// `*this`.
    ///
    /// Each of the *N* elements of ranks must be a valid rank in `*this` and all elements must be
    /// distinct, or else the program is erroneous.
    ///
    /// If *N = 0*, then the new group is `MPI_GROUP_EMPTY`.
    ///
    /// This function can, for instance, be used to reorder the elements of a group.
    ///
    [[nodiscard]] Group included(std::vector<Rank> const &ranks) const
    {
        return included(ranks.size(), ranks.data());
    }

    /// Construct a new group of processes that is obtained by deleting from `*this` those processes
    /// with ranks.
    /// @details The ordering of processes in the new group is identical to the ordering in `*this`.
    ///
    /// Each of the *N* elements of ranks must be a valid rank in `*this` and all elements must be
    /// distinct; otherwise, the program is erroneous.
    ///
    /// If *N = 0*, then the new group is identical to `*this`.
    ///
    [[nodiscard]] Group excluded(std::vector<Rank> const &ranks) const
    {
        return excluded(ranks.size(), ranks.data());
    }
    /// @}

private:
    // first translate(rank_first, rank_last, to)

    [[nodiscard]] Group included(unsigned long size, Rank const *ranks) const;
    [[nodiscard]] Group excluded(unsigned long size, Rank const *ranks) const;
};

} // namespace mpi
PARALLELKIT_NAMESPACE_END(1)

#endif /* ParallelKit_MPIGroup_h */
