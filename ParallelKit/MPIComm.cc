/*
 * Copyright (c) 2021, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "MPIComm.h"

#include <stdexcept>
#include <string>

PARALLELKIT_NAMESPACE_BEGIN(1)

auto mpi::Comm::split(int const color, int const key) const -> Comm
{
    MPI_Comm c;
    if (MPI_Comm_split(**this, color, key, &c) != MPI_SUCCESS) {
        throw std::runtime_error{ std::string{ __PRETTY_FUNCTION__ }
                                  + " - MPI_Comm_split(...) returned error" };
    }
    return Comm{ c }; // custom error handler is set on construction
}

auto mpi::Comm::duplicated() const -> Comm
{
    MPI_Comm c;
    if (MPI_Comm_dup(**this, &c) != MPI_SUCCESS) {
        throw std::runtime_error{ std::string{ __PRETTY_FUNCTION__ }
                                  + " - MPI_Comm_dup(...) returned error" };
    }
    return Comm{ c }; // custom error handler is set on construction
}

auto mpi::Comm::created(Group const &g) const -> Comm
{
    MPI_Comm c;
    if (MPI_Comm_create(**this, *g, &c) != MPI_SUCCESS) {
        throw std::runtime_error{ std::string{ __PRETTY_FUNCTION__ }
                                  + " - MPI_Comm_create(...) returned error" };
    }
    return Comm{ c }; // custom error handler is set on construction
}

PARALLELKIT_NAMESPACE_END(1)
