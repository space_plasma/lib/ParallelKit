/*
 * Copyright (c) 2021-2022, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#ifndef ParallelKit_InterProcessComm_Reduction_hh
#define ParallelKit_InterProcessComm_Reduction_hh

/// @name Reduction
/// @{

/// @memberof Communicator
/// Blocking data reduction operation.
/// @details It combines the elements provided in the input buffer of each process in the group,
/// using the operation op, and returns the combined value in the output buffer of the process with
/// rank root. The input buffer is defined by the arguments sendbuf, count and datatype; the output
/// buffer is defined by the arguments recvbuf, count and datatype; both have the same number of
/// elements, with the same type. It is called by all group members using the same arguments for
/// count, datatype, op, root and comm. Thus, all processes provide input buffers of the same
/// length, with elements of the same type as the output buffer at the root. Each process can
/// provide one element, or a sequence of elements, in which case the combine operation is executed
/// element-wise on each entry of the sequence.
///
/// The operation op is always assumed to be associative.
/// All predefined operations are also assumed to be commutative.
/// Users may define operations that are assumed to be associative, but not commutative.
/// The "canonical" evaluation order of a reduction is determined by the ranks of the processes in
/// the group. However, the implementation can take advantage of associativity, or associativity and
/// commutativity in order to change the order of evaluation. This may change the result of the
/// reduction for operations that are not strictly associative and commutative, such as floating
/// point addition.
///
/// The datatype argument of `MPI_REDUCE` must be compatible with op.
/// Furthermore, the datatype and op given for predefined operators must be the same on all
/// processes. Note that it is possible for users to supply different user-defined operations to
/// `MPI_REDUCE` in each process. MPI does not define which operations are used on which operands in
/// this case. User-defined operators may operate on general, derived datatypes. In this case, each
/// argument that the reduce operation is applied to is one element described by such a datatype,
/// which may contain several basic values.
/// @see mpi::Collective::reduce.
/// @tparam I Index to the Types parameter pack.
/// @param op mpi::ReduceOp object.
/// @param [in] send_first Pointer to the first element of the outgoing message buffer.
/// @param [in] send_last Pointer to the one-past-the-last element of the outgoing message buffer.
/// @param [out] recv_first Pointer to the first element of the incomming message buffer.
/// @param root Rank of the root process.
/// @return Pointer to the first element of the incomming message buffer.
///
template <unsigned long I>
auto reduce(mpi::ReduceOp const &op, std::tuple_element_t<I, type_pack> const *send_first,
            std::tuple_element_t<I, type_pack> const *send_last,
            std::tuple_element_t<I, type_pack> *recv_first, Rank root) const
{
    comm.reduce(op, send_first, recv_first, get_type<I>(), std::distance(send_first, send_last),
                root);
    return recv_first;
}
template <unsigned long I, class Op,
          std::enable_if_t<!std::is_base_of_v<mpi::ReduceOp, Op>, int> = 0>
auto reduce(Op const &op, std::tuple_element_t<I, type_pack> const *send_first,
            std::tuple_element_t<I, type_pack> const *send_last,
            std::tuple_element_t<I, type_pack> *recv_first, Rank root) const
{
    constexpr auto commutative = true;
    return reduce<I>(mpi::ReduceOp::make<std::tuple_element_t<I, type_pack>>(op, commutative),
                     send_first, send_last, recv_first, root);
}
/// @memberof Communicator
/// Blocking data reduction operation.
/// @details It combines the elements provided in the input buffer of each process in the group,
/// using the operation op, and returns the combined value in the output buffer of the process with
/// rank root. The input buffer is defined by the arguments sendbuf, count and datatype; the output
/// buffer is defined by the arguments recvbuf, count and datatype; both have the same number of
/// elements, with the same type. It is called by all group members using the same arguments for
/// count, datatype, op, root and comm. Thus, all processes provide input buffers of the same
/// length, with elements of the same type as the output buffer at the root. Each process can
/// provide one element, or a sequence of elements, in which case the combine operation is executed
/// element-wise on each entry of the sequence.
///
/// The operation op is always assumed to be associative.
/// All predefined operations are also assumed to be commutative.
/// Users may define operations that are assumed to be associative, but not commutative.
/// The "canonical" evaluation order of a reduction is determined by the ranks of the processes in
/// the group. However, the implementation can take advantage of associativity, or associativity and
/// commutativity in order to change the order of evaluation. This may change the result of the
/// reduction for operations that are not strictly associative and commutative, such as floating
/// point addition.
///
/// The datatype argument of `MPI_REDUCE` must be compatible with op.
/// Furthermore, the datatype and op given for predefined operators must be the same on all
/// processes. Note that it is possible for users to supply different user-defined operations to
/// `MPI_REDUCE` in each process. MPI does not define which operations are used on which operands in
/// this case. User-defined operators may operate on general, derived datatypes. In this case, each
/// argument that the reduce operation is applied to is one element described by such a datatype,
/// which may contain several basic values.
/// @see mpi::Collective::reduce.
/// @tparam T Message type.
/// @param op mpi::ReduceOp object.
/// @param [in] send_first Pointer to the first element of the outgoing message buffer.
/// @param [in] send_last Pointer to the one-past-the-last element of the outgoing message buffer.
/// @param [out] recv_first Pointer to the first element of the incomming message buffer.
/// @param root Rank of the root process.
/// @return Pointer to the first element of the incomming message buffer.
///
template <class T>
auto reduce(mpi::ReduceOp const &op, T const *send_first, T const *send_last, T *recv_first,
            Rank root) const
{
    comm.reduce(op, send_first, recv_first, this->template get_type<T>(),
                std::distance(send_first, send_last), root);
    return recv_first;
}
template <class Op, class T, std::enable_if_t<!std::is_base_of_v<mpi::ReduceOp, Op>, int> = 0>
auto reduce(Op const &op, T const *send_first, T const *send_last, T *recv_first, Rank root) const
{
    constexpr auto commutative = true;
    return reduce(mpi::ReduceOp::make<T>(op, commutative), send_first, send_last, recv_first, root);
}

/// @memberof Communicator
/// Blocking data reduction operation.
/// @details In this version, data to reduce is passed in a `std::vector` container, and
/// the reduced results on the root process are returned in a Package object.
/// On other processes, an empty Package is returned.
/// @tparam I Index to the Types parameter pack.
/// @param op mpi::ReduceOp object.
/// @param payload Data to reduce wrapped in a `std::vector` container.
/// @param root Rank of the root process.
/// @return A Package object wrapping the reduced results.
///
template <unsigned long I>
[[nodiscard]] auto reduce(mpi::ReduceOp const                            &op,
                          std::vector<std::tuple_element_t<I, type_pack>> payload, Rank root) const
{
    if (this->rank() == root) {
        comm.reduce(op, payload.data(), get_type<I>(), static_cast<long>(payload.size()));
        return Package<decltype(payload)>{ std::move(payload) };
    } else {
        comm.reduce(op, payload.data(), get_type<I>(), static_cast<long>(payload.size()), root);
        return Package<decltype(payload)>{ std::move(payload) };
    }
}
template <unsigned long I, class Op,
          std::enable_if_t<!std::is_base_of_v<mpi::ReduceOp, Op>, int> = 0>
[[nodiscard]] auto reduce(Op const &op, std::vector<std::tuple_element_t<I, type_pack>> payload,
                          Rank root) const
{
    constexpr auto commutative = true;
    return reduce(mpi::ReduceOp::make<std::tuple_element_t<I, type_pack>>(op, commutative), payload,
                  root);
}
/// @memberof Communicator
/// Blocking data reduction operation.
/// @details In this version, data to reduce is passed in a `std::vector` container, and
/// the reduced results on the root process are returned in a Package object.
/// On other processes, an empty Package is returned.
/// @tparam T Type of data to be operated on.
/// @param op mpi::ReduceOp object.
/// @param payload Data to reduce wrapped in a `std::vector` container.
/// @param root Rank of the root process.
/// @return A Package object wrapping the reduced results.
///
template <class T>
[[nodiscard]] auto reduce(mpi::ReduceOp const &op, std::vector<T> payload, Rank root) const
{
    if (this->rank() == root) {
        comm.reduce(op, payload.data(), this->template get_type<T>(),
                    static_cast<long>(payload.size()));
        return Package<decltype(payload)>{ std::move(payload) };
    } else {
        comm.reduce(op, payload.data(), this->template get_type<T>(),
                    static_cast<long>(payload.size()), root);
        return Package<decltype(payload)>{ std::move(payload) };
    }
}
template <class Op, class T, std::enable_if_t<!std::is_base_of_v<mpi::ReduceOp, Op>, int> = 0>
[[nodiscard]] auto reduce(Op const &op, std::vector<T> payload, Rank root) const
{
    constexpr auto commutative = true;
    return reduce(mpi::ReduceOp::make<T>(op, commutative), payload, root);
}

/// @memberof Communicator
/// Same as Communicator::reduce, except that the results are distributed to all communication group
/// members.
/// @see mpi::Collective::all_reduce.
/// @tparam I Index to the Types parameter pack.
/// @param op Reduction operator.
/// @param [in] send_first Pointer to the first element of the outgoing message buffer.
/// @param [in] send_last Pointer to the one-past-the-last element of the outgoing message buffer.
/// @param [out] recv_first Pointer to the first element of the incomming message buffer.
/// @return Pointer to the first element of the incomming message buffer.
///
template <unsigned long I>
auto all_reduce(mpi::ReduceOp const &op, std::tuple_element_t<I, type_pack> const *send_first,
                std::tuple_element_t<I, type_pack> const *send_last,
                std::tuple_element_t<I, type_pack>       *recv_first) const
{
    comm.all_reduce(op, send_first, recv_first, get_type<I>(),
                    std::distance(send_first, send_last));
    return recv_first;
}
template <unsigned long I, class Op,
          std::enable_if_t<!std::is_base_of_v<mpi::ReduceOp, Op>, int> = 0>
auto all_reduce(Op const &op, std::tuple_element_t<I, type_pack> const *send_first,
                std::tuple_element_t<I, type_pack> const *send_last,
                std::tuple_element_t<I, type_pack>       *recv_first) const
{
    constexpr auto commutative = true;
    return all_reduce(mpi::ReduceOp::make<std::tuple_element_t<I, type_pack>>(op, commutative),
                      send_first, send_last, recv_first);
}
/// @memberof Communicator
/// Same as Communicator::reduce, except that the results are distributed to all communication group
/// members.
/// @see mpi::Collective::all_reduce.
/// @tparam T Type of data to be operated on.
/// @param op Reduction operator.
/// @param [in] send_first Pointer to the first element of the outgoing message buffer.
/// @param [in] send_last Pointer to the one-past-the-last element of the outgoing message buffer.
/// @param [out] recv_first Pointer to the first element of the incomming message buffer.
/// @return Pointer to the first element of the incomming message buffer.
///
template <class T>
auto all_reduce(mpi::ReduceOp const &op, T const *send_first, T const *send_last,
                T *recv_first) const
{
    comm.all_reduce(op, send_first, recv_first, this->template get_type<T>(),
                    std::distance(send_first, send_last));
    return recv_first;
}
template <class Op, class T, std::enable_if_t<!std::is_base_of_v<mpi::ReduceOp, Op>, int> = 0>
auto all_reduce(Op const &op, T const *send_first, T const *send_last, T *recv_first) const
{
    constexpr auto commutative = true;
    return all_reduce(mpi::ReduceOp::make<T>(op, commutative), send_first, send_last, recv_first);
}

/// @memberof Communicator
/// In-place all-reduce.
/// @see mpi::Collective::all_reduce.
/// @tparam I Index to the Types parameter pack.
/// @param op mpi::ReduceOp object.
/// @param [in, out] first Pointer to the first element of the message buffer.
/// @param [in, out] last Pointer to the one-past-the-last element of the message buffer.
/// @return Pointer to the first element of the message buffer.
///
template <unsigned long I>
auto all_reduce(mpi::ReduceOp const &op, std::tuple_element_t<I, type_pack> *first,
                std::tuple_element_t<I, type_pack> *last) const
{
    comm.all_reduce(op, first, get_type<I>(), std::distance(first, last));
    return first;
}
template <unsigned long I, class Op,
          std::enable_if_t<!std::is_base_of_v<mpi::ReduceOp, Op>, int> = 0>
auto all_reduce(Op const &op, std::tuple_element_t<I, type_pack> *first,
                std::tuple_element_t<I, type_pack> *last) const
{
    constexpr auto commutative = true;
    return all_reduce(mpi::ReduceOp::make<std::tuple_element_t<I, type_pack>>(op, commutative),
                      first, last);
}
/// @memberof Communicator
/// In-place all-reduce.
/// @see mpi::Collective::all_reduce.
/// @tparam T Message type.
/// @param op mpi::ReduceOp object.
/// @param [in, out] first Pointer to the first element of the message buffer.
/// @param [in, out] last Pointer to the one-past-the-last element of the message buffer.
/// @return Pointer to the first element of the message buffer.
///
template <class T> auto all_reduce(mpi::ReduceOp const &op, T *first, T *last) const
{
    comm.all_reduce(op, first, this->template get_type<T>(), std::distance(first, last));
    return first;
}
template <class Op, class T, std::enable_if_t<!std::is_base_of_v<mpi::ReduceOp, Op>, int> = 0>
auto all_reduce(Op const &op, T *first, T *last) const
{
    constexpr auto commutative = true;
    return all_reduce(mpi::ReduceOp::make<T>(op, commutative), first, last);
}

/// @memberof Communicator
/// Same as Communicator::reduce, except that the results are distributed to all communication group
/// members.
/// @details In this version, data to be reduced are passed in a `std::vector` container, and
/// the results are returned in a Package object.
/// @tparam I Index to the Types parameter pack.
/// @param op Reduction operator.
/// @param payload Data to be reduced contained in a `std::vector` object.
/// @return A Package object wrapping the reduced results.
///
template <unsigned long I>
[[nodiscard]] auto all_reduce(mpi::ReduceOp const                            &op,
                              std::vector<std::tuple_element_t<I, type_pack>> payload) const
{
    comm.all_reduce(op, payload.data(), get_type<I>(), static_cast<long>(payload.size()));
    return Package<decltype(payload)>{ std::move(payload) };
}
template <unsigned long I, class Op,
          std::enable_if_t<!std::is_base_of_v<mpi::ReduceOp, Op>, int> = 0>
[[nodiscard]] auto all_reduce(Op const                                       &op,
                              std::vector<std::tuple_element_t<I, type_pack>> payload) const
{
    constexpr auto commutative = true;
    return all_reduce(mpi::ReduceOp::make<std::tuple_element_t<I, type_pack>>(op, commutative),
                      payload);
}
/// @memberof Communicator
/// Same as Communicator::reduce, except that the results are distributed to all communication group
/// members.
/// @details In this version, data to be reduced are passed in a `std::vector` container, and
/// the results are returned in a Package object.
/// @tparam T Message type.
/// @param op Reduction operator.
/// @param payload Data to be reduced contained in a `std::vector` object.
/// @return A Package object wrapping the reduced results.
///
template <class T>
[[nodiscard]] auto all_reduce(mpi::ReduceOp const &op, std::vector<T> payload) const
{
    comm.all_reduce(op, payload.data(), this->template get_type<T>(),
                    static_cast<long>(payload.size()));
    return Package<decltype(payload)>{ std::move(payload) };
}
template <class Op, class T, std::enable_if_t<!std::is_base_of_v<mpi::ReduceOp, Op>, int> = 0>
[[nodiscard]] auto all_reduce(Op const &op, std::vector<T> payload) const
{
    constexpr auto commutative = true;
    return all_reduce(mpi::ReduceOp::make<T>(op, commutative), payload);
}

/// @memberof Communicator
/// All reduction on a scalar
/// @tparam I Index to the Types parameter pack.
/// @param op Reduction operator.
/// @param payload A scalar to work on.
/// @return A Package object wrapping the reduced result.
///
template <unsigned long I>
[[nodiscard]] auto all_reduce(mpi::ReduceOp const               &op,
                              std::tuple_element_t<I, type_pack> payload) const
{
    comm.all_reduce(op, &payload, get_type<I>(), long{ 1 });
    return Package<decltype(payload)>{ std::move(payload) };
}
template <unsigned long I, class Op,
          std::enable_if_t<!std::is_base_of_v<mpi::ReduceOp, Op>, int> = 0>
[[nodiscard]] auto all_reduce(Op const &op, std::tuple_element_t<I, type_pack> payload) const
{
    constexpr auto commutative = true;
    return all_reduce(mpi::ReduceOp::make<std::tuple_element_t<I, type_pack>>(op, commutative),
                      payload);
}
/// @memberof Communicator
/// All reduction on a scalar
/// @tparam T Message type.
/// @param op Reduction operator.
/// @param payload A scalar to work on.
/// @return A Package object wrapping the reduced result.
///
template <class T> [[nodiscard]] auto all_reduce(mpi::ReduceOp const &op, T payload) const
{
    comm.all_reduce(op, &payload, this->template get_type<T>(), long{ 1 });
    return Package<decltype(payload)>{ std::move(payload) };
}
template <class Op, class T, std::enable_if_t<!std::is_base_of_v<mpi::ReduceOp, Op>, int> = 0>
[[nodiscard]] auto all_reduce(Op const &op, T payload) const
{
    constexpr auto commutative = true;
    return all_reduce(mpi::ReduceOp::make<T>(op, commutative), payload);
}
/// @}

#endif /* ParallelKit_InterProcessComm_Reduction_hh */
