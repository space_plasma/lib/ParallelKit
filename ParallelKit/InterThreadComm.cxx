/*
 * Copyright (c) 2020, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "InterThreadComm.h"

namespace PAR = PARALLELKIT_NAMESPACE(1);

namespace {
#ifdef DEBUG
[[maybe_unused]] long test_1()
{
    PAR::_ver_tuple::MessageDispatch<int> d;
    auto                                  comm = d.comm(1);
    return comm.rank;
}
[[maybe_unused]] long test_2()
{
    PAR::_ver_variant::MessageDispatch<int> d;
    auto                                    comm = d.comm(1);
    return comm.rank;
}
#endif
} // namespace
