/*
 * Copyright (c) 2019, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#ifndef ParallelKit_power_of_2_h
#define ParallelKit_power_of_2_h

#include <ParallelKit/ParallelKit-config.h>

#include <limits>
#include <stdexcept>
#include <type_traits>

PARALLELKIT_NAMESPACE_BEGIN(1)
#ifndef DOXYGEN_SHOULD_SKIP_THIS
namespace {

/// Check if the given integer is a power of 2
/// @exception `std::invalid_argument` if the value is negative.
///
template <class T>
[[nodiscard]] constexpr bool is_power_of_2(T const i) noexcept(std::is_unsigned_v<T>)
{
    static_assert(std::is_integral_v<T>, "argument type is not an integral type");
    //
    constexpr auto predicate = [](auto const i) noexcept {
        return (i > 0) & !(i & (i - 1));
    };
    //
    if constexpr (std::is_unsigned_v<T>) {
        return predicate(i);
    } else {
        return i >= 0 ? predicate(i) : throw std::invalid_argument{ __PRETTY_FUNCTION__ };
    }
}

/// Get the next power of 2
/// @exception `std::invalid_argument` if the value is negative.
///
template <class T>
[[nodiscard]] constexpr T next_power_of_2(T const i) noexcept(std::is_unsigned_v<T>)
{
    static_assert(std::is_integral_v<T>, "argument type is not an integral type");
    //
    constexpr auto next_2 = [](auto v) noexcept {
        if (v <= T{ 2 }) {
            return T{ 2 };
        } else {
            --v;
            for (long i = 1; i < std::numeric_limits<T>::digits; i *= 2) {
                v |= v >> i;
            }
            return ++v;
        }
    };
    //
    if constexpr (std::is_unsigned_v<T>) {
        return next_2(i);
    } else {
        return i >= 0 ? next_2(i) : throw std::invalid_argument{ __PRETTY_FUNCTION__ };
    }
}

} // anonymous namespace
#endif /* DOXYGEN_SHOULD_SKIP_THIS */
PARALLELKIT_NAMESPACE_END(1)

#endif /* ParallelKit_power_of_2_h */
