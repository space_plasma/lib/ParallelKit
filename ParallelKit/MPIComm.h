/*
 * Copyright (c) 2021, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#ifndef ParallelKit_MPIComm_h
#define ParallelKit_MPIComm_h

#include <ParallelKit/ParallelKit-config.h>

#include <ParallelKit/MPIBaseComm.h>
#include <ParallelKit/MPICollective.h>
#include <ParallelKit/MPIP2P.h>
#include <ParallelKit/MPIReduction.h>

#include <type_traits>

PARALLELKIT_NAMESPACE_BEGIN(1)
namespace mpi {

/// Wrapper for `MPI_Comm`.
/// @details Only intracommunicator is supported.
///
class Comm
: public BaseComm
, public P2P<Comm>
, public Collective<Comm>
, public Reduction<Comm> {
    using BaseComm::BaseComm;

public:
    explicit Comm() noexcept = default;

    /// @{
    Comm(Comm &&) noexcept = default;
    Comm &operator=(Comm &&o) noexcept = default;
    /// @}

public:
    // MARK:- Predefined Comm
    /// @name Predefined Comm
    /// @{

    /// Returns an instance wrapping `MPI_COMM_WORLD`.
    /// @details No custom error handler is set.
    ///
    [[nodiscard]] static Comm world() noexcept { return Comm{ MPI_COMM_WORLD, nullptr }; }

    /// Returns an instance wrapping `MPI_COMM_SELF`.
    /// @details No custom error handler is set.
    ///
    [[nodiscard]] static Comm self() noexcept { return Comm{ MPI_COMM_SELF, nullptr }; }
    /// @}

public:
    // MARK:- Derived Comm
    /// @name Derived Comm
    /// @{

    /// Returns an instance by partitioning the group associated with `*this` into disjoint
    /// subgroups, one for each value of color.
    /// @details Each subgroup contains all processes of the same color.
    /// Within each subgroup, the processes are ranked in the order defined by the value of the
    /// argument key, with ties broken according to their rank in the old group.
    ///
    /// A new communicator is created for each subgroup.
    ///
    /// A process may supply the color value `MPI_UNDEFINED`, in which case newcomm returns
    /// `MPI_COMM_NULL`.
    ///
    /// This is a collective call, but each process is permitted to provide different values for
    /// color and key.
    /// @param color Non-negative integer or `MPI_UNDEFINED`.
    /// @param key Process ordering hint.
    ///
    [[nodiscard]] Comm split(int color, int key) const;
    [[nodiscard]] Comm split(int const color) const { return split(color, this->rank()); }

    /// Returns an instance by duplicating `*this`.
    ///
    [[nodiscard]] Comm duplicated() const;

    /// Construct a new communicator with communication group defined by the group argument.
    /// @details Each process must call this with a group argument that is a subgroup of the group
    /// associated with **comm**; this could be `MPI_GROUP_EMPTY`. The processes may specify
    /// different values for the group argument.
    ///
    /// If a process calls with a non-empty group then all processes in that group must call the
    /// function with the same group as argument, that is the same processes in the same order.
    /// Otherwise, the call is erroneous.
    /// This implies that the set of groups specified across the processes must be disjoint.
    /// If the calling process is a member of the group given as group argument, then the instance
    /// is a communicator with group as its associated group.
    ///
    /// In the case that a process calls with a group to which it does not belong, e.g.,
    /// `MPI_GROUP_EMPTY`, then an instance with `MPI_COMM_NULL` is returned as newcomm. This call
    /// is collective and must be called by all processes in the group of comm.
    ///
    [[nodiscard]] Comm created(Group const &g) const;
    /// @}
};

} // namespace mpi
PARALLELKIT_NAMESPACE_END(1)

#endif /* ParallelKit_MPIComm_h */
