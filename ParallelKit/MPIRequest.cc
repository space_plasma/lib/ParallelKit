/*
 * Copyright (c) 2021, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "MPIRequest.h"

#include "println.h"

#include <iostream>
#include <stdexcept>
#include <string>

PARALLELKIT_NAMESPACE_BEGIN(1)

mpi::Request::~Request()
{
    if (MPI_REQUEST_NULL != m_request && MPI_Request_free(&m_request) != MPI_SUCCESS) {
        println(std::cerr, "%% ", __PRETTY_FUNCTION__, " - MPI_Request_free(...) returned error");
#if defined(PARALLELKIT_SHOULD_TERMINATE_ON_FAILURE_IN_DESTRUCTOR)                                 \
    && PARALLELKIT_SHOULD_TERMINATE_ON_FAILURE_IN_DESTRUCTOR
        std::terminate();
#endif
    }
}

void mpi::Request::wait() &&
{
    // One is allowed to call MPI_WAIT with a null or inactive request argument.
    if (MPI_Wait(&m_request, MPI_STATUS_IGNORE) != MPI_SUCCESS) {
        throw std::runtime_error{ std::string{ __PRETTY_FUNCTION__ }
                                  + " - MPI_Wait(...) returned error" };
    }
}

bool mpi::Request::test() &
{
    int flag;
    if (MPI_Test(&m_request, &flag, MPI_STATUS_IGNORE) != MPI_SUCCESS) {
        throw std::runtime_error{ std::string{ __PRETTY_FUNCTION__ }
                                  + " - MPI_Test(...) returned error" };
    }
    return flag;
}

void mpi::Request::cancel() &&
{
    if (MPI_Cancel(&m_request) != MPI_SUCCESS) {
        throw std::runtime_error{ std::string{ __PRETTY_FUNCTION__ }
                                  + " - MPI_Cancel(...) returned error" };
    }
}

void mpi::Request::start() &
{
    if (MPI_Start(&m_request) != MPI_SUCCESS) {
        throw std::runtime_error{ std::string{ __PRETTY_FUNCTION__ }
                                  + " - MPI_Start(...) returned error" };
    }
}

PARALLELKIT_NAMESPACE_END(1)
