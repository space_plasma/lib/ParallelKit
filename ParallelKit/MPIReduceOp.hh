/*
 * Copyright (c) 2021, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#ifndef ParallelKit_MPIREDUCEOP_HH
#define ParallelKit_MPIREDUCEOP_HH

template <class T> [[nodiscard]] ReduceOp ReduceOp::max(bool commutative) noexcept
{
    static_assert(!std::is_reference_v<T>, "T should not be a reference");

    constexpr auto op = [](void *invec, void *inoutvec, int *vlen, MPI_Datatype *) noexcept {
        auto const *rhs = reinterpret_cast<T const *>(invec);
        auto       *lhs = reinterpret_cast<T *>(inoutvec);
        std::transform(rhs, std::next(rhs, *vlen), lhs, lhs, [](T const &a, T const &b) {
            using std::max;
            return max(a, b);
        });
    };
    return { commutative, op };
}
template <class T> [[nodiscard]] ReduceOp ReduceOp::min(bool commutative) noexcept
{
    static_assert(!std::is_reference_v<T>, "T should not be a reference");

    constexpr auto op = [](void *invec, void *inoutvec, int *vlen, MPI_Datatype *) noexcept {
        auto const *rhs = reinterpret_cast<T const *>(invec);
        auto       *lhs = reinterpret_cast<T *>(inoutvec);
        std::transform(rhs, std::next(rhs, *vlen), lhs, lhs, [](T const &a, T const &b) {
            using std::min;
            return min(a, b);
        });
    };
    return { commutative, op };
}
template <class T> [[nodiscard]] ReduceOp ReduceOp::plus(bool commutative) noexcept
{
    static_assert(!std::is_reference_v<T>, "T should not be a reference");

    constexpr auto op = [](void *invec, void *inoutvec, int *vlen, MPI_Datatype *) noexcept {
        auto const *rhs = reinterpret_cast<T const *>(invec);
        auto       *lhs = reinterpret_cast<T *>(inoutvec);
        std::transform(rhs, std::next(rhs, *vlen), lhs, lhs, std::plus{});
    };
    return { commutative, op };
}
template <class T> [[nodiscard]] ReduceOp ReduceOp::prod(bool commutative) noexcept
{
    static_assert(!std::is_reference_v<T>, "T should not be a reference");

    constexpr auto op = [](void *invec, void *inoutvec, int *vlen, MPI_Datatype *) noexcept {
        auto const *rhs = reinterpret_cast<T const *>(invec);
        auto       *lhs = reinterpret_cast<T *>(inoutvec);
        std::transform(rhs, std::next(rhs, *vlen), lhs, lhs, std::multiplies{});
    };
    return { commutative, op };
}
template <class T> [[nodiscard]] ReduceOp ReduceOp::logic_and(bool commutative) noexcept
{
    static_assert(!std::is_reference_v<T>, "T should not be a reference");

    constexpr auto op = [](void *invec, void *inoutvec, int *vlen, MPI_Datatype *) noexcept {
        auto const *rhs = reinterpret_cast<T const *>(invec);
        auto       *lhs = reinterpret_cast<T *>(inoutvec);
        std::transform(rhs, std::next(rhs, *vlen), lhs, lhs, std::logical_and{});
    };
    return { commutative, op };
}
template <class T> [[nodiscard]] ReduceOp ReduceOp::bit_and(bool commutative) noexcept
{
    static_assert(!std::is_reference_v<T>, "T should not be a reference");

    constexpr auto op = [](void *invec, void *inoutvec, int *vlen, MPI_Datatype *) noexcept {
        auto const *rhs = reinterpret_cast<T const *>(invec);
        auto       *lhs = reinterpret_cast<T *>(inoutvec);
        std::transform(rhs, std::next(rhs, *vlen), lhs, lhs, std::bit_and{});
    };
    return { commutative, op };
}
template <class T> [[nodiscard]] ReduceOp ReduceOp::logic_or(bool commutative) noexcept
{
    static_assert(!std::is_reference_v<T>, "T should not be a reference");

    constexpr auto op = [](void *invec, void *inoutvec, int *vlen, MPI_Datatype *) noexcept {
        auto const *rhs = reinterpret_cast<T const *>(invec);
        auto       *lhs = reinterpret_cast<T *>(inoutvec);
        std::transform(rhs, std::next(rhs, *vlen), lhs, lhs, std::logical_or{});
    };
    return { commutative, op };
}
template <class T> [[nodiscard]] ReduceOp ReduceOp::bit_or(bool commutative) noexcept
{
    static_assert(!std::is_reference_v<T>, "T should not be a reference");

    constexpr auto op = [](void *invec, void *inoutvec, int *vlen, MPI_Datatype *) noexcept {
        auto const *rhs = reinterpret_cast<T const *>(invec);
        auto       *lhs = reinterpret_cast<T *>(inoutvec);
        std::transform(rhs, std::next(rhs, *vlen), lhs, lhs, std::bit_or{});
    };
    return { commutative, op };
}
template <class T> [[nodiscard]] ReduceOp ReduceOp::bit_xor(bool commutative) noexcept
{
    static_assert(!std::is_reference_v<T>, "T should not be a reference");

    constexpr auto op = [](void *invec, void *inoutvec, int *vlen, MPI_Datatype *) noexcept {
        auto const *rhs = reinterpret_cast<T const *>(invec);
        auto       *lhs = reinterpret_cast<T *>(inoutvec);
        std::transform(rhs, std::next(rhs, *vlen), lhs, lhs, std::bit_xor{});
    };
    return { commutative, op };
}

#endif // ParallelKit_MPIREDUCEOP_HH
