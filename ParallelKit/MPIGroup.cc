/*
 * Copyright (c) 2021, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "MPIGroup.h"

#include "MPIBaseComm.h"
#include "println.h"

#include <iostream>
#include <stdexcept>
#include <string>

PARALLELKIT_NAMESPACE_BEGIN(1)

mpi::Group::~Group()
{
    if (MPI_GROUP_NULL != m_group && MPI_GROUP_EMPTY != m_group
        && MPI_Group_free(&m_group) != MPI_SUCCESS) {
        println(std::cerr, "%% ", __PRETTY_FUNCTION__, " - MPI_Group_free(...) returned error");
#if defined(PARALLELKIT_SHOULD_TERMINATE_ON_FAILURE_IN_DESTRUCTOR)                                 \
    && PARALLELKIT_SHOULD_TERMINATE_ON_FAILURE_IN_DESTRUCTOR
        std::terminate();
#endif
    }
}

int mpi::Group::size() const
{
    int size;
    if (MPI_Group_size(m_group, &size) != MPI_SUCCESS) {
        throw std::runtime_error{ std::string{ __PRETTY_FUNCTION__ }
                                  + " - MPI_Group_size(...) returned error" };
    }
    return size;
}
auto mpi::Group::rank() const -> Rank
{
    Rank rank;
    if (MPI_Group_rank(m_group, &rank.v) != MPI_SUCCESS) {
        throw std::runtime_error{ std::string{ __PRETTY_FUNCTION__ }
                                  + " - MPI_Group_rank(...) returned error" };
    }
    return rank;
}

auto mpi::Group::translate(Rank const a_rank, Group const &to) const -> Rank
{
    constexpr long n = 1;
    Rank           b_rank;
    if (MPI_Group_translate_ranks(m_group, n, &a_rank.v, *to, &b_rank.v) != MPI_SUCCESS) {
        throw std::runtime_error{ std::string{ __PRETTY_FUNCTION__ }
                                  + " - MPI_Group_translate_ranks(...) returned error" };
    }
    return b_rank;
}
auto mpi::Group::translate(Rank const a_rank, BaseComm const &to) const -> Rank
{
    return translate(a_rank, to.group());
}

auto mpi::Group::compare(Group const &other) const -> Comparison
{
    int result;
    if (MPI_Group_compare(m_group, *other, &result) != MPI_SUCCESS) {
        throw std::runtime_error{ std::string{ __PRETTY_FUNCTION__ }
                                  + " - MPI_Group_compare(...) returned error" };
    }
    switch (result) {
        case MPI_IDENT:
            return identical;
        case MPI_SIMILAR:
            return similar;
        case MPI_UNEQUAL:
            return unequal;
        default:
            throw std::domain_error{ std::string{ __PRETTY_FUNCTION__ }
                                     + " - unknown Comparison enum" };
    }
}
auto mpi::Group::compare(BaseComm const &other) const -> Comparison
{
    return compare(other.group());
}

auto mpi::Group::operator|(Group const &other) const -> Group
{
    MPI_Group g;
    if (MPI_Group_union(m_group, *other, &g) != MPI_SUCCESS) {
        throw std::runtime_error{ std::string{ __PRETTY_FUNCTION__ }
                                  + " - MPI_Group_union(...) returned error" };
    }
    return Group{ g };
}
auto mpi::Group::operator&(Group const &other) const -> Group
{
    MPI_Group g;
    if (MPI_Group_intersection(m_group, *other, &g) != MPI_SUCCESS) {
        throw std::runtime_error{ std::string{ __PRETTY_FUNCTION__ }
                                  + " - MPI_Group_intersection(...) returned error" };
    }
    return Group{ g };
}
auto mpi::Group::operator-(Group const &other) const -> Group
{
    MPI_Group g;
    if (MPI_Group_difference(m_group, *other, &g) != MPI_SUCCESS) {
        throw std::runtime_error{ std::string{ __PRETTY_FUNCTION__ }
                                  + " - MPI_Group_difference(...) returned error" };
    }
    return Group{ g };
}

auto mpi::Group::included(unsigned long size, Rank const *ranks) const -> Group
{
    MPI_Group g;
    if (MPI_Group_incl(m_group, static_cast<int>(size), (size ? &ranks->v : nullptr), &g)
        != MPI_SUCCESS) {
        throw std::runtime_error{ std::string{ __PRETTY_FUNCTION__ }
                                  + " - MPI_Group_incl(...) returned error" };
    }
    return Group{ g };
}
auto mpi::Group::excluded(unsigned long size, Rank const *ranks) const -> Group
{
    MPI_Group g;
    if (MPI_Group_excl(m_group, static_cast<int>(size), (size ? &ranks->v : nullptr), &g)
        != MPI_SUCCESS) {
        throw std::runtime_error{ std::string{ __PRETTY_FUNCTION__ }
                                  + " - MPI_Group_excl(...) returned error" };
    }
    return Group{ g };
}

PARALLELKIT_NAMESPACE_END(1)
