/*
 * Copyright (c) 2017-2021, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

/// @file
/// Defines library-level config macros.
///

/// @def PARALLELKIT_ROOT_NAMESPACE
/// API root namespace macro definition
///
/// All non-global, public APIs are under this root namespace.
///

/// @def PARALLELKIT_VERSION_NAMESPACE(ver)
/// API versioning namespace macro definition
///
/// The version namespace is formed by concatenating of `v_`, ver, and `_`.
/// @param ver The version number.
///

/// @def PARALLELKIT_NAMESPACE(ver)
/// The namespace under which the specific version of APIs is defined
///
/// The APIs of the given version are defined under
/// PARALLELKIT_ROOT_NAMESPACE::PARALLELKIT_VERSION_NAMESPACE(ver).
/// @param ver The version number.
///

#ifndef ParallelKit_config_h
#define ParallelKit_config_h

/// @cond
// macro operation helpers
// https://github.com/pfultz2/Cloak/wiki/C-Preprocessor-tricks,-tips,-and-idioms
//
#define _PARALLELKIT_CAT(first, ...)           _PARALLELKIT_PRIMITIVE_CAT(first, __VA_ARGS__)
#define _PARALLELKIT_PRIMITIVE_CAT(first, ...) first##__VA_ARGS__

#define _PARALLELKIT_CHECK_N(x, n, ...) n
#define _PARALLELKIT_CHECK(...)         _PARALLELKIT_CHECK_N(__VA_ARGS__, 0, )
#define _PARALLELKIT_PROBE(x)           x, 1,

#define _PARALLELKIT_PAIR_1_1 _PARALLELKIT_PROBE(~)
#define _PARALLELKIT_PAIR_2_2 _PARALLELKIT_PROBE(~)
#define _PARALLELKIT_PAIR_3_3 _PARALLELKIT_PROBE(~)
#define _PARALLELKIT_PAIR_4_4 _PARALLELKIT_PROBE(~)
#define _PARALLELKIT_PAIR_5_5 _PARALLELKIT_PROBE(~)
#define _PARALLELKIT_PAIR_6_6 _PARALLELKIT_PROBE(~)
#define _PARALLELKIT_PAIR_7_7 _PARALLELKIT_PROBE(~)
#define _PARALLELKIT_PAIR_8_8 _PARALLELKIT_PROBE(~)
#define _PARALLELKIT_PAIR_9_9 _PARALLELKIT_PROBE(~)
#define _PARALLELKIT_PAIR(x, y)                                                                    \
    _PARALLELKIT_CAT(_PARALLELKIT_CAT(_PARALLELKIT_CAT(_PARALLELKIT_PAIR_, x), _), y)
#define _PARALLELKIT_IS_SAME(x, y) _PARALLELKIT_CHECK(_PARALLELKIT_PAIR(x, y))
/// @endcond

// namespace
//
#ifndef PARALLELKIT_ROOT_NAMESPACE
#define PARALLELKIT_ROOT_NAMESPACE parallel
/// @cond
#define _PARALLELKIT_NAMESPACE_ROOT namespace PARALLELKIT_ROOT_NAMESPACE
/// @endcond
#endif

#ifndef PARALLELKIT_VERSION_NAMESPACE
#define PARALLELKIT_VERSION_NAMESPACE(ver) _PARALLELKIT_CAT(_PARALLELKIT_CAT(v_, ver), _)
/// @cond
#define _PARALLELKIT_NAMESPACE_VERSION_0(ver) namespace PARALLELKIT_VERSION_NAMESPACE(ver)
#define _PARALLELKIT_NAMESPACE_VERSION_1(ver) inline namespace PARALLELKIT_VERSION_NAMESPACE(ver)
#define _PARALLELKIT_NAMESPACE_VERSION(ver)                                                        \
    _PARALLELKIT_CAT(_PARALLELKIT_NAMESPACE_VERSION_,                                              \
                     _PARALLELKIT_IS_SAME(ver, PARALLELKIT_INLINE_VERSION))                        \
    (ver)
/// @endcond
#endif

#ifndef PARALLELKIT_NAMESPACE
#define PARALLELKIT_NAMESPACE(ver) PARALLELKIT_ROOT_NAMESPACE::PARALLELKIT_VERSION_NAMESPACE(ver)
/// @cond
#define PARALLELKIT_NAMESPACE_BEGIN(ver)                                                           \
    _PARALLELKIT_NAMESPACE_ROOT                                                                    \
    {                                                                                              \
        _PARALLELKIT_NAMESPACE_VERSION(ver)                                                        \
        {
//
#define PARALLELKIT_NAMESPACE_END(ver)                                                             \
    }                                                                                              \
    }
/// @endcond
#endif

// flag to indicate error handler setting
//
#ifndef PARALLELKIT_COMM_SET_ERROR_HANDLER
/// @cond
#define PARALLELKIT_COMM_SET_ERROR_HANDLER 1
/// @endcond
#endif

// flag to indicate termination on error return in destructor
//
#ifndef PARALLELKIT_SHOULD_TERMINATE_ON_FAILURE_IN_DESTRUCTOR
/// @cond
#define PARALLELKIT_SHOULD_TERMINATE_ON_FAILURE_IN_DESTRUCTOR 1
/// @endcond
#endif

#endif /* ParallelKit_config_h */
