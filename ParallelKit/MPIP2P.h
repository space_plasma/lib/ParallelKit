/*
 * Copyright (c) 2021-2022, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#ifndef ParallelKit_MPIP2P_h
#define ParallelKit_MPIP2P_h

#include <ParallelKit/ParallelKit-config.h>

#include <ParallelKit/MPIRank.h>
#include <ParallelKit/MPIRequest.h>
#include <ParallelKit/MPITag.h>
#include <ParallelKit/MPIType.h>
#include <ParallelKit/TypeMap.h>

#include <memory>
#include <type_traits>
#include <utility>

#include <ParallelKit/mpi_wrapper.h>

PARALLELKIT_NAMESPACE_BEGIN(1)
namespace mpi {

// forward decls
//
/// @cond
template <class> class P2P;
class Comm;
/// @endcond

/// Wrapper for point-to-point communication.
///
template <> class P2P<Comm> {
public:
    // MARK:- Persistent Communication Requests
    /// @name Persistent Communication Requests
    /// @{

    /// Creates a persistent communication request for a synchronous mode send operation.
    /// @param [in] data Data buffer.
    /// @param type Data Type.
    /// @param count Data count.
    /// @param to A pair of Rank and Tag.
    /// @return A Request object.
    ///
    [[nodiscard]] Request ssend_init(void const *data, Type const &type, long count,
                                     std::pair<Rank, Tag> const &to) const;

    /// Creates a persistent communication request for a receive operation.
    /// @param [out] buffer Data buffer.
    /// @param type Data Type.
    /// @param count Data count.
    /// @param from A pair of Rank and Tag.
    /// @return A Request object.
    ///
    [[nodiscard]] Request recv_init(void *buffer, Type const &type, long count,
                                    std::pair<Rank, Tag> const &from) const;
    /// @}

public:
    // MARK:- Non-blocking Send/Blocking Recv
    /// @name Non-blocking Send/Blocking Recv
    /// @{

    /// Start a synchronous mode, nonblocking send.
    /// @param [in] data Data buffer.
    /// @param type Data Type.
    /// @param count Data count.
    /// @param to A pair of Rank and Tag.
    /// @return A Request object.
    ///
    [[nodiscard]] Request issend(void const *data, Type const &type, long count,
                                 std::pair<Rank, Tag> const &to) const;

    /// Message probe.
    /// @details Blocks until there is a message that can be received and that matches the pattern
    /// specified by the arguments.
    /// @param from A pair of Rank and Tag.
    /// @return `MPI_Status` token.
    ///
    [[nodiscard]] MPI_Status probe(std::pair<Rank, Tag> const &from) const;

    /// Matching probe.
    /// @details Blocks until there is a message that can be received and that matches the pattern
    /// specified by the arguments.
    /// @param from A pair of Rank and Tag.
    /// @param [out] msg Message handle.
    /// @return `MPI_Status` token.
    ///
    [[nodiscard]] MPI_Status probe(std::pair<Rank, Tag> const &from, MPI_Message *msg) const;

    /// Blocking receive.
    /// @details The length of the received message must be less than or equal to the length of the
    /// receive buffer. An overflow error occurs if all incoming data does not fit, without
    /// truncation, into the receive buffer. If a message that is shorter than the receive buffer
    /// arrives, then only those locations corresponding to the (shorter) message are modified.
    /// @param [out] buffer Data buffer.
    /// @param type Data Type.
    /// @param count Data count.
    /// @param from A pair of Rank and Tag.
    /// @return `MPI_Status` token, which can be used to retrieve received message count.
    ///
    MPI_Status recv(void *buffer, Type const &type, long count,
                    std::pair<Rank, Tag> const &from) const;

    /// Blocking receive of message matched by probe.
    /// @param [out] buffer Data buffer.
    /// @param type Data Type.
    /// @param count Data count.
    /// @param [in] msg Message handle returned by probe.
    /// @return `MPI_Status` token.
    ///
    static MPI_Status recv(void *buffer, Type const &type, long count, MPI_Message *msg);
    /// @}

public:
    // MARK:- Send-Recv
    /// @name Send-Recv
    /// Bi-directional message exchange operation between two end points
    /// @{

    /// Execute blocking send and receive.
    /// @details Both send and receive use the same communicator, but possibly different tags.
    /// The send buffer and receive buffers must be disjoint, and may have different lengths and
    /// datatypes.
    /// @param [in] send_data Outgoing message buffer.
    /// @param send_type Outgoing message type.
    /// @param send_count Outgoing message count.
    /// @param send_to Address of the receiving end.
    /// @param [out] recv_buffer Receiving message buffer.
    /// @param recv_type Receiving message type.
    /// @param recv_count Receiving message count.
    /// @param recv_from Address of the message sender.
    /// @return `MPI_Status` token.
    ///
    MPI_Status send_recv(void const *send_data, Type const &send_type, long send_count,
                         std::pair<Rank, Tag> const &send_to, void *recv_buffer,
                         Type const &recv_type, long recv_count,
                         std::pair<Rank, Tag> const &recv_from) const;

    /// Execute blocking send and receive replace.
    /// @details The same buffer is used both for the send and for the receive, so that the message
    /// sent is replaced by the message received.
    /// @param [in, out] data Message buffer.
    /// @param type Message type.
    /// @param count Message count.
    /// @param send_to Address of the receiving end.
    /// @param recv_from Address of the message sender.
    /// @return `MPI_Status` token.
    ///
    MPI_Status send_recv(void *data, Type const &type, long count,
                         std::pair<Rank, Tag> const &send_to,
                         std::pair<Rank, Tag> const &recv_from) const;
    /// @}
};

} // namespace mpi
PARALLELKIT_NAMESPACE_END(1)

#endif /* ParallelKit_MPIP2P_h */
