/*
 * Copyright (c) 2021-2022, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#ifndef ParallelKit_MPIComparison_h
#define ParallelKit_MPIComparison_h

#include <ParallelKit/ParallelKit-config.h>

#include <ParallelKit/mpi_wrapper.h>

PARALLELKIT_NAMESPACE_BEGIN(1)
namespace mpi {

/// Comparison result.
///
enum Comparison : long {
    identical = MPI_IDENT,
    congruent = MPI_CONGRUENT,
    similar   = MPI_SIMILAR,
    unequal   = MPI_UNEQUAL,
};

} // namespace mpi
PARALLELKIT_NAMESPACE_END(1)

#endif /* ParallelKit_MPIComparison_h */
