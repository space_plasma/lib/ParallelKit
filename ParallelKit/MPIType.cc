/*
 * Copyright (c) 2021, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "MPIType.h"

#include "power_of_2.h"
#include "println.h"

#include <algorithm>
#include <functional>
#include <iostream>
#include <iterator>
#include <stdexcept>
#include <vector>

PARALLELKIT_NAMESPACE_BEGIN(1)

mpi::Type::~Type()
{
    if (m.is_native || MPI_DATATYPE_NULL == m.datatype) {
        return;
    }
    if (MPI_Type_free(&m.datatype) != MPI_SUCCESS) {
        println(std::cerr, "%% ", __PRETTY_FUNCTION__, " - MPI_Type_free(...) returned error");
#if defined(PARALLELKIT_SHOULD_TERMINATE_ON_FAILURE_IN_DESTRUCTOR)                                 \
    && PARALLELKIT_SHOULD_TERMINATE_ON_FAILURE_IN_DESTRUCTOR
        std::terminate();
#endif
    }
}

std::string mpi::Type::label() const
{
    char label[MPI_MAX_OBJECT_NAME];
    int  len;
    if (MPI_Type_get_name(m.datatype, label, &len) != MPI_SUCCESS) {
        throw std::runtime_error{ std::string{ __PRETTY_FUNCTION__ }
                                  + " - MPI_Type_get_name(...) returned error" };
    }
    return label;
}
void mpi::Type::set_label(char const *label)
{
    if (MPI_Type_set_name(m.datatype, label) != MPI_SUCCESS) {
        throw std::runtime_error{ std::string{ __PRETTY_FUNCTION__ }
                                  + " - MPI_Type_set_name(...) returned error" };
    }
}

long mpi::Type::signature_size() const
{
    MPI_Count size;
    if (MPI_Type_size_x(m.datatype, &size) != MPI_SUCCESS) {
        throw std::runtime_error{ std::string{ __PRETTY_FUNCTION__ }
                                  + " - MPI_Type_size_x(...) returned error" };
    }
    if (MPI_UNDEFINED == size) {
        throw std::runtime_error{ std::string{ __PRETTY_FUNCTION__ } + " - size is undefined" };
    }
    return size;
}

std::pair<long, long> mpi::Type::extent() const
{
    MPI_Count lb, extent;
    if (MPI_Type_get_extent_x(m.datatype, &lb, &extent) != MPI_SUCCESS) {
        throw std::runtime_error{ std::string{ __PRETTY_FUNCTION__ }
                                  + " - MPI_Type_get_extent_x(...) returned error" };
    }
    if (MPI_UNDEFINED == lb || MPI_UNDEFINED == extent) {
        throw std::runtime_error{ std::string{ __PRETTY_FUNCTION__ } + " - extent is undefined" };
    }
    return std::make_pair(lb, extent);
}
std::pair<long, long> mpi::Type::true_extent() const
{
    MPI_Count lb, extent;
    if (MPI_Type_get_true_extent_x(m.datatype, &lb, &extent) != MPI_SUCCESS) {
        throw std::runtime_error{ std::string{ __PRETTY_FUNCTION__ }
                                  + " - MPI_Type_get_extent_x(...) returned error" };
    }
    if (MPI_UNDEFINED == lb || MPI_UNDEFINED == extent) {
        throw std::runtime_error{ std::string{ __PRETTY_FUNCTION__ } + " - extent is undefined" };
    }
    return std::make_pair(lb, extent);
}

long mpi::Type::get_count(MPI_Status const &status) const
{
    int count;
    if (MPI_Get_count(&status, m.datatype, &count) != MPI_SUCCESS) {
        throw std::runtime_error{ std::string{ __PRETTY_FUNCTION__ }
                                  + " - MPI_Get_count(...) returned error" };
    }
    if (MPI_UNDEFINED == count) {
        throw std::runtime_error{ std::string{ __PRETTY_FUNCTION__ } + " - count is undefined" };
    }
    return count;
}
long mpi::Type::get_elements(MPI_Status const &status) const
{
    MPI_Count count;
    if (MPI_Get_elements_x(&status, m.datatype, &count) != MPI_SUCCESS) {
        throw std::runtime_error{ std::string{ __PRETTY_FUNCTION__ }
                                  + " - MPI_Get_elements_x(...) returned error" };
    }
    if (MPI_UNDEFINED == count) {
        throw std::runtime_error{ std::string{ __PRETTY_FUNCTION__ } + " - count is undefined" };
    }
    return count;
}

auto mpi::Type::committed() && -> Type
{
    if (MPI_Type_commit(&m.datatype) != MPI_SUCCESS) {
        throw std::runtime_error{ std::string{ __PRETTY_FUNCTION__ }
                                  + " - MPI_Type_commit(...) returned error" };
    }
    return std::move(*this);
}

auto mpi::Type::realigned(long const alignment) const -> Type
{
    // check argument
    //
    if (alignment <= 0 || !is_power_of_2(alignment)) {
        throw std::invalid_argument{ std::string{ __PRETTY_FUNCTION__ }
                                     + " - invalid alignment: " + std::to_string(alignment) };
    }
    if (alignment < m.alignment) {
        throw std::invalid_argument{
            std::string{ __PRETTY_FUNCTION__ }
            + " - the new alignment has to be greater than or equal to the current alignment"
        };
    }

    // adjust extent
    //
    long extent = this->extent().second;
    if (extent % alignment) {
        extent = (extent / alignment + 1) * alignment;
    }

    // resized
    //
    constexpr long lb = 0; // all Type instances are
                           // assumed to have 0 lb (i.e., lb marker is not used)
    MPI_Datatype t;
    if (MPI_Type_create_resized(m.datatype, lb, extent, &t) != MPI_SUCCESS) {
        throw std::runtime_error{ std::string{ __PRETTY_FUNCTION__ }
                                  + " - MPI_Type_create_resized(...) returned error" };
    }
    return Type{ t, alignment, false }.committed();
}

auto mpi::Type::vector(int const n_blocks, int const blocksize, int const stride) const -> Type
{
    MPI_Datatype t;
    if (MPI_Type_vector(n_blocks, blocksize, stride, m.datatype, &t) != MPI_SUCCESS) {
        throw std::runtime_error{ std::string{ __PRETTY_FUNCTION__ }
                                  + " - MPI_Type_vector(...) returned error" };
    }
    return Type{ t, m.alignment, false }.committed();
}
auto mpi::Type::indexed(int const n_blocks, int const *blocksizes, int const *displacements) const
    -> Type
{
    // check integer bound
    //
    for (int i = 0; i < n_blocks; ++i) {
        if (blocksizes[i] < 0) {
            throw std::invalid_argument{ std::string{ __PRETTY_FUNCTION__ }
                                         + " - invalid block size(s)" };
        }
    }

    // create type
    //
    MPI_Datatype t;
    if (MPI_Type_indexed(n_blocks, blocksizes, displacements, m.datatype, &t) != MPI_SUCCESS) {
        throw std::runtime_error{ std::string{ __PRETTY_FUNCTION__ }
                                  + " - MPI_Type_indexed(...) returned error" };
    }
    return Type{ t, m.alignment, false }.committed();
}
auto mpi::Type::indexed(int const n_disps, int const blocksize, int const *displacements) const
    -> Type
{
    MPI_Datatype t;
    if (MPI_Type_create_indexed_block(n_disps, blocksize, displacements, m.datatype, &t)
        != MPI_SUCCESS) {
        throw std::runtime_error{ std::string{ __PRETTY_FUNCTION__ }
                                  + " - MPI_Type_indexed_block(...) returned error" };
    }
    return Type{ t, m.alignment, false }.committed();
}
auto mpi::Type::subarray(int const ND, int const *dims, int const *slice_locs,
                         int const *slice_lens) const -> Type
{
    // check integer bound
    //
    for (int i = 0; i < ND; ++i) {
        if (dims[i] <= 0) {
            throw std::invalid_argument{ std::string{ __PRETTY_FUNCTION__ }
                                         + " - invalid dimension size(s)" };
        }
        if (slice_locs[i] < 0 || slice_locs[i] >= dims[i]) {
            throw std::invalid_argument{ std::string{ __PRETTY_FUNCTION__ }
                                         + " - invalid slice location(s)" };
        }
        if (slice_lens[i] <= 0 || slice_locs[i] + slice_lens[i] > dims[i]) {
            throw std::invalid_argument{ std::string{ __PRETTY_FUNCTION__ }
                                         + " - invalid displacement(s)" };
        }
    }

    // create type
    //
    MPI_Datatype t;
    if (MPI_Type_create_subarray(ND, dims, slice_lens, slice_locs, MPI_ORDER_C, m.datatype, &t)
        != MPI_SUCCESS) {
        throw std::runtime_error{ std::string{ __PRETTY_FUNCTION__ }
                                  + " - MPI_Type_create_subarray(...) returned error" };
    }
    return Type{ t, m.alignment, false }.committed();
}
auto mpi::Type::make_composite(unsigned long size /*at least one element*/, Type const *types)
    -> Type
{
    // prepare arguments; there must be at least one element
    //
    std::vector<MPI_Datatype> datatypes(size);
    std::transform(types, types + size, begin(datatypes), std::mem_fn(&Type::operator*));
    //
    std::vector<MPI_Aint> disps(size); // initially filled with extents
    std::transform(types, types + size, begin(disps), [](Type const &t) {
        return t.extent().second;
    });
    //
    std::vector<long> aligns(size);
    std::transform(types, types + size, begin(aligns), std::mem_fn(&Type::alignment));

    // calculate displacements
    //
    MPI_Aint ub = 0; // this holds the upper bound
    for (unsigned long i = 0; i < size; ++i) {
        MPI_Aint const align = aligns[i];
        if (ub % align) {                  // if the cumulative sum IS NOT divisable
                                           // by the extent, adjust so that it IS
            ub = (ub / align + 1) * align; // this is now adjusted lower bound
        }
        MPI_Aint &disp = disps[i];
        ub += disp;       // this is the new upper bound
        disp = ub - disp; // now disp holds the lower bound
    }

    // block sizes are all 1
    //
    std::vector<int> const blocks(size, 1);

    // create composite type
    //
    MPI_Datatype t;
    if (MPI_Type_create_struct(static_cast<int>(size), blocks.data(), disps.data(),
                               datatypes.data(), &t)
        != MPI_SUCCESS) {
        throw std::runtime_error{ std::string{ __PRETTY_FUNCTION__ }
                                  + " - MPI_Type_create_struct(...) returned error" };
    }
    return Type{ t, 1 /*this is dummy alignment*/, false }.realigned(
        *std::max_element(begin(aligns), end(aligns)));
}

PARALLELKIT_NAMESPACE_END(1)
