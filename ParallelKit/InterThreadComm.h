/*
 * Copyright (c) 2020, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#ifndef ParallelKit_InterThreadComm_h
#define ParallelKit_InterThreadComm_h

#include <ParallelKit/MessageDispatch-tuple.h>
#include <ParallelKit/MessageDispatch-variant.h>

#endif /* ParallelKit_InterThreadComm_h */
