/*
 * Copyright (c) 2021, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "MPIReduction.h"

#include "MPIComm.h"

#include <limits>
#include <stdexcept>
#include <string>

PARALLELKIT_NAMESPACE_BEGIN(1)

using mpi::Comm;

namespace {
constexpr int int_max = std::numeric_limits<int>::max();
//
[[nodiscard]] MPI_Comm operator*(mpi::Reduction<Comm> const &comm) noexcept
{
    return static_cast<Comm const &>(comm).operator*();
}
} // namespace

// MARK:- Low-level MPI Interface Layer
//
void mpi::Reduction<Comm>::reduce(ReduceOp const &op, void const *send_data, void *recv_buffer,
                                  Type const &type, long const count, Rank const root) const
{
    if (count > int_max) {
        throw std::invalid_argument{ std::string{ __PRETTY_FUNCTION__ }
                                     + " - number of elements greater than int::max" };
    }
    if (MPI_Reduce(send_data, recv_buffer, static_cast<int>(count), *type, *op, root, **this)
        != MPI_SUCCESS) {
        throw std::runtime_error{ std::string{ __PRETTY_FUNCTION__ }
                                  + " - MPI_Reduce(...) returned error" };
    }
}
void mpi::Reduction<Comm>::reduce(ReduceOp const &op, void *buffer, Type const &type,
                                  long const count) const
{
    reduce(op, MPI_IN_PLACE, buffer, type, count, static_cast<Comm const *>(this)->rank());
}

void mpi::Reduction<Comm>::all_reduce(ReduceOp const &op, void const *send_data, void *recv_buffer,
                                      Type const &type, long const count) const
{
    if (count > int_max) {
        throw std::invalid_argument{ std::string{ __PRETTY_FUNCTION__ }
                                     + " - number of elements greater than int::max" };
    }
    if (MPI_Allreduce(send_data, recv_buffer, static_cast<int>(count), *type, *op, **this)
        != MPI_SUCCESS) {
        throw std::runtime_error{ std::string{ __PRETTY_FUNCTION__ }
                                  + " - MPI_Allreduce(...) returned error" };
    }
}

PARALLELKIT_NAMESPACE_END(1)
