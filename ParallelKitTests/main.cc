/*
 * Copyright (c) 2021-2022, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "TestInterProcessComm.h"

#include <ParallelKit/mpi_wrapper.h>

#define CATCH_CONFIG_RUNNER
#include "catch2/catch.hpp"

int main(int argc, char *argv[])
{
    using PAR::mpi::Comm;

    // global setup...
    if (!Comm::is_initialized()) {
        Comm::init(&argc, &argv);
        if (MPI_Comm_set_errhandler(MPI_COMM_WORLD, MPI_ERRORS_RETURN) != MPI_SUCCESS)
            throw std::runtime_error{ std::string{ __PRETTY_FUNCTION__ }
                                      + " - MPI_Comm_set_errhandler(...) returned error" };
    }

    int result = Catch::Session().run(argc, argv);

    // global clean-up...
    if (!Comm::is_finalized())
        MPI_Finalize();

    return result;
}
