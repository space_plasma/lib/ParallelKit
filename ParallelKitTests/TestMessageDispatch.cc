/*
 * Copyright (c) 2021, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "TestInterThreadComm.h"
#include <catch2/catch.hpp>

TEST_CASE("testSingleThreadDispatch", "[InterThreadComm]")
{
    using PAR::MessageDispatch;
    struct S {
        std::string s;
    };
    using Ptr = std::unique_ptr<std::string>;

    MessageDispatch<long, Ptr, S> dispatch{ 2 };

    S const           a1{ __PRETTY_FUNCTION__ };
    long const        b1{ 1 };
    std::string const c1{ __FUNCTION__ };

    auto tk1 = dispatch.send<0>(b1, { 0, 1 });
    auto tk2 = dispatch.send(std::make_unique<std::string>(c1), { 0, 1 });
    auto tk3 = dispatch.send(a1, { 0, 1 });

    long const        b2 = dispatch.recv<long>({ 0, 1 });
    std::string const c2 = dispatch.recv<1>({ 0, 1 }).unpack(&Ptr::operator*);
    std::string const a2 = dispatch.recv<S>({ 0, 1 }).unpack(&S::s);

    std::move(tk1).wait();
    std::move(tk2).wait();
    std::move(tk3).wait();

    INFO("a1 = " << a1.s.c_str() << ", a2 = " << a2.c_str())
    CHECK(a1.s == a2);
    INFO("b1 = " << b1 << ", b2 = " << b2)
    CHECK(b1 == b2);
    INFO("c1 = " << c1.c_str() << ", c2 = " << c2.c_str())
    CHECK(c1 == c2);
}

TEST_CASE("testMultiThreadDispatch", "[InterThreadComm]")
{
    using PAR::MessageDispatch;

    constexpr int                                N = 10;
    MessageDispatch<std::unique_ptr<long>, long> dispatch{ N + 1 };

    auto const f = [&dispatch](int const i) -> long {
        using namespace std::chrono_literals;
        std::this_thread::sleep_for(1s);
        dispatch.send(std::make_unique<long>(i + 1), { i + 1, 0 }).wait();
        std::this_thread::sleep_for(1s);
        return dispatch.recv<long>({ 0, i + 1 });
    };

    std::array<std::future<long>, N> flist;
    for (unsigned i = 0; i < N; ++i) {
        flist[i] = std::async(std::launch::async, f, i);
    }

    long sum = 0;
    for (int i = 0; i < N; ++i) {
        sum += dispatch.recv<0>({ i + 1, 0 })
                   .unpack(
                       [](auto const ptr, long const sum) noexcept {
                           return sum + *ptr;
                       },
                       sum);
    }
    for (unsigned i = 0; i < N; ++i) {
        dispatch.send(sum, { 0U, i + 1 }).wait();
    }

    for (unsigned i = 0; i < N; ++i) {
        auto const s = sum;
        auto const v = flist.at(i).get();
        INFO("i = " << i << ", v = " << v << ", s = " << s)
        CHECK(v == s);
    }
}

TEST_CASE("testMultiThreadPingPong", "[InterThreadComm]")
{
    using PAR::MessageDispatch;

    MessageDispatch<long> dispatch{ 2 };

    auto f1 = std::async(std::launch::async, [&dispatch] {
        auto tk = dispatch.send<0>(1, { 1, 1 });
        (void)tk; // std::move(tk).wait();
        INFO("1st msg sent")
    });
    auto f2 = std::async(std::launch::async, [&dispatch] {
        dispatch.send<0>(2, { 1, 1 }).wait();
        INFO("2nd msg sent")
    });
    {
        using namespace std::chrono_literals;
        std::this_thread::sleep_for(1s);
    }
    dispatch.recv<0>({ 1, 1 }).unpack([](long const payload) {
        INFO("1st msg recv'ed: " << payload)
        REQUIRE((payload == 1 || payload == 2));
    });
    dispatch.recv<0>({ 1, 1 }).unpack([](long const payload) {
        using namespace std::chrono_literals;
        std::this_thread::sleep_for(1s);
        INFO("2nd msg recv'ed: " << payload)
        REQUIRE((payload == 1 || payload == 2));
    });

    f1.get();
    f2.get();
}

TEST_CASE("testMultiThreadCollective", "[InterThreadComm]")
{
    using PAR::MessageDispatch;

    MessageDispatch<std::unique_ptr<long>, long>    dispatch{ 3 };
    constexpr long                                  magic = 6;
    std::vector<decltype(dispatch)::Envelope> const dests{ { 0, 0 }, { 0, 1 }, { 0, 2 } };

    auto f1 = std::async(std::launch::async, [&dispatch] {
        dispatch.send<0>(dispatch.recv<0>({ 0, 1 }), { 1, 0 }).wait();
        return *dispatch.recv<long>({ 0, 1 });
    });
    auto f2 = std::async(std::launch::async, [&dispatch] {
        dispatch.send<0>(dispatch.recv<0>({ 0, 2 }), { 2, 0 }).wait();
        return *dispatch.recv<long>({ 0, 2 });
    });

    {
        std::vector<std::unique_ptr<long>> payloads;
        {
            payloads.push_back(std::make_unique<long>(1));
            payloads.push_back(std::make_unique<long>(2));
            payloads.push_back(std::make_unique<long>(3));
        }
        dispatch.scatter(std::move(payloads), dests).clear();
    }
    (void)dispatch.send<0>(dispatch.recv<0>({ 0, 0 }), { 0, 0 });
    long const sum = [](auto pkgs) {
        return std::accumulate(std::make_move_iterator(begin(pkgs)),
                               std::make_move_iterator(end(pkgs)), long{},
                               [](long const &a, auto b) {
                                   return a + **std::move(b);
                               });
    }(dispatch.gather<0>({ { 0, 0 }, { 1, 0 }, { 2, 0 } }));
    dispatch.bcast(sum, { { 0, 0 }, { 0, 1 }, { 0, 2 } }).clear();

    {
        long const v = dispatch.recv<long>({ 0, 0 });
        INFO("value recv'ed from {0, 0}: " << v)
        REQUIRE(magic == v);
    }
    {
        long const v = f1.get();
        INFO("value recv'ed from {0, 1}: " << v)
        REQUIRE(magic == v);
    }
    {
        long const v = f2.get();
        INFO("value recv'ed from {0, 2}: " << v)
        REQUIRE(magic == v);
    }

    dispatch.bcast<1>(3, { { 0, 0 }, { 1, 0 }, { 2, 0 } }).clear();
    dispatch.bcast<1>(2, { { 0, 0 }, { 0, 1 }, { 0, 2 } }).clear();
    dispatch.for_each<long>({ { 0, 0 }, { 1, 0 }, { 2, 0 } }, [](long const i) {
        INFO("i = " << i)
        CHECK(i == 3);
    });
    {
        long const v = dispatch.reduce<1>({ { 0, 0 }, { 0, 1 }, { 0, 2 } }, long{}, std::plus{});
        INFO("reduced value: " << v)
        CHECK(magic == v);
    }
}
