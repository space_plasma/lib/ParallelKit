/*
 * Copyright (c) 2021, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#import "TestInterProcessComm.h"

#import <mpich2/mpi.h>

@implementation TestInterProcessComm
using PAR::mpi::Comm;

- (void)setUp {
    // Put setup code here. This method is called before the invocation of each test method in the class.

    if (!Comm::is_initialized()) {
        int argc = 0;
        char **argv = nullptr;
        Comm::init(&argc, &argv);
        if (MPI_Comm_set_errhandler(MPI_COMM_WORLD, MPI_ERRORS_RETURN) != MPI_SUCCESS) {
            throw std::runtime_error{std::string{__PRETTY_FUNCTION__} + " - MPI_Comm_set_errhandler(...) returned error"};
        }
    }
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.

    //MPI_Finalize();
}

- (void)testComm_Comm {
    using PAR::mpi::Group;
    using PAR::mpi::Comparison;

    try {
        Comm c;
        XCTAssert(!c);

        c = Comm::self();
        XCTAssert(c && MPI_COMM_SELF == *c && c.compare(Comm::self()) == Comparison::identical);
        println(std::cout, c.label());

        c = Comm::world();
        XCTAssert(c && MPI_COMM_WORLD == *c && c.compare(Comm::world()) == Comparison::identical);
        println(std::cout, c.label());

        c = c.duplicated();
        c.set_label("duplicated WORLD");
        XCTAssert(c && c.compare(Comm::world()) == Comparison::congruent && c.compare(Comm::self()) == Comparison::congruent);
        println(std::cout, c.label());
        XCTAssert(c.size() == 1 && c.rank() == 0, @"size = %d, rank = %d", c.size(), c.rank().v);

        c = c.split(1);
        c.set_label("split comm");
        XCTAssert(c && c.compare(Comm::world()) == Comparison::congruent && c.compare(Comm::self()) == Comparison::congruent);
        println(std::cout, c.label());
        XCTAssert(c.size() == 1 && c.rank() == 0, @"size = %d, rank = %d", c.size(), c.rank().v);

        c = c.created(Group{});
        XCTAssert(!c);

        c = Comm::world().split(MPI_UNDEFINED);
        XCTAssert(!c);
    } catch (std::exception const &e) {
        XCTAssert(false, @"%s", e.what());
    }
}

- (void)testComm_Group {
    using PAR::mpi::Rank;
    using PAR::mpi::Group;
    using PAR::mpi::Comparison;

    try {
        Comm const world = Comm::world().duplicated();
        Group g;
        XCTAssert(g && *g == MPI_GROUP_EMPTY);

        g = world.group();
        XCTAssert(g && g.size() == 1 && g.rank() && g.rank() == 0);
        XCTAssert(g.translate(g.rank(), world.group()) == 0);
        XCTAssert(g.compare(world.group()) == Comparison::identical);
        XCTAssert(g.compare(Group{}) == Comparison::unequal);

        g = world.group() & Group{};
        XCTAssert(g && g.size() == 0 && !g.rank());
        XCTAssert(g.translate(Rank::null(), world.group()) == Rank::null());
        XCTAssert(g.compare(world.group()) == Comparison::unequal);
        XCTAssert(g.compare(Group{}) == Comparison::identical);

        g = world.group() | Group{};
        XCTAssert(g && g.size() == 1 && g.rank() && g.rank() == 0);
        XCTAssert(g.translate(g.rank(), world.group()) == 0);
        XCTAssert(g.compare(world.group()) == Comparison::identical);
        XCTAssert(g.compare(Group{}) == Comparison::unequal);

        g = world.group() - Group{};
        XCTAssert(g && g.size() == 1 && g.rank() && g.rank() == 0);
        XCTAssert(g.translate(g.rank(), world.group()) == 0);
        XCTAssert(g.compare(world.group()) == Comparison::identical);
        XCTAssert(g.compare(Group{}) == Comparison::unequal);

        g = world.group().included({world.rank()});
        XCTAssert(g && g.size() == 1 && g.rank() && g.rank() == 0);
        XCTAssert(g.translate(g.rank(), world.group()) == 0);
        XCTAssert(g.compare(world.group()) == Comparison::identical);
        XCTAssert(g.compare(Group{}) == Comparison::unequal);

        g = world.group().excluded({world.rank()});
        XCTAssert(g && g.size() == 0 && !g.rank());
        XCTAssert(g.translate(Rank::null(), world.group()) == Rank::null());
        XCTAssert(g.compare(world.group()) == Comparison::unequal);
        XCTAssert(g.compare(Group{}) == Comparison::identical);
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }
}

@end
