/*
 * Copyright (c) 2021, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#ifndef TestInterProcessComm_h
#define TestInterProcessComm_h

#if defined(__APPLE__) && defined(__OBJC__)
#import <XCTest/XCTest.h>

@interface TestInterProcessComm : XCTestCase

@end

#include "println.h"
#else
#include "../ParallelKit/println.h"
#endif

#define PARALLELKIT_INLINE_VERSION 0
#include <ParallelKit/InterProcessComm.h>

#include <iostream>
#include <stdexcept>
#include <string>

struct double16 {
    using native_type = double;
    std::aligned_storage_t<sizeof(native_type), sizeof(native_type) * 2> data;

    constexpr double16() noexcept : data{} {}
    double16(native_type x) noexcept { ::new (static_cast<void *>(&data)) native_type{ x }; }
    [[nodiscard]] auto operator*() const noexcept
    {
        return reinterpret_cast<native_type const &>(data);
    }
    friend bool     operator==(double16 const &A, double16 const &B) noexcept { return *A == *B; }
    friend bool     operator<(double16 const &A, double16 const &B) noexcept { return *A < *B; }
    friend double16 operator+(double16 const &A, double16 const &B) noexcept { return *A + *B; }
};

PARALLELKIT_NAMESPACE_BEGIN(1)
template <> struct TypeMap<double16> {
    using type = double16;
    [[nodiscard]] auto operator()() const
    {
        return make_type<double16::native_type>().realigned(alignof(type));
    }
};
PARALLELKIT_NAMESPACE_END(1)

namespace PAR = PARALLELKIT_NAMESPACE(1);

#endif /* TestInterProcessComm_h */
