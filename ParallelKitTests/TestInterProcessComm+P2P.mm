/*
 * Copyright (c) 2021, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#import "TestInterProcessComm.h"
#include <algorithm>
#include <random>
#include <array>

@interface TestInterProcessComm (P2P)

@end

@implementation TestInterProcessComm (P2P)
using PAR::mpi::Comm;
using PAR::mpi::Tag;
using PAR::make_type;

- (void)testP2P_Request {
    using PAR::mpi::Request;

    try {
        Request req1, req2{std::move(req1)};
        XCTAssert(req2.test());
        std::move(req1).wait();
    } catch (std::exception const &e) {
        XCTAssert(false, @"%s", e.what());
    }
}

- (void)testP2P_LowLevel {
    // barrier
    try {
        Comm::world().barrier();
    } catch (std::exception const &e) {
        XCTAssert(false, @"%s", e.what());
    }

    using T = std::pair<double16, char>;
    auto const tmap = PAR::make_type<T>();
    constexpr long N = 10;

    std::mt19937_64 rng{44393};
    std::uniform_real_distribution<> real;
    std::uniform_int_distribution<>  int_{std::numeric_limits<char>::min(), std::numeric_limits<char>::max()};

    // persistence
    try {
        Comm const comm = Comm::world().duplicated();

        std::array<T, N> v1{}, v2{};

        auto send_req = comm.ssend_init(v1.data(), tmap, v1.size(), {comm.rank(), {0}});
        auto recv_req = comm.recv_init(v2.data(), tmap, v2.size(), {comm.rank(), Tag::any()});

        constexpr long n = 11;
        for (unsigned i = 0; i < n; ++i) {
            for (auto &v : v1) {
                std::get<0>(v) = real(rng);
                std::get<1>(v) = static_cast<char>(int_(rng));
            }
            send_req.start();
            recv_req.start();
            std::move(recv_req).wait();
            std::move(send_req).wait();

            XCTAssert(std::equal(v1.begin(), v1.end(), v2.begin()));
        }
    } catch (std::exception const &e) {
        XCTAssert(false, @"%s", e.what());
    }

    // non-blocking send/blocking recv
    try {
        Comm const comm = Comm::world().split(1);

        std::array<T, N> v1{}, v2{};
        for (auto &v : v1) {
            std::get<0>(v) = real(rng);
            std::get<1>(v) = static_cast<char>(int_(rng));
        }

        auto req = comm.issend(v1.data(), tmap, v1.size(), {comm.rank(), {1}});
        {
            MPI_Message msg;
            auto const type = make_type<T>();
            auto const status = comm.probe({comm.rank(), Tag::any()}, &msg);
            auto const count = type.get_count(status);
            XCTAssert(N == count, "N = %ld, get_count = %ld", N, count);
            comm.recv(v2.data(), tmap, v2.size(), &msg);
        }
        std::move(req).wait();

        XCTAssert(std::equal(v1.begin(), v1.end(), v2.begin()));
    } catch (std::exception const &e) {
        XCTAssert(false, @"%s", e.what());
    }

    // blocking send-recv
    try {
        Comm const comm = Comm::world().split(1);

        std::array<T, N> v1{}, v2{};
        for (auto &v : v1) {
            std::get<0>(v) = real(rng);
            std::get<1>(v) = static_cast<char>(int_(rng));
        }

        comm.send_recv(v1.data(), tmap, v1.size(), {comm.rank(), Tag{1}},
                       v2.data(), tmap, v2.size(), {comm.rank(), Tag::any()});

        comm.send_recv(v2.data(), tmap, v2.size(), {comm.rank(), Tag{1}}, {comm.rank(), Tag::any()});

        XCTAssert(std::equal(v1.begin(), v1.end(), v2.begin()));
    } catch (std::exception const &e) {
        XCTAssert(false, @"%s", e.what());
    }
}

- (void)testP2P_LeakCheck {
    try {
        constexpr long n = 1000;
        for (long i = 0; i < n; ++i) {
            @autoreleasepool {
                [self testP2P_LowLevel];
            }
        }
    } catch (std::exception const &e) {
        XCTAssert(false, @"%s", e.what());
    }
}

- (void)testCommunicator_P2P {
    using Communicator = PAR::Communicator<double16, char, long>;
    using std::next;

    // synchronous send & fixed-size receive
    try {
        Communicator const comm{Comm::world().duplicated()};
        double16 x{10}, y;
        if (auto req = comm.issend(&x, next(&x), comm.rank())) {
            comm.recv<0>(&y, next(&y), comm.rank());
            std::move(req).wait();
        } else {
            XCTAssert(false, @"send failed");
        }
        XCTAssert(x == y, @"x = %f, y = %f", *x, *y);

        long i = 10, j;
        if (auto req = comm.issend<2>(&i, next(&i), comm.rank())) {
            comm.recv(&j, next(&j), comm.rank());
            std::move(req).wait();
        } else {
            XCTAssert(false, @"send failed");
        }
        XCTAssert(i == j, @"i = %ld, j = %ld", i, j);
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }

    // buffered send & unknown-size receive
    try {
        Communicator const comm{Comm::world().duplicated()};
        std::array<double16, 3> x{1, 2, 3}, y;
        if (auto req = comm.ibsend<double16>({begin(x), end(x)}, comm.rank())) {
            y = comm.recv<0>({}, comm.rank()).unpack([](auto lhs, auto &rhs) {
                std::move(begin(lhs), end(lhs), begin(rhs));
                return std::move(rhs);
            }, y);
            std::move(req).wait();
        } else {
            XCTAssert(false, @"send failed");
        }
        XCTAssert(std::equal(begin(x), end(x), begin(y)));

        std::array<long, 3> i{4, 3, 5}, j;
        if (auto req = comm.ibsend<2>({begin(i), end(i)}, comm.rank())) {
            j = comm.recv<long>({}, comm.rank()).unpack([](auto lhs, auto &rhs) {
                std::move(begin(lhs), end(lhs), begin(rhs));
                return std::move(rhs);
            }, j);
            std::move(req).wait();
        } else {
            XCTAssert(false, @"send failed");
        }
        XCTAssert(std::equal(begin(i), end(i), begin(j)));
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }

    // buffered, scalar send & buffered, scalar receive
    try {
        Communicator const comm{Comm::world().duplicated()};
        double16 x{10}, y;
        if (auto req = comm.ibsend(x, comm.rank())) {
            y = comm.recv<0>(comm.rank()).unpack(&double16::operator*);
            std::move(req).wait();
        } else {
            XCTAssert(false, @"send failed");
        }
        XCTAssert(x == y, @"x = %f, y = %f", *x, *y);

        long i = 10, j;
        if (auto req = comm.ibsend<2>(i, comm.rank())) {
            j = comm.recv<decltype(j)>(comm.rank());
            std::move(req).wait();
        } else {
            XCTAssert(false, @"send failed");
        }
        XCTAssert(i == j, @"i = %ld, j = %ld", i, j);
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }

    // buffered, scalar send & fixed-size receive
    try {
        Communicator const comm{Comm::world().duplicated()};
        double16 x{10}, y;
        if (auto req = comm.issend(x, comm.rank())) {
            comm.recv<0>(&y, next(&y), comm.rank());
            std::move(req).wait();
        } else {
            XCTAssert(false, @"send failed");
        }
        XCTAssert(x == y, @"x = %f, y = %f", *x, *y);

        long i = 10, j;
        if (auto req = comm.issend<2>(i, comm.rank())) {
            comm.recv(&j, next(&j), comm.rank());
            std::move(req).wait();
        } else {
            XCTAssert(false, @"send failed");
        }
        XCTAssert(i == j, @"i = %ld, j = %ld", i, j);
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }

    // blocking send and receive
    try {
        Communicator const comm{Comm::world().duplicated()};

        std::array<double16, 3> x{1, 2, 3}, y;
        comm.send_recv<0>(begin(x), end(x), comm.rank(), begin(y), end(y), comm.rank());
        XCTAssert(std::equal(begin(x), end(x), begin(y)));

        std::array<long, 2> i{4, 3}, j;
        comm.send_recv(begin(i), end(i), comm.rank(), begin(j), end(j), comm.rank());
        XCTAssert(std::equal(begin(i), end(i), begin(j)));

        comm.send_recv<double16>({begin(y), end(y)}, comm.rank(), comm.rank())
        .unpack([](auto y, auto const &x) {
            XCTAssert(std::equal(begin(x), end(x), begin(y)));
        }, x);

        comm.send_recv<2>({begin(j), end(j)}, comm.rank(), comm.rank())
        .unpack([](auto j, auto const &i) {
            XCTAssert(std::equal(begin(i), end(i), begin(j)));
        }, i);

        XCTAssert(comm.send_recv<0>(1.0, comm.rank(), comm.rank()) == 1.0);
        XCTAssert(comm.send_recv(long{1}, comm.rank(), comm.rank()) == 1);
    } catch (std::exception &e) {
        XCTAssert(false, @"%s", e.what());
    }
}

@end
