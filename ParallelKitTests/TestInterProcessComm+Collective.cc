/*
 * Copyright (c) 2021, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "TestInterProcessComm.h"
#include <catch2/catch.hpp>

#include <algorithm>
#include <array>
#include <random>

using PAR::mpi::Comm;
using PAR::mpi::Rank;

TEST_CASE("testCollective::LowLevel", "[InterThreadComm]")
{
    // barrier
    REQUIRE_NOTHROW(Comm::world().barrier());

    constexpr Rank root{ 0 };
    using T             = std::pair<double16, char>;
    auto const     tmap = PAR::make_type<T>();
    constexpr long N    = 100;

    std::mt19937_64                  rng{ 44393 };
    std::uniform_real_distribution<> real;
    std::uniform_int_distribution<>  int_{ std::numeric_limits<char>::min(),
                                          std::numeric_limits<char>::max() };

    // broadcast
    try {
        Comm const comm = Comm::world().duplicated();

        std::array<T, N> v1{}, v2{};
        for (auto &v : v1) {
            std::get<0>(v) = real(rng);
            std::get<1>(v) = static_cast<char>(int_(rng));
        }

        v2 = v1;
        comm.bcast(v2.data(), tmap, v2.size(), root);
        REQUIRE(std::equal(v1.begin(), v1.end(), v2.begin()));
    } catch (std::exception const &e) {
        INFO(e.what())
        REQUIRE(false);
    }

    // gather
    try {
        Comm const comm = Comm::world().duplicated();

        std::array<T, N> v1{}, v2{};
        for (auto &v : v1) {
            std::get<0>(v) = real(rng);
            std::get<1>(v) = static_cast<char>(int_(rng));
        }

        comm.gather(v1.data(), tmap, v1.size(), v2.data(), tmap, v2.size(), root);
        REQUIRE(std::equal(v1.begin(), v1.end(), v2.begin()));

        if (root == comm.rank()) {
            comm.gather(nullptr, v2.data(), tmap, v2.size());
        } else {
            comm.gather(v2.data(), tmap, v2.size(), nullptr, root);
        }
        REQUIRE(std::equal(v1.begin(), v1.end(), v2.begin()));

        std::fill(v2.begin(), v2.end(), T{});
        comm.all_gather(v1.data(), tmap, v1.size(), v2.data(), tmap, v2.size());
        REQUIRE(std::equal(v1.begin(), v1.end(), v2.begin()));

        comm.all_gather(nullptr, v2.data(), tmap, v2.size());
        REQUIRE(std::equal(v1.begin(), v1.end(), v2.begin()));
    } catch (std::exception const &e) {
        INFO(e.what())
        REQUIRE(false);
    }

    // scatter
    try {
        Comm const comm = Comm::world().duplicated();

        std::array<T, N> v1{}, v2{};
        for (auto &v : v1) {
            std::get<0>(v) = real(rng);
            std::get<1>(v) = static_cast<char>(int_(rng));
        }

        comm.scatter(v1.data(), tmap, v1.size(), v2.data(), tmap, v2.size(), root);
        REQUIRE(std::equal(v1.begin(), v1.end(), v2.begin()));

        if (root == comm.rank()) {
            comm.scatter(v2.data(), tmap, v2.size(), nullptr);
        } else {
            comm.scatter(nullptr, v2.data(), tmap, v2.size(), root);
        }
        REQUIRE(std::equal(v1.begin(), v1.end(), v2.begin()));

        std::fill(v2.begin(), v2.end(), T{});
        comm.all2all(v1.data(), tmap, v1.size(), v2.data(), tmap, v2.size());
        REQUIRE(std::equal(v1.begin(), v1.end(), v2.begin()));

        // comm.all2all(nullptr, v2.data(), tmap, v2.size()); // NOTE: mvapich crashes here.
        // REQUIRE(std::equal(v1.begin(), v1.end(), v2.begin()));
    } catch (std::exception const &e) {
        INFO(e.what())
        REQUIRE(false);
    }
}

TEST_CASE("testCommunicator::Collective", "[InterThreadComm]")
{
    using Communicator = PAR::Communicator<double16, char, long>;
    Communicator const comm{ Comm::world().duplicated() };

    // bcast
    try {
        std::array<double16, 3> x{ 10, 4, 5 };
        comm.bcast(begin(x), end(x), comm.rank());
        comm.bcast<0>(begin(x), end(x), comm.rank());
        long y{ 4 };
        REQUIRE((comm.bcast<long>(y, comm.rank()) == 4));
        REQUIRE((comm.bcast<2>(y, comm.rank()) == 4));
    } catch (std::exception &e) {
        INFO(e.what())
        REQUIRE(false);
    }

    // gather
    try {
        {
            std::array<double16, 3> x{ 10, 4, 5 }, y;
            comm.gather(begin(x), end(x), begin(y), comm.rank());
            REQUIRE(std::equal(begin(x), end(x), begin(y)));
        }
        {
            std::array<long, 3> x{ 10, 4, 5 }, y;
            comm.gather<2>(begin(x), end(x), begin(y), comm.rank());
            REQUIRE(std::equal(begin(x), end(x), begin(y)));
        }
        {
            std::array<double16, 3> x{ 10, 4, 5 };
            comm.gather<0>({ begin(x), end(x) }, comm.rank())
                .unpack(
                    [](auto y, auto const &x) {
                        REQUIRE(std::equal(begin(x), end(x), begin(y)));
                    },
                    x);
            std::array<long, 3> y{ 10, 4, 5 };
            comm.gather<long>({ begin(y), end(y) }, comm.rank())
                .unpack(
                    [](auto y, auto const &x) {
                        REQUIRE(std::equal(begin(x), end(x), begin(y)));
                    },
                    y);
        }
        //
        {
            std::array<double16, 3> x{ 10, 4, 5 }, y;
            comm.all_gather(begin(x), end(x), begin(y));
            REQUIRE(std::equal(begin(x), end(x), begin(y)));
        }
        {
            std::array<long, 3> x{ 10, 4, 5 }, y;
            comm.all_gather<2>(begin(x), end(x), begin(y));
            REQUIRE(std::equal(begin(x), end(x), begin(y)));
        }
        //
        {
            std::array<double16, 3> x{ 10, 4, 5 }, y = x;
            comm.all_gather<0>(begin(y), end(y));
            REQUIRE(std::equal(begin(x), end(x), begin(y)));
        }
        {
            std::array<long, 3> x{ 10, 4, 5 }, y = x;
            comm.all_gather(begin(y), end(y));
            REQUIRE(std::equal(begin(x), end(x), begin(y)));
        }
        //
        {
            std::array<double16, 3> x{ 10, 4, 5 };
            comm.all_gather<0>({ begin(x), end(x) })
                .unpack(
                    [](auto y, auto const &x) {
                        REQUIRE(std::equal(begin(x), end(x), begin(y)));
                    },
                    x);
            std::array<long, 3> y{ 10, 4, 5 };
            comm.all_gather<long>({ begin(y), end(y) })
                .unpack(
                    [](auto y, auto const &x) {
                        REQUIRE(std::equal(begin(x), end(x), begin(y)));
                    },
                    y);
        }
        //
        {
            std::array<double16, 3> x{ 10, 4, 5 }, y;
            comm.scatter(begin(x), begin(y), end(y), comm.rank());
            REQUIRE(std::equal(begin(x), end(x), begin(y)));
        }
        {
            std::array<long, 3> x{ 10, 4, 5 }, y;
            comm.scatter<2>(begin(x), begin(y), end(y), comm.rank());
            REQUIRE(std::equal(begin(x), end(x), begin(y)));
        }
        /* // mvapich2 crashes here
        {
            std::array<double16, 3> x{10, 4, 5}, y;
            comm.all2all(begin(x), end(x), begin(y));
            REQUIRE(std::equal(begin(x), end(x), begin(y)));
        }
        {
            std::array<long, 3> x{10, 4, 5}, y;
            comm.all2all<2>(begin(x), end(x), begin(y));
            REQUIRE(std::equal(begin(x), end(x), begin(y)));
        }
        //
        {
            std::array<double16, 3> x{10, 4, 5}, y;
            comm.all2all(begin(x), end(x), begin(y));
            REQUIRE(std::equal(begin(x), end(x), begin(y)));
        }
        {
            std::array<long, 3> x{10, 4, 5}, y;
            comm.all2all<2>(begin(x), end(x), begin(y));
            REQUIRE(std::equal(begin(x), end(x), begin(y)));
        }
        //
        {
            std::array<double16, 3> x{10, 4, 5};
            comm.all2all<0>({begin(x), end(x)})
                .unpack(
                    [](auto y, auto const &x) {
                        REQUIRE(std::equal(begin(x), end(x), begin(y)));
                    },
                    x);
            std::array<long, 3> y{10, 4, 5};
            comm.all2all<long>({begin(y), end(y)})
                .unpack(
                    [](auto y, auto const &x) {
                        REQUIRE(std::equal(begin(x), end(x), begin(y)));
                    },
                    y);
        }
         */
    } catch (std::exception &e) {
        INFO(e.what())
        REQUIRE(false);
    }
}
