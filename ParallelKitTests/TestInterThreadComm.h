/*
 * Copyright (c) 2020-2021, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#ifndef TestTestInterThreadComm_h
#define TestTestInterThreadComm_h

#define PARALLELKIT_INLINE_VERSION 0
#include <ParallelKit/InterThreadComm.h>

#include <algorithm>
#include <array>
#include <chrono>
#include <functional>
#include <future>
#include <iterator>
#include <memory>
#include <numeric>
#include <string>
#include <thread>
#include <utility>
#include <vector>

namespace PAR = PARALLELKIT_NAMESPACE(1)::_ver_tuple;

#endif /* TestTestInterThreadComm_h */
