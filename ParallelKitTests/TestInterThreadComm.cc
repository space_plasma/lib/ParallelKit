/*
 * Copyright (c) 2021, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "TestInterThreadComm.h"
#include <catch2/catch.hpp>

TEST_CASE("testSingleThreadComm", "[InterThreadComm]")
{
    using PAR::MessageDispatch;
    struct S {
        long i;
    };

    MessageDispatch<S> md{ 2 };
    auto const         comm = md.comm(1U);

    auto       tk = comm.send<S const &>(S{ 1 }, 1);
    auto const i  = comm.recv<S>(1).unpack(&S::i);
    std::move(tk).wait();

    INFO("i = " << i)
    REQUIRE(i == 1);
}

TEST_CASE("testMultiThreadComm1", "[InterThreadComm]")
{
    using PAR::MessageDispatch;

    constexpr int         N      = 10;
    constexpr unsigned    master = 0;
    MessageDispatch<long> md{ N + 1 };

    std::array<std::future<long>, N> flist;
    for (unsigned i = 0; i < flist.size(); ++i) {
        flist[i] = std::async(
            std::launch::async,
            [&md](unsigned i) -> long {
                auto const comm = md.comm(i + 1);
                return comm.recv<long>(master);
            },
            i);
    }
    {
        auto const comm = md.comm(master);
        for (int i = 0; i < N; ++i) {
            comm.send(long{ i }, i + 1).wait();
        }
    }
    for (unsigned i = 0; i < flist.size(); ++i) {
        auto const v = flist[i].get();
        INFO("i = " << i << ", v = " << v)
        CHECK(v == i);
    }
}

TEST_CASE("testMultiThreadComm2", "[InterThreadComm]")
{
    using PAR::MessageDispatch;

    constexpr int         N = 10, master = 0;
    MessageDispatch<long> md{ N + 1 };

    std::array<std::future<void>, N> flist;
    for (unsigned i = 0; i < flist.size(); ++i) {
        flist[i] = std::async(
            std::launch::async,
            [&md](unsigned i) {
                auto const comm = md.comm(i + 1);
                comm.send(*comm.recv<long>(master), master).wait();
            },
            i);
    }

    auto const comm = md.comm(master);
    for (int i = 0; i < N; ++i) {
        comm.send(long{ i }, i + 1).wait();
    }

    for (int i = 0; i < N; ++i) {
        auto const v = *comm.recv<long>(i + 1);
        INFO("i = " << i << ", v = " << v)
        CHECK(v == i);
    }

    for (auto &f : flist) {
        f.get();
    }
}

TEST_CASE("testCollectiveComm", "[InterThreadComm]")
{
    using PAR::MessageDispatch;

    constexpr unsigned                           master = 0;
    constexpr int                                N = 4, magic = 10;
    MessageDispatch<std::unique_ptr<long>, long> md{ N + 1 };

    std::array<std::future<long>, N> flist;
    for (unsigned i = 0; i < flist.size(); ++i) {
        flist[i] = std::async(
            std::launch::async,
            [&md](unsigned const rank) {
                auto const comm = md.comm(rank);
                if (master == rank) { // master
                    std::vector<unsigned>              extents;
                    std::vector<std::unique_ptr<long>> payloads;
                    for (unsigned rank = 0; rank < N; ++rank) {
                        extents.emplace_back(rank);
                        payloads.emplace_back(std::make_unique<long>(rank + 1));
                    }
                    comm.scatter(std::move(payloads), { begin(extents), end(extents) }).clear();
                    long const sum = [](auto pkgs) {
                        return std::accumulate(std::make_move_iterator(begin(pkgs)),
                                               std::make_move_iterator(end(pkgs)), long{},
                                               [](long const &a, auto b) {
                                                   return a + std::move(b).unpack([](auto ptr) {
                                                       return *ptr;
                                                   });
                                               });
                    }(comm.gather<0>({ begin(extents), end(extents) }));
                    comm.bcast(sum, { begin(extents), end(extents) }).clear();
                } else { // worker
                    comm.send(*comm.recv<0>(master), master).wait();
                }
                return *comm.recv<long>(master);
            },
            i);
    }

    for (unsigned i = 0; i < flist.size(); ++i) {
        auto const v = flist[i].get();
        INFO("i = " << i << ", v = " << v)
        CHECK(magic == v);
    }
}

TEST_CASE("testReductionComm", "[InterThreadComm]")
{
    using PAR::MessageDispatch;

    constexpr unsigned                              master = 0;
    constexpr int                                   N = 4, magic = 10;
    MessageDispatch<int, std::string, char const *> md{ N + 1 };
    std::vector<int>                                participants;
    std::string                                     str1, str2;
    for (int i = 1; i <= N; ++i) {
        (void)md.comm(i).send<0>(participants.emplace_back(i), master);
        char const *s1 = "a";
        (void)md.comm(i).send(s1, master), str1 += s1;
        std::string const s2 = std::to_string(i);
        (void)md.comm(i).send(s2, master), str2 += s2;
    }
    long const sum = *md.comm(master).reduce<0>({ begin(participants), end(participants) },
                                                std::make_unique<int>(0), [](auto a, auto b) {
                                                    *b += a;
                                                    return b;
                                                });
    INFO("magic = " << magic << ", sum = " << sum)
    CHECK(magic == sum);

    std::string str3;
    md.comm(master).for_each<char const *>({ begin(participants), end(participants) },
                                           [&str3](char const *s) {
                                               str3 += s;
                                           });
    INFO("str1 = " << str1.c_str() << ", str3 = " << str3.c_str())
    CHECK(str3 == str1);

    str3.clear();
    md.comm(master).for_each<std::string>({ begin(participants), end(participants) },
                                          [&str3](std::string s) {
                                              str3 += s;
                                          });
    INFO("str2 = " << str2.c_str() << ", str3 = " << str3.c_str())
    CHECK(str3 == str2);
}
