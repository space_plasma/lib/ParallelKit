/*
 * Copyright (c) 2021, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "TestInterProcessComm.h"
#include <catch2/catch.hpp>

#include <algorithm>
#include <array>
#include <random>

using PAR::make_type;
using PAR::mpi::Comm;
using PAR::mpi::Rank;
using PAR::mpi::ReduceOp;

TEST_CASE("testReduction::ReduceOp", "[InterProcessComm]")
{
    try {
        ReduceOp op1, op2;
        REQUIRE_FALSE(op1);
        REQUIRE_FALSE(op2);

        op1 = ReduceOp::max();
        REQUIRE((op1 && MPI_MAX == *op1));
        op2 = std::move(op1);
        REQUIRE((!op1 && op2 && MPI_MAX == *op2));

        op1 = ReduceOp::max();
        REQUIRE((op1 && op1.commutative() && MPI_MAX == *op1));
        op1 = ReduceOp::min();
        REQUIRE((op1 && op1.commutative() && MPI_MIN == *op1));
        op1 = ReduceOp::plus();
        REQUIRE((op1 && op1.commutative() && MPI_SUM == *op1));
        op1 = ReduceOp::prod();
        REQUIRE((op1 && op1.commutative() && MPI_PROD == *op1));
        op1 = ReduceOp::logic_and();
        REQUIRE((op1 && op1.commutative() && MPI_LAND == *op1));
        op1 = ReduceOp::bit_and();
        REQUIRE((op1 && op1.commutative() && MPI_BAND == *op1));
        op1 = ReduceOp::logic_or();
        REQUIRE((op1 && op1.commutative() && MPI_LOR == *op1));
        op1 = ReduceOp::bit_or();
        REQUIRE((op1 && op1.commutative() && MPI_BOR == *op1));
        op1 = ReduceOp::logic_xor();
        REQUIRE((op1 && op1.commutative() && MPI_LXOR == *op1));
        op1 = ReduceOp::bit_xor();
        REQUIRE((op1 && op1.commutative() && MPI_BXOR == *op1));
        op1 = ReduceOp::loc_min();
        REQUIRE((op1 && op1.commutative() && MPI_MINLOC == *op1));
        op1 = ReduceOp::loc_max();
        REQUIRE((op1 && op1.commutative() && MPI_MAXLOC == *op1));

        op1 = ReduceOp(false, [](void *, void *, int *, MPI_Datatype *) -> void {});
        REQUIRE((op1 && !op1.commutative()));
    } catch (std::exception const &e) {
        INFO(e.what())
        REQUIRE(false);
    }
}

TEST_CASE("testReduction::LowLevel", "[InterProcessComm]")
{
    // barrier
    //
    REQUIRE_NOTHROW(Comm::world().barrier());

    constexpr Rank root{ 0 };
    using T             = std::pair<double16, short>;
    auto const     tmap = PAR::make_type<T>();
    constexpr long N    = 100;

    std::mt19937_64                  rng{ 44393 };
    std::uniform_real_distribution<> real;
    std::uniform_int_distribution<>  int_{ std::numeric_limits<char>::min(),
                                          std::numeric_limits<char>::max() };

    // reduce local
    try {
        Comm const comm = Comm::world().duplicated();

        std::array<T, N> v1{}, v2{}, v3{};
        for (auto &v : v1) {
            std::get<0>(v) = real(rng);
            std::get<1>(v) = static_cast<char>(int_(rng));
        }
        for (auto &v : v2) {
            std::get<0>(v) = real(rng);
            std::get<1>(v) = static_cast<char>(int_(rng));
        }
        std::transform(v1.begin(), v1.end(), v2.begin(), v3.begin(), [](T const &a, T const &b) {
            return std::make_pair(std::get<0>(a) + std::get<0>(b), std::get<1>(a) + std::get<1>(b));
        });

        ReduceOp const op(true, [](void *_a, void *_b, int *n, MPI_Datatype *) {
            T const *a = static_cast<T const *>(_a);
            T       *b = static_cast<T *>(_b);
            for (int i = 0; i < *n; ++i) {
                std::get<0>(b[i]) = std::get<0>(a[i]) + std::get<0>(b[i]);
                std::get<1>(b[i]) = std::get<1>(a[i]) + std::get<1>(b[i]);
            }
        });
        auto const     type = make_type<T>();
        MPI_Reduce_local(v1.data(), v2.data(), v1.size(), *type, *op);
        REQUIRE(std::equal(v3.begin(), v3.end(), v2.begin()));
    } catch (std::exception const &e) {
        INFO(e.what())
        REQUIRE(false);
    }

    // reduce
    try {
        Comm const comm = Comm::world().duplicated();

        std::array<T, N> v1{}, v2{};
        for (auto &v : v1) {
            std::get<0>(v) = real(rng);
            std::get<1>(v) = static_cast<char>(int_(rng));
        }

        ReduceOp const op(true, [](void *_a, void *_b, int *n, MPI_Datatype *) {
            T const *a = static_cast<T const *>(_a);
            T       *b = static_cast<T *>(_b);
            for (int i = 0; i < *n; ++i) {
                std::get<0>(b[i]) = std::get<0>(a[i]) + std::get<0>(b[i]);
                std::get<1>(b[i]) = std::get<1>(a[i]) + std::get<1>(b[i]);
            }
            MPI_Abort(MPI_COMM_WORLD, -1);
        });

        comm.reduce(op, v1.data(), v2.data(), tmap, v1.size(), root);
        REQUIRE(std::equal(v1.begin(), v1.end(), v2.begin()));

        if (root == comm.rank()) {
            comm.reduce(op, v2.data(), tmap, v2.size());
        } else {
            comm.reduce(op, v2.data(), tmap, v2.size(), root);
        }
        REQUIRE(std::equal(v1.begin(), v1.end(), v2.begin()));

        std::fill(v2.begin(), v2.end(), T{});
        comm.all_reduce(op, v1.data(), v2.data(), tmap, v1.size());
        REQUIRE(std::equal(v1.begin(), v1.end(), v2.begin()));

        v2 = v1;
        comm.all_reduce(op, v2.data(), tmap, v2.size());
        REQUIRE(std::equal(v1.begin(), v1.end(), v2.begin()));
    } catch (std::exception const &e) {
        INFO(e.what())
        REQUIRE(false);
    }
}

TEST_CASE("testCommunicator::Reduction", "[InterProcessComm]")
{
    using Communicator = PAR::Communicator<double16, char, long>;
    using PAR::mpi::ReduceOp;
    using std::next;
    Communicator const comm{ Comm::world().duplicated() };

    ReduceOp const double16_plus(true, [](void *a, void *b, int *n, MPI_Datatype *) {
        using T          = double16;
        T const *first   = static_cast<T const *>(a);
        T       *d_first = static_cast<T *>(b);
        std::transform(first, next(first, *n), d_first, d_first, std::plus<>{});
    });

    // reduce
    try {
        {
            std::array<double16, 3> x{ 10, 4, 5 }, y;
            comm.reduce<0>(double16_plus, begin(x), end(x), begin(y), comm.rank());
            REQUIRE(std::equal(begin(x), end(x), begin(y)));
        }
        {
            std::array<long, 3> x{ 10, 4, 5 }, y;
            comm.reduce(ReduceOp::plus(), begin(x), end(x), begin(y), comm.rank());
            REQUIRE(std::equal(begin(x), end(x), begin(y)));
        }
        //
        {
            std::array<double16, 3> x{ 10, 4, 5 };
            comm.reduce<0>(double16_plus, { begin(x), end(x) }, comm.rank())
                .unpack(
                    [](auto y, auto const &x) {
                        REQUIRE(std::equal(begin(x), end(x), begin(y)));
                    },
                    x);
            std::array<long, 3> y{ 10, 4, 5 };
            comm.reduce<long>(ReduceOp::plus(), { begin(y), end(y) }, comm.rank())
                .unpack(
                    [](auto y, auto const &x) {
                        REQUIRE(std::equal(begin(x), end(x), begin(y)));
                    },
                    y);
        }
        //
        {
            std::array<double16, 3> x{ 10, 4, 5 }, y;
            comm.all_reduce<0>(double16_plus, begin(x), end(x), begin(y));
            REQUIRE(std::equal(begin(x), end(x), begin(y)));
        }
        {
            std::array<long, 3> x{ 10, 4, 5 }, y;
            comm.all_reduce(ReduceOp::plus(), begin(x), end(x), begin(y));
            REQUIRE(std::equal(begin(x), end(x), begin(y)));
        }
        //
        {
            std::array<double16, 3> x{ 10, 4, 5 }, y = x;
            comm.all_reduce<0>(double16_plus, begin(y), end(y));
            REQUIRE(std::equal(begin(x), end(x), begin(y)));
        }
        {
            std::array<long, 3> x{ 10, 4, 5 }, y = x;
            comm.all_reduce(ReduceOp::plus(), begin(y), end(y));
            REQUIRE(std::equal(begin(x), end(x), begin(y)));
        }
        //
        {
            std::array<double16, 3> x{ 10, 4, 5 };
            comm.all_reduce<0>(double16_plus, { begin(x), end(x) })
                .unpack(
                    [](auto y, auto const &x) {
                        REQUIRE(std::equal(begin(x), end(x), begin(y)));
                    },
                    x);
            std::array<long, 3> y{ 10, 4, 5 };
            comm.all_reduce<long>(ReduceOp::plus(), { begin(y), end(y) })
                .unpack(
                    [](auto y, auto const &x) {
                        REQUIRE(std::equal(begin(x), end(x), begin(y)));
                    },
                    y);
        }
        //
        {
            double16 x{ 10 };
            REQUIRE(comm.all_reduce<0>(double16_plus, x).unpack(std::equal_to<>{}, x));
            long y{ 5 };
            REQUIRE(comm.all_reduce<long>(ReduceOp::plus(), y).unpack(std::equal_to<>{}, y));
        }
    } catch (std::exception &e) {
        INFO(e.what())
        REQUIRE(false);
    }
}

TEST_CASE("testReduction::CustomReductionOperator", "[InterProcessComm]")
{
    // barrier
    //
    REQUIRE_NOTHROW(Comm::world().barrier());
    Comm const comm = Comm::world().duplicated();

    constexpr long N = 100;

    std::mt19937_64                         rng{ 44393 };
    std::uniform_real_distribution<>        real;
    std::uniform_int_distribution<unsigned> uint{ std::numeric_limits<unsigned char>::min(),
                                                  std::numeric_limits<unsigned char>::max() };

    try {
        using T = double;
        std::array<T, N> v1{}, v2{}, v3{}, v4{};
        for (auto &v : v1) {
            v = real(rng);
        }
        for (auto &v : v2) {
            v = real(rng);
        }

        std::copy(cbegin(v2), cend(v2), begin(v3));
        std::copy(cbegin(v2), cend(v2), begin(v4));
        MPI_Reduce_local(v1.data(), v3.data(), N, *make_type<T>(), *ReduceOp::max());
        MPI_Reduce_local(v1.data(), v4.data(), N, *make_type<T>(), *ReduceOp::max<T>(true));
        REQUIRE(std::equal(v3.begin(), v3.end(), v4.begin()));

        std::copy(cbegin(v2), cend(v2), begin(v3));
        std::copy(cbegin(v2), cend(v2), begin(v4));
        MPI_Reduce_local(v1.data(), v3.data(), N, *make_type<T>(), *ReduceOp::min());
        MPI_Reduce_local(v1.data(), v4.data(), N, *make_type<T>(), *ReduceOp::min<T>(true));
        REQUIRE(std::equal(v3.begin(), v3.end(), v4.begin()));

        std::copy(cbegin(v2), cend(v2), begin(v3));
        std::copy(cbegin(v2), cend(v2), begin(v4));
        MPI_Reduce_local(v1.data(), v3.data(), N, *make_type<T>(), *ReduceOp::plus());
        MPI_Reduce_local(v1.data(), v4.data(), N, *make_type<T>(), *ReduceOp::plus<T>(true));
        REQUIRE(std::equal(v3.begin(), v3.end(), v4.begin()));

        std::copy(cbegin(v2), cend(v2), begin(v3));
        std::copy(cbegin(v2), cend(v2), begin(v4));
        MPI_Reduce_local(v1.data(), v3.data(), N, *make_type<T>(), *ReduceOp::prod());
        MPI_Reduce_local(v1.data(), v4.data(), N, *make_type<T>(), *ReduceOp::prod<T>(true));
        REQUIRE(std::equal(v3.begin(), v3.end(), v4.begin()));
    } catch (std::exception const &e) {
        INFO(e.what())
        REQUIRE(false);
    }

    try {
        using T = unsigned long;
        std::array<T, N> v1{}, v2{}, v3{}, v4{};
        for (auto &v : v1) {
            v = uint(rng);
        }
        for (auto &v : v2) {
            v = uint(rng);
        }

        std::copy(cbegin(v2), cend(v2), begin(v3));
        std::copy(cbegin(v2), cend(v2), begin(v4));
        MPI_Reduce_local(v1.data(), v3.data(), N, *make_type<T>(), *ReduceOp::logic_and());
        MPI_Reduce_local(v1.data(), v4.data(), N, *make_type<T>(), *ReduceOp::logic_and<T>(true));
        REQUIRE(std::equal(v3.begin(), v3.end(), v4.begin()));

        std::copy(cbegin(v2), cend(v2), begin(v3));
        std::copy(cbegin(v2), cend(v2), begin(v4));
        MPI_Reduce_local(v1.data(), v3.data(), N, *make_type<T>(), *ReduceOp::bit_and());
        MPI_Reduce_local(v1.data(), v4.data(), N, *make_type<T>(), *ReduceOp::bit_and<T>(true));
        REQUIRE(std::equal(v3.begin(), v3.end(), v4.begin()));

        std::copy(cbegin(v2), cend(v2), begin(v3));
        std::copy(cbegin(v2), cend(v2), begin(v4));
        MPI_Reduce_local(v1.data(), v3.data(), N, *make_type<T>(), *ReduceOp::logic_or());
        MPI_Reduce_local(v1.data(), v4.data(), N, *make_type<T>(), *ReduceOp::logic_or<T>(true));
        REQUIRE(std::equal(v3.begin(), v3.end(), v4.begin()));

        std::copy(cbegin(v2), cend(v2), begin(v3));
        std::copy(cbegin(v2), cend(v2), begin(v4));
        MPI_Reduce_local(v1.data(), v3.data(), N, *make_type<T>(), *ReduceOp::bit_or());
        MPI_Reduce_local(v1.data(), v4.data(), N, *make_type<T>(), *ReduceOp::bit_or<T>(true));
        REQUIRE(std::equal(v3.begin(), v3.end(), v4.begin()));

        std::copy(cbegin(v2), cend(v2), begin(v3));
        std::copy(cbegin(v2), cend(v2), begin(v4));
        MPI_Reduce_local(v1.data(), v3.data(), N, *make_type<T>(), *ReduceOp::bit_xor());
        MPI_Reduce_local(v1.data(), v4.data(), N, *make_type<T>(), *ReduceOp::bit_xor<T>(true));
        REQUIRE(std::equal(v3.begin(), v3.end(), v4.begin()));
    } catch (std::exception const &e) {
        INFO(e.what())
        REQUIRE(false);
    }

    try {
        using T = double16;
        std::array<T, N> v1{}, v2{}, v3{}, v4{};
        for (auto &v : v1) {
            v = real(rng);
        }
        for (auto &v : v2) {
            v = real(rng);
        }

        std::copy(cbegin(v2), cend(v2), begin(v3));
        std::copy(cbegin(v2), cend(v2), begin(v4));
        std::transform(cbegin(v1), cend(v1), cbegin(v2), begin(v3), std::plus{});
        MPI_Reduce_local(v1.data(), v4.data(), N, *make_type<T>(), *ReduceOp::plus<T>(true));
        REQUIRE(std::equal(v3.begin(), v3.end(), v4.begin()));
    } catch (std::exception const &e) {
        INFO(e.what())
        REQUIRE(false);
    }
}
