/*
 * Copyright (c) 2020-2021, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#import <XCTest/XCTest.h>
#import "TestInterThreadComm.h"

@interface TestMessageDispatch : XCTestCase

@end

@implementation TestMessageDispatch

- (void)setUp {
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
}

- (void)testSingleThreadDispatch {
    using PAR::MessageDispatch;
    struct S { std::string s; };
    using Ptr = std::unique_ptr<std::string>;

    MessageDispatch<long, Ptr, S> dispatch{2};

    S const a1{__PRETTY_FUNCTION__};
    long const b1{1};
    std::string const c1{__FUNCTION__};

    auto tk1 = dispatch.send<0>(b1, {0, 1});
    auto tk2 = dispatch.send(std::make_unique<std::string>(c1), {0, 1});
    auto tk3 = dispatch.send(a1, {0, 1});

    long const b2 = dispatch.recv<long>({0, 1});
    std::string const c2 = dispatch.recv<1>({0, 1}).unpack(&Ptr::operator*);
    std::string const a2 = dispatch.recv<S>({0, 1}).unpack(&S::s);

    std::move(tk1).wait();
    std::move(tk2).wait();
    std::move(tk3).wait();

    XCTAssert(a1.s == a2, @"a1 = %s, a2 = %s", a1.s.c_str(), a2.c_str());
    XCTAssert(b1 == b2, @"b1 = %ld, b2 = %ld", b1, b2);
    XCTAssert(c1 == c2, @"c1 = %s, c2 = %s", c1.c_str(), c2.c_str());
}

- (void)testMultiThreadDispatch {
    using PAR::MessageDispatch;

    constexpr int N = 10;
    MessageDispatch<std::unique_ptr<long>, long> dispatch{N + 1};

    auto const f = [&dispatch](int const i) -> long {
        using namespace std::chrono_literals;
        std::this_thread::sleep_for(1s);
        dispatch.send(std::make_unique<long>(i + 1), {i + 1, 0}).wait();
        std::this_thread::sleep_for(1s);
        return dispatch.recv<long>({0, i + 1});
    };

    std::array<std::future<long>, N> flist;
    for (unsigned i = 0; i < N; ++i) {
        flist[i] = std::async(std::launch::async, f, i);
    }

    long sum = 0;
    for (int i = 0; i < N; ++i) {
        sum += dispatch.recv<0>({i + 1, 0}).unpack([](auto const ptr, long const sum) noexcept {
            return sum + *ptr;
        }, sum);
    }
    for (unsigned i = 0; i < N; ++i) {
        dispatch.send(static_cast<long const>(sum), {0U, i + 1}).wait();
    }

    for (unsigned i = 0; i < N; ++i) {
        auto const s = sum;
        auto const v = flist.at(i).get();
        XCTAssert(v == s, @"i = %d, v = %ld, s = %ld", i, v, s);
    }
}

- (void)testMultiThreadPingPong {
    using PAR::MessageDispatch;

    MessageDispatch<long> dispatch{2};

    auto f1 = std::async(std::launch::async, [&dispatch]{
        auto tk = dispatch.send<0>(1, {1, 1});
        (void)tk; //std::move(tk).wait();
        NSLog(@"1st msg sent");
    });
    auto f2 = std::async(std::launch::async, [&dispatch]{
        dispatch.send<0>(2, {1, 1}).wait();
        NSLog(@"2nd msg sent");
    });
    {
        using namespace std::chrono_literals;
        std::this_thread::sleep_for(1s);
    }
    dispatch.recv<0>({1, 1}).unpack([](long const payload) {
        NSLog(@"1st msg recv'ed: %ld", payload);
        XCTAssert(payload == 1 || payload == 2, @"payload is neither 1 nor 2.");
    });
    dispatch.recv<0>({1, 1}).unpack([](long const payload) {
        using namespace std::chrono_literals;
        std::this_thread::sleep_for(1s);
        NSLog(@"2nd msg recv'ed: %ld", payload);
        XCTAssert(payload == 1 || payload == 2, @"payload is neither 1 nor 2.");
    });

    f1.get();
    f2.get();
}

- (void)testMultiThreadCollective {
    using PAR::MessageDispatch;

    MessageDispatch<std::unique_ptr<long>, long> dispatch{3};
    constexpr long magic = 6;
    std::vector<decltype(dispatch)::Envelope> const dests{
        {0, 0}, {0, 1}, {0, 2}
    };

    auto f1 = std::async(std::launch::async, [&dispatch]{
        dispatch.send<0>(dispatch.recv<0>({0, 1}), {1, 0}).wait();
        return *dispatch.recv<long>({0, 1});
    });
    auto f2 = std::async(std::launch::async, [&dispatch]{
        dispatch.send<0>(dispatch.recv<0>({0, 2}), {2, 0}).wait();
        return *dispatch.recv<long>({0, 2});
    });

    {
        std::vector<std::unique_ptr<long>> payloads; {
            payloads.push_back(std::make_unique<long>(1));
            payloads.push_back(std::make_unique<long>(2));
            payloads.push_back(std::make_unique<long>(3));
        }
        dispatch.scatter(std::move(payloads), dests).clear();
    }
    (void)dispatch.send<0>(dispatch.recv<0>({0, 0}), {0, 0});
    long const sum = [](auto pkgs) {
        return std::accumulate(std::make_move_iterator(begin(pkgs)),
                               std::make_move_iterator(end(pkgs)),
                               long{}, [](long const &a, auto b) {
            return a + **std::move(b);
        });
    }(dispatch.gather<0>({{0, 0}, {1, 0}, {2, 0}}));
    dispatch.bcast(sum, {{0, 0}, {0, 1}, {0, 2}}).clear();

    {
        long const v = dispatch.recv<long>({0, 0});
        XCTAssert(magic == v, @"value recv'ed from {0, 0}: %ld", v);
    }
    {
        long const v = f1.get();
        XCTAssert(magic == v, @"value recv'ed from {0, 1}: %ld", v);
    }
    {
        long const v = f2.get();
        XCTAssert(magic == v, @"value recv'ed from {0, 2}: %ld", v);
    }

    dispatch.bcast<1>(3, {{0, 0}, {1, 0}, {2, 0}}).clear();
    dispatch.bcast<1>(2, {{0, 0}, {0, 1}, {0, 2}}).clear();
    dispatch.for_each<long>({{0, 0}, {1, 0}, {2, 0}}, [](long const i) {
        XCTAssert(i == 3, @"i = %ld", i);
    });
    {
        long const v = dispatch.reduce<1>({{0, 0}, {0, 1}, {0, 2}}, long{}, std::plus{});
        XCTAssert(magic == v, @"reduced value: %ld", v);
    }
}

@end
