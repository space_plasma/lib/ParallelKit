/*
 * Copyright (c) 2021, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "TestInterProcessComm.h"
#include <catch2/catch.hpp>

#include <algorithm>
#include <array>
#include <random>

using PAR::make_type;
using PAR::mpi::Comm;
using PAR::mpi::Tag;

TEST_CASE("testP2P::Request", "[InterThreadComm]")
{
    using PAR::mpi::Request;

    try {
        Request req1, req2{ std::move(req1) };
        CHECK(req2.test());
        std::move(req1).wait();
    } catch (std::exception const &e) {
        INFO(e.what())
        REQUIRE(false);
    }
}

TEST_CASE("testP2P::LowLevel", "[InterThreadComm]")
{
    // barrier
    REQUIRE_NOTHROW(Comm::world().barrier());

    using T             = std::pair<double16, char>;
    auto const     tmap = PAR::make_type<T>();
    constexpr long N    = 10;

    std::mt19937_64                  rng{ 44393 };
    std::uniform_real_distribution<> real;
    std::uniform_int_distribution<>  int_{ std::numeric_limits<char>::min(),
                                          std::numeric_limits<char>::max() };

    // persistence
    try {
        Comm const comm = Comm::world().duplicated();

        std::array<T, N> v1{}, v2{};

        auto send_req = comm.ssend_init(v1.data(), tmap, v1.size(), { comm.rank(), { 0 } });
        auto recv_req = comm.recv_init(v2.data(), tmap, v2.size(), { comm.rank(), Tag::any() });

        constexpr long n = 11;
        for (unsigned i = 0; i < n; ++i) {
            for (auto &v : v1) {
                std::get<0>(v) = real(rng);
                std::get<1>(v) = static_cast<char>(int_(rng));
            }
            send_req.start();
            recv_req.start();
            std::move(recv_req).wait();
            std::move(send_req).wait();

            REQUIRE(std::equal(v1.begin(), v1.end(), v2.begin()));
        }
    } catch (std::exception const &e) {
        INFO(e.what())
        REQUIRE(false);
    }

    // non-blocking send/blocking recv
    try {
        Comm const comm = Comm::world().split(1);

        std::array<T, N> v1{}, v2{};
        for (auto &v : v1) {
            std::get<0>(v) = real(rng);
            std::get<1>(v) = static_cast<char>(int_(rng));
        }

        auto req = comm.issend(v1.data(), tmap, v1.size(), { comm.rank(), { 1 } });
        {
            MPI_Message msg;
            auto const  type   = make_type<T>();
            auto const  status = comm.probe({ comm.rank(), Tag::any() }, &msg);
            auto const  count  = type.get_count(status);
            INFO("N = " << N << ", get_count = " << count)
            REQUIRE(N == count);
            comm.recv(v2.data(), tmap, v2.size(), &msg);
        }
        std::move(req).wait();

        REQUIRE(std::equal(v1.begin(), v1.end(), v2.begin()));
    } catch (std::exception const &e) {
        INFO(e.what())
        REQUIRE(false);
    }

    // blocking send-recv
    try {
        Comm const comm = Comm::world().split(1);

        std::array<T, N> v1{}, v2{};
        for (auto &v : v1) {
            std::get<0>(v) = real(rng);
            std::get<1>(v) = static_cast<char>(int_(rng));
        }

        comm.send_recv(v1.data(), tmap, v1.size(), { comm.rank(), Tag{ 1 } }, v2.data(), tmap,
                       v2.size(), { comm.rank(), Tag::any() });

        comm.send_recv(v2.data(), tmap, v2.size(), { comm.rank(), Tag{ 1 } },
                       { comm.rank(), Tag::any() });

        REQUIRE(std::equal(v1.begin(), v1.end(), v2.begin()));
    } catch (std::exception const &e) {
        INFO(e.what())
        REQUIRE(false);
    }
}

TEST_CASE("testCommunicator::P2P", "[InterThreadComm]")
{
    using Communicator = PAR::Communicator<double16, char, long>;
    using std::next;

    // synchronous send & fixed-size receive
    try {
        Communicator const comm{ Comm::world().duplicated() };
        double16           x{ 10 }, y;
        if (auto req = comm.issend(&x, next(&x), { comm.rank(), 1 })) {
            comm.recv<0>(&y, next(&y), { comm.rank(), 1U });
            std::move(req).wait();
        } else {
            INFO("send failed")
            REQUIRE(false);
        }
        INFO("x = " << *x << ", y = " << *y)
        REQUIRE(x == y);

        long i = 10, j{};
        if (auto req = comm.issend<2>(&i, next(&i), { comm.rank(), PAR::mpi::Tag{ 2 } })) {
            comm.recv(&j, next(&j), { static_cast<PAR::mpi::Rank>(comm.rank()), 2 });
            std::move(req).wait();
        } else {
            INFO("send failed")
            REQUIRE(false);
        }
        INFO("i = " << i << ", j = " << j)
        REQUIRE(i == j);
    } catch (std::exception &e) {
        INFO(e.what())
        REQUIRE(false);
    }

    // buffered send & unknown-size receive
    try {
        Communicator const      comm{ Comm::world().duplicated() };
        std::array<double16, 3> x{ 1, 2, 3 }, y;
        if (auto req = comm.ibsend<double16>({ begin(x), end(x) }, { comm.rank(), 3 })) {
            y = comm.recv<0>({}, { comm.rank(), 3 })
                    .unpack(
                        [](auto lhs, auto &rhs) {
                            std::move(begin(lhs), end(lhs), begin(rhs));
                            return std::move(rhs);
                        },
                        y);
            std::move(req).wait();
        } else {
            INFO("send failed")
            REQUIRE(false);
        }
        REQUIRE(std::equal(begin(x), end(x), begin(y)));

        std::array<long, 3> i{ 4, 3, 5 }, j;
        if (auto req = comm.ibsend<2>({ begin(i), end(i) }, { comm.rank(), 4 })) {
            j = comm.recv<long>({}, { comm.rank(), 4 })
                    .unpack(
                        [](auto lhs, auto &rhs) {
                            std::move(begin(lhs), end(lhs), begin(rhs));
                            return std::move(rhs);
                        },
                        j);
            std::move(req).wait();
        } else {
            INFO("send failed")
            REQUIRE(false);
        }
        REQUIRE(std::equal(begin(i), end(i), begin(j)));
    } catch (std::exception &e) {
        INFO(e.what())
        REQUIRE(false);
    }

    // buffered, scalar send & buffered, scalar receive
    try {
        Communicator const comm{ Comm::world().duplicated() };
        double16           x{ 10 }, y;
        if (auto req = comm.ibsend(x, { comm.rank(), 5 })) {
            y = comm.recv<0>({ comm.rank(), 5 }).unpack(&double16::operator*);
            std::move(req).wait();
        } else {
            INFO("send failed")
            REQUIRE(false);
        }
        INFO("x = " << *x << ", y = " << *y)
        REQUIRE(x == y);

        long i = 10, j{};
        if (auto req = comm.ibsend<2>(i, { comm.rank(), 6 })) {
            j = comm.recv<decltype(j)>({ comm.rank(), 6 });
            std::move(req).wait();
        } else {
            INFO("send failed")
            REQUIRE(false);
        }
        INFO("i = " << i << ", j = " << j)
        REQUIRE(i == j);
    } catch (std::exception &e) {
        INFO(e.what())
        REQUIRE(false);
    }

    // buffered, scalar send & fixed-size receive
    try {
        Communicator const comm{ Comm::world().duplicated() };
        double16           x{ 10 }, y;
        if (auto req = comm.issend(x, { comm.rank(), 7 })) {
            comm.recv<0>(&y, next(&y), { comm.rank(), 7 });
            std::move(req).wait();
        } else {
            INFO("send failed")
            REQUIRE(false);
        }
        INFO("x = " << *x << ", y = " << *y)
        REQUIRE(x == y);

        long i = 10, j{};
        if (auto req = comm.issend<2>(i, { comm.rank(), 8 })) {
            comm.recv(&j, next(&j), { comm.rank(), 8 });
            std::move(req).wait();
        } else {
            INFO("send failed")
            REQUIRE(false);
        }
        INFO("i = " << i << ", j = " << j)
        REQUIRE(i == j);
    } catch (std::exception &e) {
        INFO(e.what())
        REQUIRE(false);
    }

    // blocking send and receive
    try {
        Communicator const comm{ Comm::world().duplicated() };

        std::array<double16, 3> x{ 1, 2, 3 }, y;
        comm.send_recv<0>(begin(x), end(x), { comm.rank(), 1 }, begin(y), end(y),
                          { comm.rank(), 1 });
        REQUIRE(std::equal(begin(x), end(x), begin(y)));

        std::array<long, 2> i{ 4, 3 }, j;
        comm.send_recv(begin(i), end(i), { comm.rank(), 2 }, begin(j), end(j), { comm.rank(), 2 });
        REQUIRE(std::equal(begin(i), end(i), begin(j)));

        comm.send_recv<double16>({ begin(y), end(y) }, { comm.rank(), 3 }, { comm.rank(), 3 })
            .unpack(
                [](auto y, auto const &x) {
                    REQUIRE(std::equal(begin(x), end(x), begin(y)));
                },
                x);

        comm.send_recv<2>({ begin(j), end(j) }, { comm.rank(), 4 }, { comm.rank(), 4 })
            .unpack(
                [](auto j, auto const &i) {
                    REQUIRE(std::equal(begin(i), end(i), begin(j)));
                },
                i);

        REQUIRE((comm.send_recv<0>(1.0, { comm.rank(), 1 }, { comm.rank(), 1 }) == 1.0));
        REQUIRE((comm.send_recv(long{ 1 }, { comm.rank(), 2 }, { comm.rank(), 2 }) == 1));
    } catch (std::exception &e) {
        INFO(e.what())
        REQUIRE(false);
    }
}
