/*
 * Copyright (c) 2021, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "TestInterProcessComm.h"
#include <catch2/catch.hpp>

using PAR::make_type;

TEST_CASE("testType::Basic", "[InterProcessComm]")
{
    try {
        auto t0 = make_type<char>();
        REQUIRE((t0 && MPI_CHAR == *t0 && t0.alignment() == 1));

        auto t1 = std::move(t0);
        REQUIRE((!t0 && t0.alignment() == 0 && t1 && MPI_CHAR == *t1 && t1.alignment() == 1));

        t0 = std::move(t1);
        REQUIRE((!t1 && t1.alignment() == 0 && t0 && MPI_CHAR == *t0 && t0.alignment() == 1));
    } catch (std::exception const &e) {
        INFO(e.what())
        REQUIRE(false);
    }
}

TEST_CASE("testType::NativeChar", "[InterProcessComm]")
{
    try {
        using T      = char;
        auto const t = make_type<T>();
        REQUIRE((t && *t == MPI_CHAR && t.alignment() == alignof(T)));

        long const sig_size = t.signature_size();
        INFO("sig_size = " << sig_size)
        REQUIRE(sig_size == sizeof(T));

        auto [lb, extent] = t.extent();
        INFO("lb = " << lb << "; extent = " << extent)
        REQUIRE((lb == 0 && extent == sizeof(T)));

        std::tie(lb, extent) = t.true_extent();
        INFO("true lb = " << lb << "; true extent = " << extent)
        REQUIRE((lb == 0 && extent == sizeof(T)));
    } catch (std::exception const &e) {
        INFO(e.what())
        REQUIRE(false);
    }
}

TEST_CASE("testType::NativeUnsignedChar", "[InterProcessComm]")
{
    try {
        using T      = unsigned char;
        auto const t = make_type<T>();
        REQUIRE((t && *t == MPI_UNSIGNED_CHAR && t.alignment() == alignof(T)));

        long const sig_size = t.signature_size();
        INFO("sig_size = " << sig_size)
        REQUIRE(sig_size == sizeof(T));

        auto [lb, extent] = t.extent();
        INFO("lb = " << lb << "; extent = " << extent)
        REQUIRE((lb == 0 && extent == sizeof(T)));

        std::tie(lb, extent) = t.true_extent();
        INFO("true lb = " << lb << "; true extent = " << extent)
        REQUIRE((lb == 0 && extent == sizeof(T)));
    } catch (std::exception const &e) {
        INFO(e.what())
        REQUIRE(false);
    }
}

TEST_CASE("testType::NativeShort", "[InterProcessComm]")
{
    try {
        using T      = short;
        auto const t = make_type<T>();
        REQUIRE((t && *t == MPI_SHORT && t.alignment() == alignof(T)));

        long const sig_size = t.signature_size();
        INFO("sig_size = " << sig_size)
        REQUIRE(sig_size == sizeof(T));

        auto [lb, extent] = t.extent();
        INFO("lb = " << lb << "; extent = " << extent)
        REQUIRE((lb == 0 && extent == sizeof(T)));

        std::tie(lb, extent) = t.true_extent();
        INFO("true lb = " << lb << "; true extent = " << extent)
        REQUIRE((lb == 0 && extent == sizeof(T)));
    } catch (std::exception const &e) {
        INFO(e.what())
        REQUIRE(false);
    }
}

TEST_CASE("testType::NativeUnsignedShort", "[InterProcessComm]")
{
    try {
        using T      = unsigned short;
        auto const t = make_type<T>();
        REQUIRE((t && *t == MPI_UNSIGNED_SHORT && t.alignment() == alignof(T)));

        long const sig_size = t.signature_size();
        INFO("sig_size = " << sig_size)
        REQUIRE(sig_size == sizeof(T));

        auto [lb, extent] = t.extent();
        INFO("lb = " << lb << "; extent = " << extent)
        REQUIRE((lb == 0 && extent == sizeof(T)));

        std::tie(lb, extent) = t.true_extent();
        INFO("true lb = " << lb << "; true extent = " << extent)
        REQUIRE((lb == 0 && extent == sizeof(T)));
    } catch (std::exception const &e) {
        INFO(e.what())
        REQUIRE(false);
    }
}

TEST_CASE("testType::NativeInt", "[InterProcessComm]")
{
    try {
        using T      = int;
        auto const t = make_type<T>();
        REQUIRE((t && *t == MPI_INT && t.alignment() == alignof(T)));

        long const sig_size = t.signature_size();
        INFO("sig_size = " << sig_size)
        REQUIRE(sig_size == sizeof(T));

        auto [lb, extent] = t.extent();
        INFO("lb = " << lb << "; extent = " << extent)
        REQUIRE((lb == 0 && extent == sizeof(T)));

        std::tie(lb, extent) = t.true_extent();
        INFO("true lb = " << lb << "; true extent = " << extent)
        REQUIRE((lb == 0 && extent == sizeof(T)));
    } catch (std::exception const &e) {
        INFO(e.what())
        REQUIRE(false);
    }
}

TEST_CASE("testType::NativeUnsignedInt", "[InterProcessComm]")
{
    try {
        using T      = unsigned int;
        auto const t = make_type<T>();
        REQUIRE((t && *t == MPI_UNSIGNED && t.alignment() == alignof(T)));

        long const sig_size = t.signature_size();
        INFO("sig_size = " << sig_size)
        REQUIRE(sig_size == sizeof(T));

        auto [lb, extent] = t.extent();
        INFO("lb = " << lb << "; extent = " << extent)
        REQUIRE((lb == 0 && extent == sizeof(T)));

        std::tie(lb, extent) = t.true_extent();
        INFO("true lb = " << lb << "; true extent = " << extent)
        REQUIRE((lb == 0 && extent == sizeof(T)));
    } catch (std::exception const &e) {
        INFO(e.what())
        REQUIRE(false);
    }
}

TEST_CASE("testType::NativeLong", "[InterProcessComm]")
{
    try {
        using T      = long;
        auto const t = make_type<T>();
        REQUIRE((t && *t == MPI_LONG && t.alignment() == alignof(T)));

        long const sig_size = t.signature_size();
        INFO("sig_size = " << sig_size)
        REQUIRE(sig_size == sizeof(T));

        auto [lb, extent] = t.extent();
        INFO("lb = " << lb << "; extent = " << extent)
        REQUIRE((lb == 0 && extent == sizeof(T)));

        std::tie(lb, extent) = t.true_extent();
        INFO("true lb = " << lb << "; true extent = " << extent)
        REQUIRE((lb == 0 && extent == sizeof(T)));
    } catch (std::exception const &e) {
        INFO(e.what())
        REQUIRE(false);
    }
}

TEST_CASE("testType::NativeUnsignedLong", "[InterProcessComm]")
{
    try {
        using T      = unsigned long;
        auto const t = make_type<T>();
        REQUIRE((t && *t == MPI_UNSIGNED_LONG && t.alignment() == alignof(T)));

        long const sig_size = t.signature_size();
        INFO("sig_size = " << sig_size)
        REQUIRE(sig_size == sizeof(T));

        auto [lb, extent] = t.extent();
        INFO("lb = " << lb << "; extent = " << extent)
        REQUIRE((lb == 0 && extent == sizeof(T)));

        std::tie(lb, extent) = t.true_extent();
        INFO("true lb = " << lb << "; true extent = " << extent)
        REQUIRE((lb == 0 && extent == sizeof(T)));
    } catch (std::exception const &e) {
        INFO(e.what())
        REQUIRE(false);
    }
}

TEST_CASE("testType::NativeFloat", "[InterProcessComm]")
{
    try {
        using T      = float;
        auto const t = make_type<T>();
        REQUIRE((t && *t == MPI_FLOAT && t.alignment() == alignof(T)));

        long const sig_size = t.signature_size();
        INFO("sig_size = " << sig_size)
        REQUIRE(sig_size == sizeof(T));

        auto [lb, extent] = t.extent();
        INFO("lb = " << lb << "; extent = " << extent)
        REQUIRE((lb == 0 && extent == sizeof(T)));

        std::tie(lb, extent) = t.true_extent();
        INFO("true lb = " << lb << "; true extent = " << extent)
        REQUIRE((lb == 0 && extent == sizeof(T)));
    } catch (std::exception const &e) {
        INFO(e.what())
        REQUIRE(false);
    }
}

TEST_CASE("testType::NativeDouble", "[InterProcessComm]")
{
    try {
        using T      = double;
        auto const t = make_type<T>();
        REQUIRE((t && *t == MPI_DOUBLE && t.alignment() == alignof(T)));

        long const sig_size = t.signature_size();
        INFO("sig_size = " << sig_size)
        REQUIRE(sig_size == sizeof(T));

        auto [lb, extent] = t.extent();
        INFO("lb = " << lb << "; extent = " << extent)
        REQUIRE((lb == 0 && extent == sizeof(T)));

        std::tie(lb, extent) = t.true_extent();
        INFO("true lb = " << lb << "; true extent = " << extent)
        REQUIRE((lb == 0 && extent == sizeof(T)));
    } catch (std::exception const &e) {
        INFO(e.what())
        REQUIRE(false);
    }
}

TEST_CASE("testType::Realignment", "[InterProcessComm]")
{
    try {
        using T                  = double;
        constexpr long alignment = alignof(T) * 2, new_ext = sizeof(T) * 2;

        auto const t = make_type<T>().realigned(alignment);
        REQUIRE((t && t.alignment() == alignment));

        long const sig_size = t.signature_size();
        INFO("sig_size = " << sig_size)
        REQUIRE(sig_size == sizeof(T));

        auto [lb, extent] = t.extent();
        INFO("lb = " << lb << "; extent = " << extent)
        REQUIRE((lb == 0 && extent == new_ext));

        std::tie(lb, extent) = t.true_extent();
        INFO("true lb = " << lb << "; true extent = " << extent)
        REQUIRE((lb == 0 && extent == sizeof(T)));
    } catch (std::exception const &e) {
        INFO(e.what())
        REQUIRE(false);
    }
}

TEST_CASE("testType::Composite", "[InterProcessComm]")
{
    try {
        using T = std::pair<double, char>;

        auto t = make_type<T>();
        REQUIRE((t && t.alignment() == alignof(T)));

        long const sig_size = t.signature_size();
        INFO("sig_size = " << sig_size)
        REQUIRE(sizeof(std::tuple_element_t<0, T>) + sizeof(std::tuple_element_t<1, T>)
                == sig_size);

        auto [lb, extent] = t.extent();
        INFO("lb = " << lb << "; extent = " << extent)
        REQUIRE((lb == 0 && extent == sizeof(T)));

        std::tie(lb, extent) = t.true_extent();
        INFO("true lb = " << lb << "; true extent = " << extent)
        REQUIRE(lb == 0);
        REQUIRE(sizeof(std::tuple_element_t<0, T>) + sizeof(std::tuple_element_t<1, T>) == extent);

        auto const t2 = std::move(t);
        {
            REQUIRE((t2 && t2.alignment() == alignof(T)));

            long const sig_size = t2.signature_size();
            INFO("sig_size = " << sig_size)
            REQUIRE(sizeof(std::tuple_element_t<0, T>) + sizeof(std::tuple_element_t<1, T>)
                    == sig_size);

            auto [lb, extent] = t2.extent();
            INFO("lb = " << lb << "; extent = " << extent)
            REQUIRE((lb == 0 && extent == sizeof(T)));

            std::tie(lb, extent) = t2.true_extent();
            INFO("true lb = " << lb << "; true extent = " << extent)
            REQUIRE(lb == 0);
            REQUIRE(sizeof(std::tuple_element_t<0, T>) + sizeof(std::tuple_element_t<1, T>)
                    == extent);
        }
    } catch (std::exception const &e) {
        INFO(e.what())
        REQUIRE(false);
    }
    try {
        using T = std::pair<std::pair<std::pair<char, double>, char>, short>;

        auto const t = make_type<T>();
        REQUIRE((t && t.alignment() == alignof(T)));

        long const sig_size = t.signature_size();
        INFO("sig_size = " << sig_size)
        REQUIRE(sizeof(char) + sizeof(double) + sizeof(char) + sizeof(short) == sig_size);

        auto [lb, extent] = t.extent();
        INFO("lb = " << lb << "; extent = " << extent)
        REQUIRE((lb == 0 && extent == sizeof(T)));

        std::tie(lb, extent) = t.true_extent();
        INFO("true lb = " << lb << "; true extent = " << extent)
        REQUIRE(lb == 0);
        REQUIRE(sizeof(double) * 3 + sizeof(short) == extent);
    } catch (std::exception const &e) {
        INFO(e.what())
        REQUIRE(false);
    }
}

TEST_CASE("testType::Vector", "[InterProcessComm]")
{
    try {
        using T                = double;
        constexpr int n_blocks = 2, blocksize = 2, stride = 3;

        auto const t = make_type<T>().vector<n_blocks, blocksize, stride>();
        REQUIRE((t && t.alignment() == alignof(T)));

        long const sig_size = t.signature_size();
        INFO("sig_size = " << sig_size)
        REQUIRE(sig_size == sizeof(T) * n_blocks * blocksize);

        auto [lb, extent] = t.extent();
        INFO("lb = " << lb << "; extent = " << extent)
        REQUIRE(lb == 0);
        REQUIRE(extent == sizeof(T) * ((n_blocks - 1) * stride + blocksize));

        std::tie(lb, extent) = t.true_extent();
        INFO("true lb = " << lb << "; true extent = " << extent)
        REQUIRE(lb == 0);
        REQUIRE(extent == sizeof(T) * ((n_blocks - 1) * stride + blocksize));
    } catch (std::exception const &e) {
        INFO(e.what())
        REQUIRE(false);
    }
}

TEST_CASE("testType::Indexed", "[InterProcessComm]")
{
    try {
        using T                                      = std::pair<double, char>;
        constexpr int                       n_blocks = 2;
        constexpr std::array<int, n_blocks> blocksizes{ 3, 1 }, displacements{ 4, 0 };

        auto const t = make_type<T>().indexed<n_blocks>(blocksizes, displacements);
        REQUIRE((t && t.alignment() == alignof(T)));

        long const sig_size = t.signature_size();
        INFO("sig_size = " << sig_size)
        REQUIRE(sig_size == (sizeof(double) + sizeof(char)) * 4);

        auto [lb, extent] = t.extent();
        INFO("lb = " << lb << "; extent = " << extent)
        REQUIRE((lb == 0 && extent == 112));

        std::tie(lb, extent) = t.true_extent();
        INFO("true lb = " << lb << "; true extent = " << extent)
        REQUIRE((lb == 0 && extent == 112 - 7));
    } catch (std::exception const &e) {
        INFO(e.what())
        REQUIRE(false);
    }
}

TEST_CASE("testType::IndexedBlock", "[InterProcessComm]")
{
    try {
        using T                                      = std::pair<double, char>;
        constexpr int                       n_blocks = 2, blocksize = 2;
        constexpr std::array<int, n_blocks> displacements{ 4, 0 };

        auto const t = make_type<T>().indexed<n_blocks, blocksize>(displacements);
        REQUIRE((t && t.alignment() == alignof(T)));

        long const sig_size = t.signature_size();
        INFO("sig_size = " << sig_size)
        REQUIRE(sig_size == (sizeof(double) + sizeof(char)) * 4);

        auto [lb, extent] = t.extent();
        INFO("lb = " << lb << "; extent = " << extent)
        REQUIRE((lb == 0 && extent == 64 + sizeof(T) * 1 * blocksize));

        std::tie(lb, extent) = t.true_extent();
        INFO("true lb = " << lb << "; true extent = " << extent)
        REQUIRE((lb == 0 && extent == 64 + sizeof(T) * 1 * blocksize - 7));
    } catch (std::exception const &e) {
        INFO(e.what())
        REQUIRE(false);
    }
}

TEST_CASE("testType::Subarray", "[InterProcessComm]")
{
    try {
        using T                          = std::pair<double, char>;
        constexpr int                 ND = 2;
        constexpr std::array<int, ND> dims{ 3, 2 }, locs{ 1, 1 }, lens{ 1, 1 };

        auto const t = make_type<T>().subarray<ND>(dims, locs, lens);
        REQUIRE((t && t.alignment() == alignof(T)));

        long const sig_size = t.signature_size();
        INFO("sig_size = " << sig_size)
        REQUIRE(sig_size == (sizeof(double) + sizeof(char)));

        auto [lb, extent] = t.extent();
        INFO("lb = " << lb << "; extent = " << extent)
        REQUIRE((lb == 0 && extent == sizeof(T) * 3 * 2));

        std::tie(lb, extent) = t.true_extent();
        INFO("true lb = " << lb << "; true extent = " << extent)
        REQUIRE((lb == sizeof(T) * 3 && extent == sig_size));
    } catch (std::exception const &e) {
        INFO(e.what())
        REQUIRE(false);
    }
}

TEST_CASE("testType::StdComplex", "[InterProcessComm]")
{
    try {
        using T = std::complex<double>;

        auto const t = make_type<T>();
        REQUIRE((t && t.alignment() == alignof(T)));

        long const sig_size = t.signature_size();
        INFO("sig_size = " << sig_size)
        REQUIRE(sig_size == sizeof(T));

        auto [lb, extent] = t.extent();
        INFO("lb = " << lb << "; extent = " << extent)
        REQUIRE((lb == 0 && extent == sizeof(T)));

        std::tie(lb, extent) = t.true_extent();
        INFO("true lb = " << lb << "; true extent = " << extent)
        REQUIRE((lb == 0 && extent == sizeof(T)));
    } catch (std::exception const &e) {
        INFO(e.what())
        REQUIRE(false);
    }
}

TEST_CASE("testType::StdArray", "[InterProcessComm]")
{
    try {
        using T = std::array<double, 3>;

        auto const t = make_type<T>();
        REQUIRE((t && t.alignment() == alignof(T)));

        long const sig_size = t.signature_size();
        INFO("sig_size = " << sig_size)
        REQUIRE(sig_size == sizeof(T));

        auto [lb, extent] = t.extent();
        INFO("lb = " << lb << "; extent = " << extent)
        REQUIRE((lb == 0 && extent == sizeof(T)));

        std::tie(lb, extent) = t.true_extent();
        INFO("true lb = " << lb << "; true extent = " << extent);
        REQUIRE((lb == 0 && extent == sizeof(T)));
    } catch (std::exception const &e) {
        INFO(e.what())
        REQUIRE(false);
    }
}

TEST_CASE("testType::SomeType", "[InterProcessComm]")
{
    try {
        using T = std::pair<double16::native_type, double16>;

        auto const t = make_type<T>();
        REQUIRE((t && t.alignment() == alignof(T)));

        long const sig_size = t.signature_size();
        INFO("sig_size = " << sig_size)
        REQUIRE(sig_size == sizeof(double16::native_type) * 2);

        auto [lb, extent] = t.extent();
        INFO("lb = " << lb << "; extent = " << extent)
        REQUIRE((lb == 0 && extent == sizeof(T)));

        std::tie(lb, extent) = t.true_extent();
        INFO("true lb = " << lb << "; true extent = " << extent)
        REQUIRE((lb == 0 && extent == alignof(T) + sizeof(double16::native_type)));
    } catch (std::exception const &e) {
        INFO(e.what())
        REQUIRE(false);
    }
}
