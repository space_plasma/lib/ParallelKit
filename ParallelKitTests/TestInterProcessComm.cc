/*
 * Copyright (c) 2021-2022, Kyungguk Min
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "TestInterProcessComm.h"
#include <catch2/catch.hpp>

#include <ParallelKit/mpi_wrapper.h>

using PAR::mpi::Comm;

TEST_CASE("testComm::Comm", "[InterProcessComm]")
{
    using PAR::mpi::Comparison;
    using PAR::mpi::Group;

    try {
        Comm c;
        REQUIRE_FALSE(c);

        c = Comm::self();
        REQUIRE((c && MPI_COMM_SELF == *c && c.compare(Comm::self()) == Comparison::identical));
        println(std::cout, c.label());

        c = Comm::world();
        REQUIRE((c && MPI_COMM_WORLD == *c && c.compare(Comm::world()) == Comparison::identical));
        println(std::cout, c.label());

        c = c.duplicated();
        c.set_label("duplicated WORLD");
        REQUIRE((c && c.compare(Comm::world()) == Comparison::congruent
                 && c.compare(Comm::self()) == Comparison::congruent));
        println(std::cout, c.label());
        INFO("size = " << c.size() << ", rank = " << c.rank().v)
        REQUIRE((c.size() == 1 && c.rank() == 0));

        c = c.split(1);
        c.set_label("split comm");
        REQUIRE((c && c.compare(Comm::world()) == Comparison::congruent
                 && c.compare(Comm::self()) == Comparison::congruent));
        println(std::cout, c.label());
        INFO("size = " << c.size() << ", rank = " << c.rank().v)
        REQUIRE((c.size() == 1 && c.rank() == 0));

        c = c.created(Group{});
        REQUIRE_FALSE(c);

        c = Comm::world().split(MPI_UNDEFINED);
        REQUIRE_FALSE(c);
    } catch (std::exception const &e) {
        INFO(e.what())
        REQUIRE(false);
    }
}

TEST_CASE("testComm::Group", "[InterProcessComm]")
{
    using PAR::mpi::Comparison;
    using PAR::mpi::Group;
    using PAR::mpi::Rank;

    try {
        Comm const world = Comm::world().duplicated();
        Group      g;
        REQUIRE((g && *g == MPI_GROUP_EMPTY));

        g = world.group();
        REQUIRE((g && g.size() == 1 && g.rank() && g.rank() == 0));
        REQUIRE(g.translate(g.rank(), world.group()) == 0);
        REQUIRE(g.compare(world.group()) == Comparison::identical);
        REQUIRE(g.compare(Group{}) == Comparison::unequal);

        g = world.group() & Group{};
        REQUIRE((g && g.size() == 0 && !g.rank()));
        REQUIRE(g.translate(Rank::null(), world.group()) == Rank::null());
        REQUIRE(g.compare(world.group()) == Comparison::unequal);
        REQUIRE(g.compare(Group{}) == Comparison::identical);

        g = world.group() | Group{};
        REQUIRE((g && g.size() == 1 && g.rank() && g.rank() == 0));
        REQUIRE(g.translate(g.rank(), world.group()) == 0);
        REQUIRE(g.compare(world.group()) == Comparison::identical);
        REQUIRE(g.compare(Group{}) == Comparison::unequal);

        g = world.group() - Group{};
        REQUIRE((g && g.size() == 1 && g.rank() && g.rank() == 0));
        REQUIRE(g.translate(g.rank(), world.group()) == 0);
        REQUIRE(g.compare(world.group()) == Comparison::identical);
        REQUIRE(g.compare(Group{}) == Comparison::unequal);

        g = world.group().included({ world.rank() });
        REQUIRE((g && g.size() == 1 && g.rank() && g.rank() == 0));
        REQUIRE(g.translate(g.rank(), world.group()) == 0);
        REQUIRE(g.compare(world.group()) == Comparison::identical);
        REQUIRE(g.compare(Group{}) == Comparison::unequal);

        g = world.group().excluded({ world.rank() });
        REQUIRE((g && g.size() == 0 && !g.rank()));
        REQUIRE(g.translate(Rank::null(), world.group()) == Rank::null());
        REQUIRE(g.compare(world.group()) == Comparison::unequal);
        REQUIRE(g.compare(Group{}) == Comparison::identical);
    } catch (std::exception &e) {
        INFO(e.what())
        REQUIRE(false);
    }
}
