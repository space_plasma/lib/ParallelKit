# ParallelKit

[![pipeline status](https://gitlab.com/space_plasma/lib/ParallelKit/badges/master/pipeline.svg)](https://gitlab.com/space_plasma/lib/ParallelKit/-/commits/master)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://github.com/pre-commit/pre-commit)

ParallelKit attempts to provide abstraction for intra- and inter-process data parallelism in numerical computations.

For the inter-process data prallelism, Message Passing Interface (MPI) is widely used.
While the MPI specification covers a wide range of use cases and supports various programming languages, most notably FORTRAN, C, and C++,
one major drawback is not being user-friendly; users have to face a lot of boiler-plate codes to accomplish even a simple data exchange.
ParallelKit, through `parallel::TypeMap` (found at [ParallelKit/TypeMap.h](ParallelKit/TypeMap.h)) and `parallel::Communicator` (found at [ParallelKit/InterProcessComm.h](ParallelKit/InterProcessComm-Core.h)), is intended to abstract away those boiler-plate codes
and to make the API more strongly-typed and less prone to human errors.

It also provides primitive inter-thread data parallelism.
There are may multi-threading libraries, but focus here is on the MPI-like data parallelism.
This is provided by the `parallel::MessageDispatch` class and
`parallel::MessageDispatch::Communicator` class (found at [ParallelKit/InterThreadComm.h](ParallelKit/MessageDispatch-tuple.h)).


## Build and Test

Building it requires a **C++** compiler with a **C++17** support and the MPI library, such as **OpenMPI**.

The build configurations are found at [ParallelKit/Makefile](ParallelKit/Makefile).

Test cases are found at [test-ParallelKit](test-ParallelKit).


## A Quick Tour

### Inter-thread Communication

Below is a pseudo code demonstrating inter-thread data communication.

```cpp
#include <ParallelKit/InterThreadComm.h>
using namespace parallel;

template <class Comm>
void do_work(Comm const comm)
{
    long const rank = comm.rank();

    if (rank == 0) {
        std::vector<double> some_large_vector;
        auto ticket = comm.send(std::move(some_large_vector), 1);
        std::unique_ptr<double> const ptr_x = *comm.recv<2>(1);
        ticket.wait();

        // do other things with ptr_x

    } else {
        auto ptr_x = std::make_unique<double>(1.0);
        (void)comm.send(std::move(ptr_x), 0);
        auto const sum = comm.recv<1>(0).unpack([](auto payload) {
            return std::accumulate(begin(payload), end(payload), double{});
        }); // delievery notification is fired after unpack returns

        // do other things...
    }
}

int main() {
    MessageDispatch<long, std::vector<double>, std::unique_ptr<double>> dispatch{2};

    std::future<void>
    f1 = std::async(std::launch::async, [&dispatch]{ do_work(dispatch.comm(0)); });
    f2 = std::async(std::launch::async, [&dispatch]{ do_work(dispatch.comm(1)); });

    f1.get();
    f2.get();

    return 0;
}
```

### Inter-process Communication

Below is a pseudo code that sums an array of values from all MPI processes (all-gather operation).

```cpp
#include <ParallelKit/InterProcessComm.h>
using namespace parallel;

int main(int argc, char **argv) {
    Comm::init(&argc, &argv);
    if (MPI_Comm_set_errhandler(MPI_COMM_WORLD, MPI_ERRORS_RETURN) != MPI_SUCCESS) {
        throw std::runtime_error{std::string{__PRETTY_FUNCTION__} + " - MPI_Comm_set_errhandler(...) returned error"};
    }

    Communicator<double, char, long> const comm = mpi::Comm::world();

    std::array<double, N> data{...}, reduced;
    comm.all_reduce<0>(mpi::ReduceOp::plus(), begin(data), end(data), begin(reduced));

    // another way of doing the same thing
    // but, say data is in std::list
    std::list const data1{...};
    auto reduced1
    = comm.all_reduce<0>(mpi::ReduceOp::plus(), {begin(data1), end(data1)})
          .unpack([](auto payload)->std::list {
        return {begin(payload), end(payload)};
    });

    return MPI_Finalize();
}
```

A more concrete example is found at [mpi_bandwidth/mpi_bandwidth.cc](mpi_bandwidth/mpi_bandwidth.cc).
This is a modification of the program that measures the MPI communication bandwidth, written by Blaise Barney.
The original source code is found at [https://computing.llnl.gov/tutorials/mpi/samples/C/mpi_bandwidth.c](https://computing.llnl.gov/tutorials/mpi/samples/C/mpi_bandwidth.c).


## API Documentation

API is documented using doxygen markups (also available [here](https://space_plasma.gitlab.io/lib/ParallelKit)).

To build documentation, execute `make docs` in the project's root directory.
By default, it will create documentation in the `html` format.
To produce the documentation in other formats, modify the Doxygen configuration, [Doxyfile](Doxyfile).

For details about providing user-defined typemaps, refer to [ParallelKit/TypeMap.h](ParallelKit/TypeMap.h).
